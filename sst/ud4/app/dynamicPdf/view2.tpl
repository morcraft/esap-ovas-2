<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PLANEACIÓN ESTRATÉGICA</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<style>
    img {
        width: 100%;
    }
    @font-face {
        font-family: 'BebasNeue';
        src: url('../fonts/BebasNeue.otf');
    }
    @font-face {
        font-family: 'FFLight';
        src: url('../fonts/FF_DIN_Pro_Light.otf');
    }
    @font-face {
        font-family: 'FFBold';
        src: url('../fonts/FF_DIN_Pro_Bold.otf');
    }
    @font-face {
        font-family: 'FFBCondBold';
        src: url('../fonts/FF_DIN_Pro_Cond_Bold.otf');
    }
    @font-face {
        font-family: 'FFCondRegular';
        src: url('../fonts/FF_DIN_Pro_Cond_Regular.otf');
    }
    @font-face {
        font-family: 'FFCondLight';
        src: url('../fonts/FF_DIN_Pro_Cond_Light.otf');
    }
    @font-face {
        font-family: 'FFLight';
        src: url('../fonts/FF_DIN_Pro_Light.otf');
    }
    @font-face {
        font-family: 'FFThin';
        src: url('../fonts/FF_DIN_Pro_Thin.otf');
    }
    @font-face {
        font-family: 'Wingding';
        src: url('../fonts/wingding.ttf');
    }
    @font-face {
        font-family: 'century';
        src: url('../fonts/CenturyGothic.ttf');
    }
    @font-face {
        font-family: 'gothiddd';
        src: url('../fonts/GOTHIC.ttf');

    }
    @font-face {
        font-family: 'myriadRegular';
        src: url('../fonts/MyriadPro-Regular.otf');
    }

    html {

    }

    body {
        font-size: 12px;
        font-family: 'gothiddd';
    }

    h2 {
        font-family: 'FFDINPro-Bold';
        font-size: 20px;
        font-weight: 100;
        text-align: center;
        color: #3F0029;
    }

    p {
        font-size: 20px;
        color:#3F0029;
        font-family: 'gothiddd';
    }

    @page {
        margin: 100px 25px;
    }

    header {
        position: fixed;
        top: -60px;
        left: 0px;
        right: 0px;
        height: 55px;
        background-color:#96005A;
        padding: 10px;
        border-radius: 20px;
    }

    footer {
        position: fixed;
        bottom: -60px;
        left: 0px;
        right: 0px;
        font-size: 12px;
        font-family: 'FFDINProCond-Bold';
        text-align: center;
        background-color: #96005A;
        color: #FFFFFF;
        padding: 10px;
        border-radius: 10px;
    }

    header h1 {
        font-family: 'FFDINPro-Bold';
        font-size: 16px;
        font-weight: 100;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #FFF;
        padding-left: 80px;

    }

    header h2 {
        font-family: 'FFDINPro-Bold';
        font-size: 14px;
        font-weight: 100;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #FFFFFF;
        padding-left: 80px;
    }

    #esap-logo {
        position: absolute;
        width: 60px;
    }

    .question-text {
        font-family: 'FFDINPro-Light';
        color: #033449;
        font-size: 16px;
    }

    .date {
        font-family: 'FFDINPro-Light';
        color:#3F0029;
        text-align: right;
        font-size: 8px;
        border-bottom: solid #3F0029 1px dashed;
        margin-bottom: 10px;
        margin-top: 30px;
    }

    .date p {
        font-size: 12px;
        font-family: 'FFDINPro-Bold';
    }

    .date b {
        font-family: 'FFDINPro-Bold';
        font-weight: inherit;
    }

    .quiz-title {
        font-family: 'FFDINPro-Light';
        margin: 0 18px 0 10px;
        padding: 0;
        font-size: 24px;
    }

    .contRtaUno {
        width: 100%;
        height: auto;
    }

    .contRtaUno h2 {
        padding: 0%;
        margin: 0%;
        font-size: 1.5em;
    }
    .contRtaUno p {
        padding: 4px;
        margin: 0%;
        font-size: 2em;
        text-align: center;
        border: solid 2px  #3F0029;
        font-family: 'FFDINPro-Light';
        margin-top: 10px;
    }
    .contRtaUno ul {
        border: solid 2px  #3F0029;
    }

    .contRtaUno ul li {
        padding: 0%;
        margin: 0%;
        font-size: 2em;
        text-align: center;
        font-family: 'FFDINPro-Light';
        color: #033449;
        list-style: none;
        margin-bottom: 10px;
    }
    .contRtaUno p span{
        font-family: 'FFDINPro-Bold';
    }


</style>

<body>
    <header>
        <img src="assets/img/esap_logo.png" id="esap-logo">
        <h1>Gestión organizacional de los grupos de apoyo en Seguridad y Salud en el Trabajo: comité de convivencia laboral</h1>
        <h2>Seguridad y Salud en el Trabajo</h2>
    </header>
    <div class="date">
        <p>Hora de generación:
            <b>{$date}</b>
        </p>
    </div>

    <h2 class="quiz-title">Respuestas Actividad 2</h2>
    <br>
    <div class="contRtaUno">
        <h2>3) Analice el video anterior con relación a la Ley 1010 de 2006 y defina las conductas que pueden o no considerarse como acoso laboral.</h2>
        <p>{$r1[0]}</p>
        <br>
        <br>
        <h2>5) Comparta la infografía en la plataforma virtual, y realice aportes constructivos con relación a las medidas de intervención propuestas por otros compañero.</h2>
        <p>{$r1[1]}</p>
        <br>

    </div>
    <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>

</body>

</html>
