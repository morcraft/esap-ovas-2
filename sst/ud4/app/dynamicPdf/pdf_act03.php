<?php
error_reporting(E_ALL);
require 'vendor/autoload.php';
use Dompdf\Dompdf;

$smarty = new Smarty();
$smarty->error_reporting = 1;
$smarty->caching = 0;
$smarty->assign($_GET);
$smarty->assign(array(
    'date' => date("Y-m-d h:i:sa")
));
$view3 = $smarty->fetch('view3.tpl');
// echo $view;
// exit(0);

$dompdf = new Dompdf();
$dompdf->loadHtml($view3);
$dompdf->setPaper('A4', 'portrait');
$dompdf->render();
$dompdf->stream("Actividad 03.pdf", array("Attachment" => false));
//$dompdf->stream("justificacion_actividad_MASC.pdf", array("Attachment" => true));
exit(0);
?>
