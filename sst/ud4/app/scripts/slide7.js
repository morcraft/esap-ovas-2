const _ = require("lodash");
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const courseData = require('courseData.json');
var $textarraydos= new Array()
const slide2 = require('slide2.js');
module.exports = {
  onEnter: function(args){
      swal({
          target: stageResize.currentProps.$stage.get(0),
          customClass: 'ova-themed biggest modal_s5',
          showCloseButton: true,
          showConfirmButton: false,
          html: require("templates/modal_s7_1.hbs")()
       })
      var self = this;
      console.log(self.activities)
      if (_.isObject(self.activities.AutoevaluacionPrimeraUd4.textos['r10'])) {
        var rtauno = self.activities.AutoevaluacionPrimeraUd4.textos.r10;
        console.log(rtauno);
        var a=-1;
        $('.cont-preguntados .cont-text textarea').each(function(){
          a++
          $(this).val(rtauno[a]);
        })
      }else{
        var actpartuno  = slide2.currentProps.$textarrayf
        console.log("respuesta de la variable", actpartuno);
        var a=-1;
        $('.cont-preguntados .cont-text textarea').each(function(){
          a++
          $(this).val(actpartuno[a]);
        })
      }

  },
    addEventListeners: function () {
        var self = this;
        var $dataicon
        $('.btn-cont-itemdos').on('click', function() {
          $('.cont-preguntados').css('display', 'none');
        })
        $('.icon-preguntados').on("click", function(){
          $dataicon = $(this).data('icon-pregunta')
          $('.cont-preguntados[data-pregunta="'+$dataicon+'"]').css('display', 'block');
        })
        $('.button-finish-s7').on("click", function(){
          $('.cont-preguntados .cont-text textarea').each(function(){
            var textoareados = $(this).val();
            console.log(textoareados);
            $textarraydos.push(textoareados);
            console.log($textarraydos);
          })
            swal({
                target: stageResize.currentProps.$stage.get(0),
                customClass: 'ova-send-activity',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/modal_act_3.hbs")({
                  parametros: $.param({
                    r1: $textarraydos
                  }),
                  assignLink: _.get(self, 'courseActivities.assign.1.href')
                })
              })
              .then(function (value) {
                console.log(self);
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Actividad 3': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
                $('.act-end').addClass("disabled");
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });
            })
        })
    }
}
