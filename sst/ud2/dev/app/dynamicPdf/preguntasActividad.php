<?php
    $options = Array(
        "¿Cuales pueden ser los peligros y riesgos en un contexto laboral?",
        "Su descripción de los peligros laborales",
        "¿Qué métodos de control en seguridad y salud en el trabajo, se pueden usar para la intervención de los peligros y riesgos?",
        "¿Cuales métodos de control en seguridad y salud en el trabajo, se pueden establecer teniendo en cuenta la jerarquización de los controles para la intervención de los peligros y riesgos identificados, y utilizando la metodología GTC 45?",
        "¿Qué es y cómo se desarrolla un proceso de identificación de peligros y riesgos para las organizaciones?"
    )
?>