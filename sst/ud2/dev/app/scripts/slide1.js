const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

    addEventListeners: function(){

        setTimeout(function () {
            swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed allDoneModal',
                showCloseButton: true,
                showConfirmButton: false,
                title: 'En la presente unidad usted aprenderá a establecer métodos de control en seguridad y salud en el trabajo para la intervención de los peligros y riesgos identificados, utilizando la metodología GTC 45 y teniendo en cuenta la jerarquización de los controles propuestos.<br><br>Para empezar inicie la Actividad de aprendizaje 1 de la primera Experiencia de Aprendizaje.',
            });
        }, 4000);

        var self = this;

        console.log("this", self);
        console.log(self.activities);

        $("#presentation").on("slide", function (e, i) {
            console.log("onSlide");
            $('.navigation-buttons .home').addClass("oculto");
            $('.navigation-buttons .next').addClass("oculto");
            $('.navigation-buttons .prev').addClass("oculto");

            var slideId = presentation.currentProps.$slide.attr("data-slide");
            console.log(slideId);
            if (slideId != 1) {
                setTimeout(function () {
                    $('.navigation-buttons .home').removeClass("oculto");
                    //$('.navigation-buttons .home').addClass("volver");
                }, 1000);
            }

            // if (slideId == 2 || slideId == 4 || slideId == 7 || slideId == 10) {
            //     $('.navigation-buttons .next').removeClass("oculto");
            //     setTimeout(function () {
            //         $('.navigation-buttons .next').addClass("highlight");
            //     }, 1000);
            // }

            // if (slideId == 3 || slideId == 5 || slideId == 8 || slideId == 11) { 
            //     $('.navigation-buttons .prev').removeClass("oculto");
            // }
        });

        $("[data-slide='1'] .recurso[data-recurso='1']").on("click", function () {
            if (!$(this).hasClass("disabled")) {
                swal({
                    target: stageResize.currentProps.$stage.get(0),
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    customClass: 'ova-themed video',
                    showCloseButton: true,
                    showConfirmButton: false,
                    html: require("templates/slide1Video.hbs")(self.media),
                }).then(function (t) {
                    var targetConcepts = _.pick(concepts, [
                        "Análisis de Riesgos",
                        "Identificación del peligro",
                        "Medida de control",
                        "Valoración de los riesgos",
                        "Riesgo aceptable"
                    ]);
                    
                    self.ovaConcepts.showNewConceptsModal({
                        title: "¿Ha usado estos conceptos en su contexto laboral?",
                        subtitle: "Imagine una situación cotidiana donde se usen estos conceptos. Lea en el cofre las definiciones de estos y compare sus imágenes con la información que allí encontró.",
                        concepts: targetConcepts
                    })
                    .then(function (t) {
                        setTimeout(function () {
                            self.ovaConcepts.updateConcepts({
                                concepts: targetConcepts,
                                action: "insert",
                                courseShortName: courseData.shortName,
                                courseUnit: courseData.unit
                            });
                        }, 1500);
                        setTimeout(function () {
                            self.ovaProgress.updateFinishedTopics({
                                topics: {
                                    'recurso_1': true
                                },
                                action: 'add',
                                courseShortName: courseData.shortName,
                                courseUnit: courseData.unit
                            });
                        }, 3000);
                        setTimeout(function () {
                            $('.slide[data-slide="1"] .recurso[data-recurso="2"]').addClass("click-me");
                        }, 4500);
                    })
                });
            }
        });

        $("[data-slide='1'] .recurso[data-recurso='2']").on("click", function () {
            if (!$(this).hasClass("disabled")) {
                swal({
                    target: stageResize.currentProps.$stage.get(0),
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    customClass: 'ova-themed video',
                    showCloseButton: true,
                    showConfirmButton: false,
                    html: require("templates/slide1Video2.hbs")(self.media),
                }).then(function (t) {
                    var targetConcepts = _.pick(concepts, [
                        "Riesgo",
                        "Peligro",
                        "Efectividad de los controles",
                        "Consecuencia"
                    ]);
                    
                    self.ovaConcepts.showNewConceptsModal({
                        title: "¿Ha escuchado antes estos conceptos?",
                        subtitle: "Imagine una situación cotidiana donde se usen estos conceptos. Lea en el cofre las definiciones de estos y compare sus imágenes con la información que allí encontró.",
                        concepts: targetConcepts
                    })
                    .then(function (t) {
                        setTimeout(function () {
                            self.ovaConcepts.updateConcepts({
                                concepts: targetConcepts,
                                action: "insert",
                                courseShortName: courseData.shortName,
                                courseUnit: courseData.unit
                            });
                        }, 1500);
                        setTimeout(function () {
                            self.ovaProgress.updateFinishedTopics({
                                topics: {
                                    'recurso_2': true
                                },
                                action: 'add',
                                courseShortName: courseData.shortName,
                                courseUnit: courseData.unit
                            });
                        }, 3000);
                        setTimeout(function () {
                            $('.slide[data-slide="1"] .actividad[data-actividad="2"]').addClass("click-me");
                        }, 4500);
                    })
                });
            }
        });

        // $("[data-slide='1'] .estrella[data-estrella='4']").on("click", function () {
        //     if(!$(this).hasClass("disabled")){
        //         swal({
        //             target: stageResize.currentProps.$stage.get(0),
        //             allowOutsideClick: false,
        //             allowEscapeKey: false,
        //             customClass: 'ova-themed video',
        //             showCloseButton: true,
        //             showConfirmButton: false,
        //             html: require("templates/slide1Video.hbs")(self.media),
        //         }).then(function (t) {
        //             var targetConcepts = _.pick(concepts, [
        //                 "concepto 7"
        //             ]);
                    
        //             ovaConcepts.showNewConceptsModal({
        //                 title: "Conteste mentalmente la siguiente pregunta",
        //                 subtitle: "¿Considera usted que estos estándares se cumplen en su IPS más cercana?",
        //                 concepts: targetConcepts
        //             })
        //             .then(function (t) {
        //                 setTimeout(function () {
        //                     ovaConcepts.updateConcepts({
        //                         concepts: targetConcepts,
        //                         action: "insert",
        //                         courseShortName: courseData.shortName,
        //                         courseUnit: courseData.unit
        //                     });
        //                 }, 500);
        //                 setTimeout(function () {
        //                     self.ovaProgress.updateFinishedTopics({
        //                         topics: {
        //                             'recurso_3': true
        //                         },
        //                         action: 'add',
        //                         courseShortName: courseData.shortName,
        //                         courseUnit: courseData.unit
        //                     });
        //                 }, 1500);
        //                 setTimeout(function () {
        //                     $('.slide[data-slide="1"] .estrella[data-estrella="4"]').addClass("dorada"); 
        //                 }, 2500);
        //             })
        //         });
        //     }
        // });

        // $("[data-slide='1'] .estrella[data-estrella='7']").on("click", function () {
        //     if(!$(this).hasClass("disabled")){
        //         swal({
        //             target: stageResize.currentProps.$stage.get(0),
        //             allowOutsideClick: false,
        //             allowEscapeKey: false,
        //             customClass: 'ova-themed video',
        //             showCloseButton: true,
        //             showConfirmButton: false,
        //             html: require("templates/slide1Video1.hbs")(self.media),
        //         }).then(function (t) {
        //             self.ovaProgress.updateFinishedTopics({
        //                 topics: {
        //                     'recurso_5': true
        //                 },
        //                 action: 'add',
        //                 courseShortName: courseData.shortName,
        //                 courseUnit: courseData.unit
        //             });
        //             setTimeout(function () {
        //                 $('.slide[data-slide="1"] .estrella[data-estrella="7"]').addClass("dorada"); 
        //             }, 1000);
        //         });
        //     }
        // });


        $('.navigation-buttons .next').addClass("oculto");
        $('.navigation-buttons .prev').addClass("oculto");
        $('.navigation-buttons .home').addClass("oculto");

        if (_.isObject(this.finishedTopics['evaluacion_inicial'])) {
            $('.slide[data-slide="1"] .edificio').removeClass("disabled");
            $('.slide[data-slide="1"] .recurso').removeClass("disabled");
            $('.slide[data-slide="1"] .actividad').removeClass("disabled");
            $('.slide[data-slide="3"]').data("presentation").enabled = true;
            $('.slide[data-slide="2"]').data("presentation").enabled = false;
            $('.slide[data-slide="1"] .actividad[data-actividad="1"]').addClass("disabled");       
        }

        if (_.isObject(this.finishedTopics['evaluacion_final'])) {
            $('.slide[data-slide="5"]').data("presentation").enabled = false;
            $('.slide[data-slide="1"] .actividad[data-actividad="3"]').addClass("disabled"); 
        }
    }
}