const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const draggables = require("draggables.json");
const preciseDraggable = require('precise-draggable')
const slidesManager = require('slidesManager.js')

module.exports = {

    currentProps: {
        newAnswers: [], 
        finalDrags: [],
        finalAnswers: []
    },

    onEnter: function(){
        swal({
            target: stageResize.currentProps.$stage.get(0),
            allowOutsideClick: false,
            allowEscapeKey: false,
            customClass: 'ova-themed modalText',
            showCloseButton: true,
            showConfirmButton: false,
            title: '¿Recuerda qué contesto en la primera actividad? En este momento puede cambiar o complementar su respuestas, según lo aprendido a lo largo de esta unidad.',
        });
    },

    addEventListeners: function(){

        var $pregunta;

        var self = this;

        //var finalDrags = [];
        //var finalAnswers = [];

        $('.slide[data-slide="5"]  textarea').on("keyup", function () {
            $this = $(this);
            $respuesta = $this.data("respuesta");

            module.exports.currentProps.finalAnswers[$respuesta] = $this.val();
        });

        _.forEach(draggables.droppableZones, function (v, k) {
            $('<div class="droppable drop" data-ani-class="wobble"/>')
            .appendTo($('.slide[data-slide="5"] .dropables'))
            .on({
                'draggableOver': function () {
                    if (preciseDraggable.currentProps.dragging) {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('over');
                        }
                    }
                },
                'draggableLeave': function () {
                    if (!$(this).data('dropped')) {
                        $(this).removeClass('over');
                    }
                },
                'draggableDrop': function () {
                    preciseDraggable.currentProps.lastDraggable.$element.removeClass('is-dragging');
                    if (!$(this).data('dropped')) {
                        var element = preciseDraggable.currentProps.lastDraggable;
                        var draggableKey = element.data.key;
                        var droppableKey = $(this).attr('data-key')
                        $(this).removeClass('over');
                        $('.slide[data-slide="5"] .dropables').addClass('dropeado');
                        // if(draggableKey !== droppableKey){
                        //     return true;
                        // }
                        //$(this).data('dropped', true);
                        var $el = preciseDraggable.currentProps.lastDraggable.$element;
                        $(this).append('<div class="logo fs-bg" data-logo="' + $el.data("logo") +'"></div>');
                        $el.remove();

                        console.log($el.data("logo"));
                        module.exports.currentProps.finalDrags.push($el.data("logo"));
                        // if(self.$slide.find('.draggable-element').length === 0){
                            
                        // }
                        $("[data-slide='5'] .reiniciar").removeClass("oculto");
                    }
                    
                }
            });
        });

        var $droppableElements = self.$slide.find('.drop');

        cargarDrags();

        function cargarDrags(){
            console.log("cargar drgas");

            var $draggableItems = $('.slide[data-slide="5"] .draggable-items'); //toma div 
            $draggableItems.html("");

            _.forEach(draggables.items, function (v, k) {
                var $el = $('<div class="logo fs-bg draggable-element" data-logo="' + (k+1) +'" data-ani-class="wobble" data-ani-delay="200"></div>')
                    .appendTo($('.slide[data-slide="5"] .draggable-items'));

                $el.on('mouseenter', function(){
                    $(this).addClass('animations-disabled');
                });

                preciseDraggable.currentProps.$stage = $('#stage');

                var $draggable = $el;
                var draggie = preciseDraggable.setDraggable({
                    $target: $draggable,
                    data: {
                        key: $draggable.attr('data-key')
                    }
                });

                draggie.on('dragStart', function(){
                    $draggable.addClass('animations-disabled');
                    preciseDraggable.currentProps.dragging = true;
                    var $this = draggie.$element;
                    preciseDraggable.currentProps.lastDraggable = {
                        data: {
                            key: $this.attr('data-key')
                        },
                        $element: $this
                    };
                    self.$slide.find('.drop').each(function () {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('highlight');
                        }
                    })
                });

                draggie.on('dragMove', function(event){
                    draggie.overlap = preciseDraggable.getOverlap({
                        originalGroup: $draggable,
                        matchingGroup: $droppableElements
                    });

                    $droppableElements.trigger('draggableLeave');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableOver');
                    }
                })

                draggie.on('dragEnd', function(){
                    $droppableElements.removeClass('highlight over');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableDrop');
                    }
                })
            });
        }

        cargarDragsIniciales();

        function cargarDragsIniciales(){
            if (_.isObject(self.activities['Autoevaluación primera'])) {
                respuestasIniciales = self.activities['Autoevaluación primera'].respuestas[0];
                console.log(respuestasIniciales);
                console.log(respuestasIniciales.length);

                contenedor = $("[data-slide='5'] .draggable-items");

                for (var i = 0; i < respuestasIniciales.length; i++) {
                    $("[data-slide='5'] .logo[data-logo='" + respuestasIniciales[i] + "']").remove();
                    $("[data-slide='5'] .drop").append('<div class="logo fs-bg" data-logo="' + respuestasIniciales[i] +'"></div>');
                    $('.slide[data-slide="5"] .dropables').addClass('dropeado');
                    module.exports.currentProps.finalDrags.push(respuestasIniciales[i]);
                    $("[data-slide='5'] .reiniciar").removeClass("oculto");
                }   
                $("[data-slide='5'] textarea[data-respuesta='1']").text(self.activities['Autoevaluación primera'].respuestas[1]);
                $("[data-slide='5'] textarea[data-respuesta='2']").text(self.activities['Autoevaluación primera'].respuestas[2]);
                $("[data-slide='5'] textarea[data-respuesta='3']").text(self.activities['Autoevaluación primera'].respuestas[3]);
                $("[data-slide='5'] textarea[data-respuesta='4']").text(self.activities['Autoevaluación primera'].respuestas[4]);
            }
        }

        $("[data-slide='5'] .reiniciar").on("click", function () {
            $("[data-slide='5'] .drop").html("");
            $("[data-slide='5'] .drop").removeClass("dropeado");
            $('.slide[data-slide="5"] .dropables').removeClass('dropeado');

            console.log(module.exports.currentProps.finalDrags);
             module.exports.currentProps.finalDrags = [];
             console.log(module.exports.currentProps.finalDrags);

             console.log(module.exports.currentProps.newAnswers[0]);
             module.exports.currentProps.newAnswers[0] = module.exports.currentProps.finalDrags;

             cargarDrags();

             $(this).addClass("oculto");
        });

        $("[data-slide='5'] .preguntas").on("click", function () {
            $("[data-slide='5'] .botonera").removeClass("oculto");
        });

        $("[data-slide='5'] .boton-pregunta").on("click", function () {
            $this = $(this);
            $pregunta = $this.data("boton-pregunta");
            $("[data-slide='5'] .pagina").addClass("oculto");
            $("[data-slide='5'] .pagina[data-pagina='" + ($pregunta+1) + "']").removeClass("oculto");
            $("[data-slide='5'] .pagina[data-pagina='" + ($pregunta+1) + "']").addClass("visto");
            $("[data-slide='5'] .acciones").removeClass("oculto");
        });

        $("[data-slide='5'] .responder").on("click", function () {
            $("[data-slide='5'] .pagina").addClass("oculto");
            $("[data-slide='5'] .pagina[data-pagina='" + ($pregunta+1) + "']").removeClass("oculto");
            $("[data-slide='5'] .pagina[data-pagina='" + ($pregunta+1) + "']").addClass("visto");
            $("[data-slide='5'] .acciones").removeClass("oculto");
        });

        $("[data-slide='5'] .volver").on("click", function () {
            vuelve();
        });

        $("[data-slide='5'] .guardar").on("click", function () {
            module.exports.currentProps.newAnswers[0] = module.exports.currentProps.finalDrags;
            module.exports.currentProps.newAnswers[1] = module.exports.currentProps.finalAnswers[1];
            module.exports.currentProps.newAnswers[2] = module.exports.currentProps.finalAnswers[2];
            module.exports.currentProps.newAnswers[3] = module.exports.currentProps.finalAnswers[3];
            module.exports.currentProps.newAnswers[4] = module.exports.currentProps.finalAnswers[4];
            console.log(module.exports.currentProps.newAnswers);
            vuelve();
        });

        function vuelve(){
            $("[data-slide='5'] .pagina").addClass("oculto");
            $("[data-slide='5'] .acciones").addClass("oculto");
            $("[data-slide='5'] .pagina[data-pagina='1']").removeClass("oculto");
            validaFinaliza();
        }

        function validaFinaliza(){
            if ($("[data-slide='5'] .visto").length == $("[data-slide='5'] .paginita").length) {
                $("[data-slide='5'] .finalizar").removeClass("oculto");
            }
        }

        $("[data-slide='5'] .finalizar").on("click", function () {

            require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
                component: 'activities',
                method: 'addLOActivities',
                showFeedback: true,
                arguments: {
                    shortName: require('courseData.json').shortName,
                    unit: require('courseData.json').unit,
                    activityId: 'Autoevaluación tercera',
                    answers: JSON.stringify({
                        respuestas: module.exports.currentProps.newAnswers
                    })
                }
            })
            .then(function(response){
                console.log('response', response)
            })
                console.log(module.exports.currentProps.newAnswers);

                var pdfArgs = {};
                for (var i = 0; i < module.exports.currentProps.newAnswers.length; i++) {
                    pdfArgs[i] = {
                      answer: module.exports.currentProps.newAnswers[i]
                    };
                }

                console.log('pdfArgs', pdfArgs);

                var pdfArgs = $.param({
                  pdfArgs: pdfArgs
                });

            swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed modalPdf',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/slide5Pdf.hbs")( 
                        {
                            downloadArguments: "?" + pdfArgs,
                            assignLink: _.get(self, 'courseActivities.assign.2.href')
                        } 
                    ),
            }).then(function (t) {
                $("[data-slide='5'] .button-continuar").removeClass("escondido");
            });
        });

        $("[data-slide='5'] .button-continuar").on("click", function () {
            presentation.switchToSlide({
              slide: $('.slide[data-slide="1"]')
            });
            setTimeout(function () {
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'evaluacion_final': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                });
            }, 1500);
            setTimeout(function () {
                $('.slide[data-slide="5"]').data("presentation").enabled = false;
                $('.slide[data-slide="1"] .actividad[data-actividad="3"]').addClass("disabled");
            }, 2500);
        });
    }
}