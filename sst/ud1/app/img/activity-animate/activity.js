(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Símbolo2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#ED7C3F").s().p("AgFAGQgDgCAAgEQAAgDADgCQACgDADAAQAEAAACADQADACAAADQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape.setTransform(69.9,58,2.291,2.291);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#ED7C3F").s().p("AgfAJQgDAAgCgDQgDgCAAgEQAAgCADgDQACgDADAAIA+AAQAEAAACADQADADAAACQAAAEgDACQgCADgEAAg");
	this.shape_1.setTransform(85.2,58,2.291,2.291);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#ED7C3F").s().p("Ag9AJQgEAAgDgDQgCgCAAgEQAAgCACgDQADgDAEAAIB7AAQAEAAADADQACADAAACQAAAEgCACQgDADgEAAg");
	this.shape_2.setTransform(47.5,58,2.291,2.291);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#ED7C3F").s().p("AiAAJQgDAAgEgDQgCgCAAgEQAAgCACgDQAEgDADAAIEBAAQAEAAACADQADADAAACQAAAEgDACQgCADgEAAg");
	this.shape_3.setTransform(62.8,84.5,2.291,2.291);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#ED7C3F").s().p("AiAAJQgDAAgEgDQgCgCAAgEQAAgDACgDQAEgCADAAIEBAAQAEAAACACQADADAAADQAAAEgDACQgCADgEAAg");
	this.shape_4.setTransform(62.8,70.2,2.291,2.291);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#ED7C3F").s().p("AiAAJQgDAAgEgCQgCgDAAgEQAAgDACgCQAEgDADAAIEBAAQAEAAACADQADACAAADQAAAEgDADQgCACgEAAg");
	this.shape_5.setTransform(62.8,45.8,2.291,2.291);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#ED7C3F").s().p("AgGEYIjxAAQgFAAgEgDQgHgDgCgIQgBgHADgHIAagrQAJgOAAgRQAAgQgJgPQgjg5gGhEQgJhzBLhYQBLhYB0gJQA6gFA4AUQA4AUArApQBDA+ARBbIADAnIAAAPQgDB0hUBQQhSBPhxAAIgDAAgAh+jMQhVA0gXBiQgYBgA1BWQAOAXAAAbQAAAbgOAXIgJAPIDQAAQBkABBIhGQBJhFADhlQABhFglg8Qg0hVhigYQgdgHgcAAQhBAAg8Alg");
	this.shape_6.setTransform(64,64.1,2.291,2.291);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F7AD2B").s().p("AhrEDQg6gDgwAAIglACIAnhDIgZhSQgMgngFgnQgQh6A7hBQBLhTBlgQQBqgSBRBGQBbBPAMB7QANCEhmBLQgyAZhMAQQhBANg4AAIgbgBg");
	this.shape_7.setTransform(64.8,63.8,2.291,2.291);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Símbolo2, new cjs.Rectangle(0,0,128.1,128.2), null);


(lib.Símbolo1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#009F86").s().p("AgFAGQgDgDAAgDQAAgDADgDQADgCACAAQAEAAADACQACAEAAACQAAADgCADQgEADgDAAQgCAAgDgDg");
	this.shape.setTransform(58.2,58,2.291,2.291);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#009F86").s().p("AgeAJQgEAAgDgCQgCgDAAgEQAAgDACgCQADgDAEAAIA9AAQAEAAADADQACACAAADQAAAEgCADQgDACgEAAg");
	this.shape_1.setTransform(43,57.9,2.291,2.291);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#009F86").s().p("Ag9AJQgEAAgDgDQgCgCAAgEQAAgDACgDQADgCAEAAIB7AAQAEAAADACQACADAAADQAAAEgCACQgDADgEAAg");
	this.shape_2.setTransform(80.6,57.9,2.291,2.291);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#009F86").s().p("AiAAJQgDAAgDgDQgDgCAAgEQAAgDADgDQADgCADAAIEBAAQAEAAACACQADADAAADQAAAEgDACQgCADgEAAg");
	this.shape_3.setTransform(65.4,84.4,2.291,2.291);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#009F86").s().p("AiAAJQgDAAgDgDQgDgCAAgEQAAgDADgDQADgCADAAIEBAAQAEAAACACQADADAAADQAAAEgDACQgCADgEAAg");
	this.shape_4.setTransform(65.4,70.2,2.291,2.291);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#009F86").s().p("AiAAJQgDAAgDgDQgDgCAAgEQAAgDADgCQADgDADAAIEBAAQADAAADADQADACAAADQAAAEgDACQgCADgEAAg");
	this.shape_5.setTransform(65.4,45.7,2.291,2.291);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#009F86").s().p("AjADJQhThRgEhzIAAgQIAEgmQAQhaBEg/QArgpA4gUQA4gUA6AFQBzAJBMBYQBLBYgJByQgGBFgkA5QgIAOAAARQAAAQAIAOIAaAsQAEAGgCAIQgBAHgHAEQgFADgFAAIjwAAIgDAAQhxAAhThPgAg3jqQhiAXg0BWQgmA8ACBFQADBlBJBFQBIBGBkgBIDQAAIgJgQQgOgXAAgbQAAgaAOgYQA0hVgXhgQgXhihVg1Qg7gkhBAAQgcAAgeAHg");
	this.shape_6.setTransform(64.1,64.1,2.291,2.291);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#4CB9B3").s().p("AgoD3QhNgQgxgZQhmhLANiEQAMh7BbhPQBRhGBqASQBkAQBLBSQA8BCgQB6QgFAngMAnIgZBRIAnBEQgzgEhcAFIgbABQg4AAhBgNg");
	this.shape_7.setTransform(63.4,63.8,2.291,2.291);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Símbolo1, new cjs.Rectangle(0,0,128.2,128.2), null);


(lib.Interpolación4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D41554").s().p("AgUAVQgJgJgBgMQABgMAJgJQAJgJALABQANgBAIAJQAKAJgBAMQABANgKAIQgIAJgNABQgLgBgJgJgAgKgLQgFAFAAAGQAAAHAFAEQAEAFAGABQAHgBAEgFQAGgEgBgHQABgGgGgFQgEgEgHAAQgGAAgEAEg");
	this.shape.setTransform(-82.5,-76,2.291,2.291);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F7AD2B").s().p("AgJAhIAAgYIgXAAIAAgSIAXAAIAAgXIASAAIAAAXIAYAAIAAASIgYAAIAAAYg");
	this.shape_1.setTransform(84.6,-63.8,2.291,2.291);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D41554").s().p("AAbHHQgGAAgBgHQAAgGAHgBQAcgCAdgFQADgBADACQACABABADQABAHgHACQgdAFgeACgAgfHHQghgDgagFQgDgBgBgCQgCgDABgDQAAgCADgCQACgCADABQAaAFAfADQAHAAAAAHQgBAHgGAAgAiTGvQgfgKgYgNQgDgBgBgDQgBgDABgCQAEgGAGACQAaAOAbAJQAHACgCAHQgCAFgFAAgADyF6QgEgGAGgEQAWgPAYgUQAFgEAFAFQAEAGgFAEQgVATgaARIgEABQgEAAgCgDgAj+F6QgXgPgYgVQgFgFAEgFQAFgGAFAFQAWAUAYAPQAGAFgEAFQgCADgEAAIgEgBgAlXErQgTgVgRgaQgCgDABgCQAAgDADgCQAGgEAEAGQAPAXAUAXQAEAFgFAFQgCACgDAAQgDAAgCgDgAGPDPQgDgBgBgDQgBgDABgDQANgZAKgcQABgDACgBIAGgBQAHADgDAHQgKAegNAZQgCAEgEAAgAmZDIQgMgYgLgfQgCgHAHgCQAHgCACAHQAJAZANAcQADAGgGAEIgDAAQgFAAgCgEgAG2BhQgGgCABgHQAFgbADgeIAAgCQABgHAGABQAHAAAAAHIAAACQgCAbgGAgQgBAGgGAAgAm4BdQgGAAgBgGQgGgcgBgfQgBgDACgCQACgCADAAQADgBADACQACACAAADQACAdAFAdQACAGgHACgAG5gcQgCgcgGgdQgBgHAHgBQAHgBACAGQAFAdACAfQAAAGgHABQgGAAgBgHgAm/gXQgHAAAAgHIAAgBQACggAGgaQABgHAHABQADABACACQACADgBACQgFAcgDAdQgBAHgGAAgAGjiNQgMgggKgVQgDgGAGgEQACgBADABQADABABADQAMAWALAhQACAHgGACIgDAAQgFAAgBgFgAmqiKQgGgCABgHQAJgZAOgeQAEgHAGAEQACABABADQABADgBACQgNAcgJAaQgCAEgFAAgAEklLQgYgVgWgOQgCgCgBgDQgBgDACgCQAEgGAGAEQAYAQAXAUQAGAGgFAFQgCACgDAAIgFgCgADEmLQgbgOgagIQgHgDACgGQADgHAGACQAfALAYAMQAHADgDAHQgDAEgEAAgAjLmPQgBgDABgDQABgDACgBQAZgMAegKQAHgDACAHQADAHgHACQgaAJgcANIgDABQgEAAgCgEgABXmwQghgGgXgCIgBAAQgIgBABgHQAAgDADgCQACgCADABIABAAQAfACAbAGQADAAABADQACACgBADQgBAGgGAAgAhWmxQgGAAgBgFQgBgIAHgBQAdgGAegBQAHgBABAHQAAAIgHAAQgeACgbAFg");
	this.shape_2.setTransform(0,0,2.291,2.291);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-104.3,-104.3,208.6,208.6);


// stage content:
(lib.activity = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_2
	this.instance = new lib.Símbolo2();
	this.instance.parent = this;
	this.instance.setTransform(102.9,96.7,1,1,0,0,0,64,64);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regY:64.1,scaleX:1.02,scaleY:1.02,rotation:1.4,y:96.8},0).wait(1).to({scaleX:1.04,scaleY:1.04,rotation:2.7},0).wait(1).to({scaleX:1.06,scaleY:1.06,rotation:4.1,x:102.8},0).wait(1).to({scaleX:1.08,scaleY:1.08,rotation:5.5,x:102.9},0).wait(1).to({scaleX:1.09,scaleY:1.09,rotation:6.8},0).wait(1).to({scaleX:1.11,scaleY:1.11,rotation:8.2},0).wait(1).to({scaleX:1.13,scaleY:1.13,rotation:9.5,x:103},0).wait(1).to({scaleX:1.15,scaleY:1.15,rotation:10.9},0).wait(1).to({scaleX:1.17,scaleY:1.17,rotation:12.3},0).wait(1).to({scaleX:1.19,scaleY:1.19,rotation:13.6},0).wait(1).to({scaleX:1.21,scaleY:1.21,rotation:15,x:103.1},0).wait(1).to({scaleX:1.19,scaleY:1.19,rotation:13.2,x:103.2},0).wait(1).to({scaleX:1.17,scaleY:1.17,rotation:11.3},0).wait(1).to({scaleX:1.15,scaleY:1.15,rotation:9.5,x:103.3},0).wait(1).to({scaleX:1.13,scaleY:1.13,rotation:7.7,x:103.4},0).wait(1).to({scaleX:1.11,scaleY:1.11,rotation:5.8},0).wait(1).to({scaleX:1.09,scaleY:1.09,rotation:4,x:103.5},0).wait(1).to({scaleX:1.07,scaleY:1.07,rotation:2.2,x:103.6},0).wait(1).to({scaleX:1.05,scaleY:1.05,rotation:0.3},0).wait(1).to({scaleX:1.03,scaleY:1.03,rotation:-1.5,x:103.7},0).wait(1).to({scaleX:1.02,scaleY:1.02,rotation:-3.3,x:103.8},0).wait(1).to({scaleX:1,scaleY:1,rotation:-5.1,x:104},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:-7,x:103.9},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:-8.8,x:103.8},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:-10.6},0).wait(1).to({scaleX:0.92,scaleY:0.92,rotation:-12.5,x:103.7},0).wait(1).to({scaleX:0.9,scaleY:0.9,rotation:-14.3,x:103.6},0).wait(1).to({scaleX:0.88,scaleY:0.88,rotation:-16.1,x:103.5},0).wait(1).to({scaleX:0.87,scaleY:0.87,rotation:-18,x:103.4},0).wait(1).to({scaleX:0.85,scaleY:0.85,rotation:-19.8,x:103.3},0).wait(1).to({scaleX:0.83,scaleY:0.83,rotation:-21.6,x:103.2},0).wait(1).to({scaleX:0.84,scaleY:0.84,rotation:-20.1,x:103.1},0).wait(1).to({scaleX:0.86,scaleY:0.86,rotation:-18.6,x:103},0).wait(1).to({scaleX:0.88,scaleY:0.88,rotation:-17,x:102.9},0).wait(1).to({scaleX:0.89,scaleY:0.89,rotation:-15.5,x:103},0).wait(1).to({scaleX:0.91,scaleY:0.91,rotation:-14,x:103.2},0).wait(1).to({scaleX:0.92,scaleY:0.92,rotation:-12.5,x:103.3},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:-10.9,x:103.4},0).wait(1).to({scaleX:0.95,scaleY:0.95,rotation:-9.4,x:103.5},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:-7.9,x:103.6},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:-6.4},0).wait(1).to({scaleX:1,scaleY:1,rotation:-4.8},0).wait(1).to({scaleX:1.02,scaleY:1.02,rotation:-3.3},0).wait(1).to({scaleX:1.03,scaleY:1.03,rotation:-1.8},0).wait(1).to({scaleX:1.05,scaleY:1.05,rotation:-0.3,x:103.5},0).wait(1).to({scaleX:1.06,scaleY:1.06,rotation:1.3},0).wait(1).to({scaleX:1.08,scaleY:1.08,rotation:2.8,x:103.4},0).wait(1).to({scaleX:1.1,scaleY:1.1,rotation:4.3,x:103.3},0).wait(1).to({scaleX:1.11,scaleY:1.11,rotation:5.8,x:103.2},0).wait(1).to({scaleX:1.13,scaleY:1.13,rotation:7.4,x:103},0).wait(1).to({scaleX:1.14,scaleY:1.14,rotation:8.9,x:102.9},0).wait(1).to({scaleX:1.16,scaleY:1.16,rotation:10.4},0).wait(1).to({scaleX:1.17,scaleY:1.17,rotation:11.9,x:103},0).wait(1).to({scaleX:1.19,scaleY:1.19,rotation:13.5,x:103.1},0).wait(1).to({scaleX:1.21,scaleY:1.21,rotation:15},0).wait(1).to({scaleX:1.19,scaleY:1.19,rotation:14.1},0).wait(1).to({scaleX:1.18,scaleY:1.18,rotation:13.1},0).wait(1).to({scaleX:1.17,scaleY:1.17,rotation:12.2},0).wait(1).to({scaleX:1.15,scaleY:1.15,rotation:11.2,x:103},0).wait(1).to({scaleX:1.14,scaleY:1.14,rotation:10.3},0).wait(1).to({scaleX:1.13,scaleY:1.13,rotation:9.4,x:102.9},0).wait(1).to({scaleX:1.12,scaleY:1.12,rotation:8.4,x:102.8},0).wait(1).to({scaleX:1.1,scaleY:1.1,rotation:7.5,x:102.9},0).wait(1).to({scaleX:1.09,scaleY:1.09,rotation:6.6},0).wait(1).to({scaleX:1.08,scaleY:1.08,rotation:5.6,x:103},0).wait(1).to({scaleX:1.06,scaleY:1.06,rotation:4.7},0).wait(1).to({scaleX:1.05,scaleY:1.05,rotation:3.7},0).wait(1).to({scaleX:1.04,scaleY:1.04,rotation:2.8},0).wait(1).to({scaleX:1.03,scaleY:1.03,rotation:1.9,x:102.9},0).wait(1).to({scaleX:1.01,scaleY:1.01,rotation:0.9},0).wait(1).to({scaleX:1,scaleY:1,rotation:0},0).wait(1));

	// Capa_3
	this.instance_1 = new lib.Símbolo1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(153.8,147.9,1,1,0,0,0,64,64);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({regX:64.1,regY:64.1,scaleX:1.02,scaleY:1.02,rotation:-1.5,y:148},0).wait(1).to({scaleX:1.04,scaleY:1.04,rotation:-3},0).wait(1).to({scaleX:1.06,scaleY:1.06,rotation:-4.4},0).wait(1).to({scaleX:1.08,scaleY:1.08,rotation:-5.8,x:153.9},0).wait(1).to({scaleX:1.09,scaleY:1.1,rotation:-7.2,x:153.8},0).wait(1).to({scaleX:1.11,scaleY:1.12,rotation:-8.6,x:153.9},0).wait(1).to({scaleX:1.13,scaleY:1.14,rotation:-10},0).wait(1).to({scaleX:1.15,scaleY:1.16,rotation:-11.3},0).wait(1).to({scaleX:1.16,scaleY:1.17,rotation:-12.6},0).wait(1).to({scaleX:1.18,scaleY:1.19,rotation:-13.9},0).wait(1).to({scaleX:1.19,scaleY:1.21,rotation:-15},0).wait(7).to({scaleX:1.18,scaleY:1.19,rotation:-11.7},0).wait(1).to({scaleX:1.17,scaleY:1.18,rotation:-8.3},0).wait(1).to({scaleX:1.16,scaleY:1.17,rotation:-5,x:153.8},0).wait(1).to({scaleX:1.15,scaleY:1.16,rotation:-1.7,x:153.9},0).wait(1).to({scaleX:1.14,scaleY:1.15,rotation:1.5},0).wait(1).to({scaleX:1.13,scaleY:1.14,rotation:4.6},0).wait(1).to({scaleX:1.12,scaleY:1.13,rotation:7.7,x:153.8},0).wait(1).to({scaleX:1.11,scaleY:1.12,rotation:10.7},0).wait(1).to({scaleX:1.1,scaleY:1.11,rotation:13.6,x:153.9},0).wait(1).to({scaleX:1.09,scaleY:1.1,rotation:16.5,x:153.8},0).wait(1).to({scaleX:1.08,scaleY:1.09,rotation:19.3},0).wait(1).to({scaleX:1.07,scaleY:1.08,rotation:18.5},0).wait(1).to({scaleX:1.06,scaleY:1.07,rotation:17.7},0).wait(1).to({scaleX:1.05,scaleY:1.06,rotation:16.8},0).wait(1).to({scaleX:1.04,scaleY:1.05,rotation:16},0).wait(1).to({scaleX:1.03,scaleY:1.04,rotation:15.2},0).wait(1).to({scaleX:1.02,scaleY:1.03,rotation:14.4,x:153.9},0).wait(1).to({scaleX:1.02,scaleY:1.03,rotation:13.6},0).wait(1).to({scaleX:1.02,scaleY:1.03,rotation:12.9},0).wait(1).to({scaleX:1.02,scaleY:1.02,rotation:12.1},0).wait(1).to({scaleX:1.02,scaleY:1.02,rotation:11.4},0).wait(1).to({scaleX:1.01,scaleY:1.02,rotation:10.8,x:153.8},0).wait(1).to({scaleX:1.01,scaleY:1.02,rotation:10.1,x:153.9},0).wait(1).to({scaleX:1.01,scaleY:1.02,rotation:9.5,x:153.8},0).wait(1).to({scaleY:1.02,rotation:8.8,x:153.9},0).wait(1).to({scaleX:1.01,scaleY:1.02,rotation:8.2},0).wait(1).to({scaleX:1.01,scaleY:1.02,rotation:7.7,x:153.8},0).wait(1).to({scaleX:1.01,scaleY:1.01,rotation:7.1},0).wait(1).to({scaleX:1.01,scaleY:1.01,rotation:6.6},0).wait(1).to({scaleY:1.01,rotation:6.1},0).wait(1).to({scaleX:1.01,scaleY:1.01,rotation:5.6},0).wait(1).to({scaleX:1.01,scaleY:1.01,rotation:5.1},0).wait(1).to({scaleY:1.01,rotation:4.6},0).wait(1).to({scaleX:1.01,scaleY:1.01,rotation:4.2},0).wait(1).to({scaleX:1,scaleY:1.01,rotation:3.8,x:153.9},0).wait(1).to({scaleY:1.01,rotation:3.4,x:153.8},0).wait(1).to({scaleX:1,scaleY:1.01,rotation:3,x:153.9},0).wait(1).to({rotation:2.7,x:153.8},0).wait(1).to({scaleX:1,scaleY:1,rotation:2.4},0).wait(1).to({scaleY:1,rotation:2.1,x:153.9},0).wait(1).to({rotation:1.8},0).wait(1).to({scaleX:1,scaleY:1,rotation:1.5,x:153.8},0).wait(1).to({rotation:1.3},0).wait(1).to({scaleY:1,rotation:1.1},0).wait(1).to({scaleX:1,rotation:0.9},0).wait(1).to({scaleY:1,rotation:0.7},0).wait(1).to({rotation:0.5},0).wait(1).to({rotation:0.4,x:153.9},0).wait(1).to({scaleX:1,rotation:0.3,x:153.8},0).wait(1).to({scaleY:1,rotation:0.2},0).wait(1).to({rotation:0.1},0).wait(1).to({rotation:0},0).wait(1).to({x:153.9},0).wait(2));

	// Capa_4
	this.instance_2 = new lib.Interpolación4("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(126.6,122.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({rotation:360},71).wait(1));

	// Capa_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D41554").s().p("AkgEhQh4h4AAipQAAipB4h3QB4h4CoAAQCpAAB4B4QB4B3AACpQAACph4B4Qh4B4ipAAQioAAh4h4g");
	this.shape.setTransform(126.5,122.4,2.291,2.291);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(72));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(147.3,141.6,208.6,208.6);
// library properties:
lib.properties = {
	id: '491324DABDC64E69A3DF5C32672750EB',
	width: 250,
	height: 247,
	fps: 24,
	color: "#FFFFFF",
	opacity: 0.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['491324DABDC64E69A3DF5C32672750EB'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;