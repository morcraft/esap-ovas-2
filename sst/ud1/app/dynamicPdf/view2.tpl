<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PLANEACIÓN ESTRATÉGICA</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<style>
    img {
        width: 100%;
    }
    @font-face {
        font-family: 'BebasNeue';
        src: url('../fonts/BebasNeue.otf');
    }
    @font-face {
        font-family: 'FFLight';
        src: url('../fonts/FF_DIN_Pro_Light.otf');
    }
    @font-face {
        font-family: 'FFBold';
        src: url('../fonts/FF_DIN_Pro_Bold.otf');
    }
    @font-face {
        font-family: 'FFBCondBold';
        src: url('../fonts/FF_DIN_Pro_Cond_Bold.otf');
    }
    @font-face {
        font-family: 'FFCondRegular';
        src: url('../fonts/FF_DIN_Pro_Cond_Regular.otf');
    }
    @font-face {
        font-family: 'FFCondLight';
        src: url('../fonts/FF_DIN_Pro_Cond_Light.otf');
    }
    @font-face {
        font-family: 'FFLight';
        src: url('../fonts/FF_DIN_Pro_Light.otf');
    }
    @font-face {
        font-family: 'FFThin';
        src: url('../fonts/FF_DIN_Pro_Thin.otf');
    }
    @font-face {
        font-family: 'Wingding';
        src: url('../fonts/wingding.ttf');
    }
    @font-face {
        font-family: 'century';
        src: url('../fonts/CenturyGothic.ttf');
    }
    @font-face {
        font-family: 'gothiddd';
        src: url('../fonts/GOTHIC.ttf');

    }
    @font-face {
        font-family: 'myriadRegular';
        src: url('../fonts/MyriadPro-Regular.otf');
    }

    html {

    }

    body {
        font-size: 12px;
        font-family: 'gothiddd';
    }

    h2 {
        font-family: 'FFDINPro-Bold';
        font-size: 20px;
        font-weight: 100;
        text-align: center;
        color: #033449;
    }

    p {
        font-size: 20px;
        color:#033449;
        font-family: 'gothiddd';
    }

    @page {
        margin: 100px 25px;
    }

    header {
        position: fixed;
        top: -60px;
        left: 0px;
        right: 0px;
        height: 55px;
        background-color: #0075BF;
        padding: 10px;
        border-radius: 20px;
    }

    footer {
        position: fixed;
        bottom: -60px;
        left: 0px;
        right: 0px;
        font-size: 12px;
        font-family: 'FFDINProCond-Bold';
        text-align: center;
        background-color: #0075BF;
        color: #FFFFFF;
        padding: 10px;
        border-radius: 10px;
    }

    header h1 {
        font-family: 'FFDINPro-Bold';
        font-size: 16px;
        font-weight: 100;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #FFF;
        padding-left: 80px;

    }

    header h2 {
        font-family: 'FFDINPro-Bold';
        font-size: 14px;
        font-weight: 100;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #FFFFFF;
        padding-left: 80px;
    }

    #esap-logo {
        position: absolute;
        width: 60px;
    }

    .question-text {
        font-family: 'FFDINPro-Light';
        color: #033449;
        font-size: 16px;
    }

    .date {
        font-family: 'FFDINPro-Light';
        color:#033449;
        text-align: right;
        font-size: 8px;
        border-bottom: solid #033449 1px dashed;
        margin-bottom: 10px;
    }

    .date p {
        font-size: 12px;
        font-family: 'FFDINPro-Bold';
    }

    .date b {
        font-family: 'FFDINPro-Bold';
        font-weight: inherit;
    }

    .quiz-title {
        font-family: 'FFDINPro-Light';
        margin: 0 18px 0 10px;
        padding: 0;
        font-size: 24px;
    }

    .contRtaUno {
        width: 100%;
        height: auto;
    }

    .contRtaUno h2 {
        padding: 0%;
        margin: 0%;
        font-size: 1.5em;
    }
    .contRtaUno p {
        padding: 4px;
        margin: 0%;
        font-size: 2em;
        text-align: center;
        border: solid 2px  #0075BF;
        font-family: 'FFDINPro-Light';
        margin-top: 10px;
    }
    .contRtaUno ul {
        border: solid 2px  #0075BF;
    }

    .contRtaUno ul li {
        padding: 0%;
        margin: 0%;
        font-size: 2em;
        text-align: center;
        font-family: 'FFDINPro-Light';
        color: #033449;
        list-style: none;
        margin-bottom: 10px;
    }
    .contRtaUno p span{
        font-family: 'FFDINPro-Bold';
    }





</style>

<body>
    <header>
        <img src="assets/img/esap_logo.png" id="esap-logo">
        <h1>Obligaciones y responsabilidades de los actores del Sistema de Gestión de la Seguridad y Salud en el Trabajo (SG-SST), Decreto 1072 de 2015</h1>
        <h2>Seguridad y Salud en el Trabajo</h2>
    </header>
    <div class="date">
        <p>Hora de generación:
            <b>{$date}</b>
        </p>
    </div>

    <h2 class="quiz-title">Respuestas Actividad 2</h2>
    <br>
    <div class="contRtaUno">
        <h2>PASO 1: Realice un análisis de las obligaciones y responsabilidades de los actores del Sistema de Gestión de la Seguridad y Salud en el Trabajo del Decreto 1072 de 2015.</h2>
        <p>{$r1[0]}</p>
        <br>
        <br>
        <h2>PASO 2: Establezca las acciones de un plan de intervención en seguridad y salud en el trabajo, teniendo en cuenta el área de la empresa, la oportunidad de mejora, la propuesta de intervención y el objetivo de la propuesta y los responsables.</h2>
        <p>{$r1[1]}</p>
        <br>
        <br>
        <h2>PASO 3: Con base en las obligaciones y responsabilidades definidas en el Decreto 1072 de 2015 para los actores del Sistema de Gestión de la Seguridad y Salud en el Trabajo, defina: Oportunidades de mejora y acciones encaminadas al fortalecimiento de la gestión del talento humano y organizacional.</h2>
        <p>{$r1[2]}</p>
        <br>
        <br>
    </div>
    <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>

</body>

</html>
