'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
  module.exports = {
    currentProps:{
      seenSlides: {},
      finished: false
  },
  onEnter:function(){
      $('audio').css('display' , 'none');
  },
    addEventListeners: function () {
        var self = this;
        var timed = 0
        $('.disc img').each(function(){
          var $img = this;
          setTimeout(function () {
            $($img).css('display' , 'block')
            timed++
            $($img).css('animation-delay' , ''+timed+'s')
          }, 1000);
        })

          $('.disc').on('click' , function(){


              $('audio').css('display' , 'none');
              $('audio').each(function(){
                  this.pause();
                  this.currentTime = 0;
              });
              var data_audio = $(this).data("disc");
              var audioclass = $('audio'+data_audio+'');
              var audioUno = document.getElementById('audio'+data_audio+'');
              console.log(audioUno);
              if(data_audio !== 5){
                audioUno.play();
                $(audioUno).css('display' , 'block');
                module.exports.currentProps.seenSlides[data_audio]= true;
                module.exports.checkModals();
              }
              $(audioUno).css('display' , 'block');

          })

        $('.disc[data-disc="5"]').on("click", function(){
          swal({
              target: stageResize.currentProps.$stage.get(0),
              customClass: 'ova-themed biggest modal_s6',
              showCloseButton: true,
              showConfirmButton: false,
              html: require("templates/modal_s6_1.hbs")()
           })
            .then(function () {
              var numero = 5
              module.exports.currentProps.seenSlides[numero] = true;
              module.exports.checkModals();
            })
            $('.numero').on('click' , function(){
                $('audio').css('display' , 'none');
                $('audio').each(function(){
                    this.pause();
                    this.currentTime = 0;
                });
                var data_numero = $(this).data("numero");
                console.log(data_numero);
                var audiodos = document.getElementById('audiomodal'+data_numero+'');
                  audiodos.play();
                  $(audiodos).css('display' , 'block');
            })
        })
        $('.btn_cont_S5').on('click' , function(){
          presentation.switchToSlide({
              slide: $('.slide[data-slide="1"]')
          });
          self.ovaProgress.updateFinishedTopics({
              topics: {
                  'Recursos 3': true
              },
              action: 'add',
              courseShortName: courseData.shortName,
              courseUnit: courseData.unit
          })
        })
    },
    checkModals: function () {
        var self = this;
        if (module.exports.currentProps.finished == true) {
            return true;
        }
        if (_.size(module.exports.currentProps.seenSlides) == 5) {
          $('.btn_cont_S5').css('visibility' , 'visible');
        }
    }
}
