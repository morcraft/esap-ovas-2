'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');

  module.exports = {
    addEventListeners: function () {
        var self = this;
        $('.archivo').on('click', function(){
          $('.archivo').removeClass('archivoActive');
          $('.cont-paso').css('display', 'none');
          $('.cont-enlaces').css('display', 'none');
          var $archivo = $(this).data('archivo');
          $('.cont-paso[data-paso="'+$archivo+'"]').css('display', 'block');
          if($archivo <= 3){
            $('.cont-enlaces').css('display', 'block');
          }else {
            $('.cont-enlaces').css('display', 'none');
          }
          $(this).addClass('archivoActive');
        });
        $('.computador').on("click", function(){
          var $pasoarray= new Array()
          $('.cont-paso .text-paso textarea').each(function(){
            var $pasotexto = $(this).val();
            console.log($pasotexto);
            $pasoarray.push($pasotexto);
            console.log($pasoarray);
          })
          swal({
              target: stageResize.currentProps.$stage.get(0),
              customClass: 'ova-send-activity',
              showCloseButton: true,
              showConfirmButton: false,
              html: require("templates/modal_act_2.hbs")({
                parametros: $.param({
                  r1: $pasoarray
                }),
                assignLink: _.get(self, 'courseActivities.forum.0.href')
              })
            })
            .then(function (value) {
              console.log(self);
              self.ovaProgress.updateFinishedTopics({
                  topics: {
                      'Actividad 2': true
                  },
                  action: 'add',
                  courseShortName: courseData.shortName,
                  courseUnit: courseData.unit
              })
              presentation.switchToSlide({
                  slide: $('.slide[data-slide="1"]')
              });
          })
    });
}
}
