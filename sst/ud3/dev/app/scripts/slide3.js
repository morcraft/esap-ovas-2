const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const draggablesCirculos = require("draggablesCirculosActividad2.json");
const draggablesEquis = require("draggablesEquisActividad2.json");
const preciseDraggable = require('precise-draggable');
const slidesManager = require('slidesManager.js');

module.exports = {

    currentProps: {
        answers: [],
        elementos: 18
    },

    onEnter: function(){
        swal({
            target: stageResize.currentProps.$stage.get(0),
            allowOutsideClick: false,
            allowEscapeKey: false,
            customClass: 'ova-themed modal3',
            showCloseButton: false,
            showConfirmButton: false,
            html: require("templates/slide3Modal.hbs")(),
        });

        $('.modal3  .explorar').on("click", function () {
            swal.close();
            presentation.switchToSlide({
              slide: $('.slide[data-slide="1"]')
            });
        });

        $('.modal3  .continuar').on("click", function () {
            swal.close();
        });
    },  

    addEventListeners: function(){

        var self = this;
        // var offsetCirculo;
        // var xPosCirculo;
        // var yPosCirculo;
        // var dragsCirculo = [];

        // var offsetEquis;
        // var xPosEquis;
        // var yPosEquis;
        // var dragsEquis = [];

        var answersImagen = [];
        var answers = [];

        $('.slide[data-slide="3"]  textarea').on("keyup", function () {
            $this = $(this);
            $respuesta = $this.data("respuesta");

            answers[$respuesta] = $this.val();
        });

        for (var i = 0; i < module.exports.currentProps.elementos; i++) {
            $('<div class="drop"/ data-drop="' + (i+1) + '">')
            .appendTo($('.slide[data-slide="3"] .imagen .izquierda .dropeable'))
            .on({
                'draggableOver': function () {
                    if (preciseDraggable.currentProps.dragging) {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('over');
                        }
                    }
                },
                'draggableLeave': function () {
                    if (!$(this).data('dropped')) {
                        $(this).removeClass('over');
                    }
                },
                'draggableDrop': function () {
                    preciseDraggable.currentProps.lastDraggable.$element.removeClass('is-dragging');
                    if (!$(this).data('dropped')) {
                        var element = preciseDraggable.currentProps.lastDraggable;
                        var draggableKey = element.data.key;
                        var droppableKey = $(this).attr('data-key')
                        $(this).removeClass('over');
                        $('.slide[data-slide="3"] .dropables').addClass('dropeado');
                        // if(draggableKey !== droppableKey){
                        //     return true;
                        // }
                        $(this).data('dropped', true);
                        var $el = preciseDraggable.currentProps.lastDraggable.$element;

                        var tipo;

                        if ($el.data("tipo") == "circulo") {
                            //$el.css("top", (yPosCirculo-210) + "px");
                            //$el.css("left", (xPosCirculo-120) + "px");
                            //$el.addClass("circulo");
                            $(this).append("<div class='circulo fs-bg'></div>");
                            //console.log($el.data("circulo"));
                        }
                        if ($el.data("tipo") == "equis") {
                            //$el.css("top", (yPosEquis-210) + "px");
                            //$el.css("left", (xPosEquis-120) + "px");
                            //$el.addClass("equis");
                            $(this).append("<div class='equis fs-bg'></div>");
                            //console.log($el.data("equis"));
                        }
                        $el.remove();

                        answersImagen[$(this).data("drop")-1] = $el.data("tipo");

                        console.log($(this).data("drop"));
                        console.log(answersImagen);

                        $("[data-slide='3'] .reiniciar").removeClass("oculto");
                    }
                    
                }
            });
        }

        var $droppableElements = self.$slide.find('.drop');

        cargarDragsCirculos();

        function cargarDragsCirculos(){

            var $draggableItems = $('.slide[data-slide="3"] .draggable-items[data-drags="circulos"]'); //toma div 
            $draggableItems.html("");

            _.forEach(draggablesCirculos.items, function (v, k) {
                var $el = $('<div class="circulo fs-bg draggable-element" data-circulo="' + k + '" data-tipo="circulo"></div>')
                    .appendTo($('.slide[data-slide="3"] .draggable-items[data-drags="circulos"]')); 

                $el.on('mouseenter', function(){
                    $(this).addClass('animations-disabled');
                });

                preciseDraggable.currentProps.$stage = $('#stage');

                var $draggable = $el;
                var draggie = preciseDraggable.setDraggable({
                    $target: $draggable,
                    data: {
                        key: $draggable.attr('data-key')
                    }
                });

                draggie.on('dragStart', function(){
                    $draggable.addClass('animations-disabled');
                    preciseDraggable.currentProps.dragging = true;
                    var $this = draggie.$element;
                    preciseDraggable.currentProps.lastDraggable = {
                        data: {
                            key: $this.attr('data-key')
                        },
                        $element: $this
                    };
                    self.$slide.find('.drop').each(function () {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('highlight hoverable');
                        }
                    })
                });

                draggie.on('dragMove', function(event){
                    draggie.overlap = preciseDraggable.getOverlap({
                        originalGroup: $draggable,
                        matchingGroup: $droppableElements
                    });

                    $droppableElements.trigger('draggableLeave');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableOver');
                        // offsetCirculo = $draggable.offset();
                        // xPosCirculo = offsetCirculo.left;
                        // yPosCirculo = offsetCirculo.top;
                        // console.log(xPos);
                        // console.log(yPos);
                    }
                })

                draggie.on('dragEnd', function(){
                    $droppableElements.removeClass('highlight over');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableDrop');
                    }
                })
            });
        }

        cargarDragsEquis();

        function cargarDragsEquis(){

            var $draggableItems = $('.slide[data-slide="3"] .draggable-items[data-drags="equis"]'); //toma div 
            $draggableItems.html("");

            _.forEach(draggablesEquis.items, function (v, k) {
                var $el = $('<div class="equis fs-bg draggable-element" data-equis="' + k + '" data-tipo="equis"></div>')
                    .appendTo($('.slide[data-slide="3"] .draggable-items[data-drags="equis"]')); 

                $el.on('mouseenter', function(){
                    $(this).addClass('animations-disabled');
                });

                preciseDraggable.currentProps.$stage = $('#stage');

                var $draggable = $el;
                var draggie = preciseDraggable.setDraggable({
                    $target: $draggable,
                    data: {
                        key: $draggable.attr('data-key')
                    }
                });

                draggie.on('dragStart', function(){
                    $draggable.addClass('animations-disabled');
                    preciseDraggable.currentProps.dragging = true;
                    var $this = draggie.$element;
                    preciseDraggable.currentProps.lastDraggable = {
                        data: {
                            key: $this.attr('data-key')
                        },
                        $element: $this
                    };
                    self.$slide.find('.drop').each(function () {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('highlight');
                        }
                    })
                });

                draggie.on('dragMove', function(event){
                    draggie.overlap = preciseDraggable.getOverlap({
                        originalGroup: $draggable,
                        matchingGroup: $droppableElements
                    });

                    $droppableElements.trigger('draggableLeave');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableOver');
                        offsetEquis = $draggable.offset();
                        xPosEquis = offsetEquis.left;
                        yPosEquis = offsetEquis.top;
                        // console.log(xPos);
                        // console.log(yPos);
                    }
                })

                draggie.on('dragEnd', function(){
                    $droppableElements.removeClass('highlight over');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableDrop');
                    }
                })
            });
        }

        $("[data-slide='3'] .reiniciar").on("click", function () {
            $("[data-slide='3'] .drop").html("");
            $("[data-slide='3'] .drop").removeClass("dropeado");
            $("[data-slide='3'] .drop").data('dropped', false);

            console.log(answersImagen);
             answersImagen = [];
             console.log(answersImagen);
            module.exports.currentProps.answers[0] = answersImagen;
            console.log(module.exports.currentProps.answers);

             cargarDragsCirculos();
             cargarDragsEquis();

             $(this).addClass("oculto");
        });

        $("[data-slide='3'] .tablinks").on("click", function () {
            $this = $(this);
            //$this.addClass("visto");
            $tab = $this.data("tab");

            $("[data-slide='3'] .tabcontent").css("display", "none");
            $("[data-slide='3'] .tablinks").removeClass("active");
            $("[data-slide='3'] #" + $tab).css("display", "block");
            
            $this.addClass("active");

            // if ($("[data-slide='3'] .visto").length == $("[data-slide='3'] .tablinks").length) {
            //     $("[data-slide='3'] .button-continuar").removeClass("escondido");
            // }
        });

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();

        $("[data-slide='3'] .boton-pregunta").on("click", function () {
            $this = $(this);
            cargarEnlace($this, "pregunta", 1);
        });

        $("[data-slide='3'] .boton-enlace").on("click", function () {
            $this = $(this);
            cargarEnlace($this, "enlace", 2);
        });

        function cargarEnlace($this, tipo, cual){
            $pregunta = $this.data("boton-" + tipo);
            $("[data-slide='3'] .botonera[data-n='" + cual + "']").find("div").removeClass("activo");
            $this.addClass("activo");

            $("[data-slide='3'] ." + tipo).addClass("oculto");
            $("[data-slide='3'] ." + tipo + "[data-" + tipo + "='" + $pregunta + "']").removeClass("oculto");
        }

        $("[data-slide='3'] .caso").on("click", function () {
            $("[data-slide='3'] .contenedor").addClass("oculto");
            $("[data-slide='3'] .imagen").removeClass("oculto");
        });

        $("[data-slide='3'] .volver").on("click", function () {
            $("[data-slide='3'] .contenedor").removeClass("oculto");
            $("[data-slide='3'] .imagen").addClass("oculto");
            cargarPagina(1);
        });

        $("[data-slide='3'] .guardar").on("click", function () {
            $("[data-slide='3'] .contenedor").removeClass("oculto");
            $("[data-slide='3'] .imagen").addClass("oculto");
             module.exports.currentProps.answers[0] = answersImagen;
            console.log(module.exports.currentProps.answers);
            cargarPagina(2);
        });
        $("[data-slide='3'] .matriz").on("click", function () {
            cargarPagina(3);
        });

        function cargarPagina(pagina){
            $("[data-slide='3'] .pagina").removeClass("activa");
            $("[data-slide='3'] .pagina[data-pagina='" + pagina + "']").addClass("activa");
        }

        $("[data-slide='3'] .continuar").on("click", function () {

            module.exports.currentProps.answers[0] = answersImagen;
            module.exports.currentProps.answers[1] = answers[1];
            module.exports.currentProps.answers[2] = answers[2];
            module.exports.currentProps.answers[3] = answers[3];

            console.log(module.exports.currentProps.answers);
        });

        $("[data-slide='3'] .finalizar").on("click", function () {

            require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
                component: 'activities',
                method: 'addLOActivities',
                showFeedback: true,
                arguments: {
                    shortName: require('courseData.json').shortName,
                    unit: require('courseData.json').unit,
                    activityId: 'actividad segunda',
                    answers: JSON.stringify({
                        respuestas: module.exports.currentProps.answers
                    })
                }
            })
            .then(function(response){
                console.log('response', response);
            })

            var pdfArgs = {};
            for (var i = 0; i < module.exports.currentProps.answers.length; i++) {
                pdfArgs[i] = {
                  answer: module.exports.currentProps.answers[i]
                };
            }

            console.log('pdfArgs', pdfArgs);

            var pdfArgs = $.param({
              pdfArgs: pdfArgs 
            });

            swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed modalPdf',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/slide3Pdf.hbs")( 
                        {
                            downloadArguments: "?" + pdfArgs,
                            assignLink: _.get(self, 'courseActivities.assign.1.href')
                        } 
                    ),
            }).then(function (t) {
                $("[data-slide='3'] .button-continuar").removeClass("escondido");
            });
        });

        $("[data-slide='3'] .button-continuar").on("click", function () {
            presentation.switchToSlide({
              slide: $('.slide[data-slide="1"]')
            });
            setTimeout(function () {
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'actividad_2': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                });
            }, 1500);
            setTimeout(function () {
                $('.slide[data-slide="1"] .recurso[data-recurso="3"]').addClass("click-me");
            }, 3000);
        });
    }
}