const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

	currentProps: {
	    title: ["Fortalecimiento organizacional", 
                "Gestión organizacional", 
                "Comités de apoyo", 
                "Copasst", 
                "Inspección de seguridad", 
                "Investigación de accidentes  e incidentes de trabajo"],
	    text: ["El fortalecimiento organizacional hace referencia al proceso orientado al desarrollo de las capacidades de la organización comunitaria, que reconoce el potencial de participación de las personas en la construcción del desarrollo sostenible de la organización.", 
            "La gestión organizacional tiene como objetivo principal promover la seguridad y salud, y prevenir la ocurrencia de accidentes de trabajo y la aparición de enfermedades laborales. Por lo cual es fundamental la creación de los comités de apoyo.", 
            "Un comité de apoyo es un conjunto de colaboradores que representan a la organización en aspectos del ámbito institucional, cuenta con determinadas funciones y alcances. Los comités en la seguridad y salud en el trabajo son: emergencias, seguridad vial, convivencia laboral y el Copasst.", 
            "El Copasst, es el organismo conformado por representantes del empleador y representantes de los trabajadores, que se encarga de la promoción y vigilancia de las normas y reglamentos de seguridad y salud en el trabajo dentro de la empresa, a través de actividades de promoción, información y divulgación.", 
            "Los procesos de inspección de seguridad y salud en el trabajo son una técnica para el análisis de las condiciones de seguridad, la cual se debe ejecutar por medio de observaciones directas a las instalaciones locativas, equipos, máquinas, herramientas y a los propios procesos productivos que ejecuta la organización.", 
            "La investigación de accidente o incidente de trabajo, es el proceso sistemático de determinación y ordenación de causas, hechos o situaciones que generaron o favorecieron la ocurrencia del accidente o incidente, que se realiza con el objeto de prevenir su repetición, mediante el control de los riesgos que lo produjeron."]
	},

    addEventListeners: function(){

    	var self = this;

        $("[data-slide='4'] .info").on("click", function () {
            if (!$(this).hasClass("disabled")) {
                $this = $(this);
                $this.addClass("visto");

                $boton = $this.data("info");

                swal({
                    target: stageResize.currentProps.$stage.get(0),
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    customClass: 'ova-themed slide4Modal',
                    showCloseButton: true,
                    showConfirmButton: false,
                    title: module.exports.currentProps.title[$boton-1],
                    text: module.exports.currentProps.text[$boton-1],
                }).then(function (t) {
                    $this.removeClass("noColor");
                    setTimeout(function () {
                        $("[data-slide='4'] .info[data-info='" + ($boton+1) + "']").removeClass("noColor");
                    }, 700);
                    setTimeout(function () {
                        $("[data-slide='4'] .info[data-info='" + ($boton+1) + "']").addClass("click-me");
                        $("[data-slide='4'] .info[data-info='" + ($boton+1) + "']").removeClass("disabled");
                    }, 1500);
                    setTimeout(function () {
                        if ($("[data-slide='4'] .visto").length == $("[data-slide='4'] .info").length) {
                            $("[data-slide='4'] .button-continuar").removeClass("escondido");
                        }
                    }, 2500);
                });
            }
        });

        $("[data-slide='4'] .button-continuar").on("click", function () {

            var targetConcepts = _.pick(concepts, [
                "Apoyo organizacional",
                "Evaluación del riesgo",
                "Valoración de los riesgos",
                "Mejora continua"
            ]);
            
            self.ovaConcepts.showNewConceptsModal({
                title: "¿Cuál es la relación de estos términos con la salud en el trabajo?",
                subtitle: "Lea los conceptos e imagine una situación cotidiana donde se usan. Compare sus imágenes con las definiciones en el cofre de conceptos.",
                concepts: targetConcepts
            })
            .then(function (t) {

            	presentation.switchToSlide({
            	  slide: $('.slide[data-slide="1"]')
            	});
                setTimeout(function () {
                    self.ovaConcepts.updateConcepts({
                        concepts: targetConcepts,
                        action: "insert",
                        courseShortName: courseData.shortName,
                        courseUnit: courseData.unit
                    });
                }, 1500);
            	setTimeout(function () {
            		self.ovaProgress.updateFinishedTopics({
            		    topics: {
            		        'recurso_3': true
            		    },
            		    action: 'add',
            		    courseShortName: courseData.shortName,
            		    courseUnit: courseData.unit
            		});
            	}, 3000);
            	setTimeout(function () {
            		$("[data-slide='1'] .actividad[data-actividad='3']").addClass("click-me");
            	}, 4500);
            });
	   });
    }
}