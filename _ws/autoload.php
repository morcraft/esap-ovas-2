<?php
    spl_autoload_register(function($traitName) {
        $fileName = stream_resolve_include_path('Traits/' . $traitName . '.php');
        if ($fileName !== false) {
            require $fileName;
        }
    });
?>