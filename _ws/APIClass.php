<?php
/**
 * APIClass.php
 * @author Christian Arias <cristian1995amr@gmail.com>
 */

ini_set('display_errors', 1);
error_reporting(E_ALL);
require 'vendor/autoload.php';
require 'autoload.php';

/**
 * Clase que contiene todos los métodos de la API
 *
 * @version v1.0.1
 *
 */

class API{
    use Validator, Concepts, Course, CourseActivities, Media, Topics, User, Team, Activities;
    /** 
     * Información enviada a la API.
     * @var array
     */
    private $data;

    /**
     * Clase sobre la cual se interactúa con la base de datos.
     * @var class
     */
    private $DB;

    /**
     * Variable base de Moodle que contiene información y métodos útiles.
     * @var class
     */
    private $CFG;

    /**
     * Constructor de la clase.
     * Se verifica la integridad de la información recibida y se asignan las propiedades de la clase.
     * @param array $arguments Un arreglo asociativo que como mínimo debe tener las propiedades *data* y *DB*.
     * 
     * @throws CustomException 0x000 Si no se recibe la propiedad *data* dentro de __$arguments__
     * @throws CustomException 0x001 Si no se recibe la propiedad *DB* dentro de __$arguments__
     * @throws CustomException 0x002 Si no se recibe la propiedad *CFG* dentro de __$arguments__
     * 
     */

    public function __construct(array $arguments = array()){
        if (!isset($arguments['data'])) {
            throw new CustomException('No information was received', '0x000');
        }

        if (!isset($arguments['DB'])) {
            throw new CustomException('No Moodle database manager instance was received', '0x001');
        }

        if (!isset($arguments['CFG'])) {
            throw new CustomException('No Moodle CFG variable was received.', '0x002');
        }


        if (!ini_get('date.timezone')) {
            date_default_timezone_set('America/Bogota');
        }

        //$this->data = $this->validator->sanitize($arguments['data']);
        $this->DB = $arguments['DB'];
        $this->CFG = $arguments['CFG'];
        $this->validator = new GUMP();
        $this->addValidators();
    }

    /**
     * Este método procesa todas las peticiones hechas.
     * Se encarga de consultar si existe el método dentro de la clase, y pasar los argumentos obtenidos desde la petición a dicho método, de existir.
     *
     * @api
     * @param array $arguments Un arreglo asociativo que como mínimo debe tener la propiedad *method*.
     * @throws CustomException 1x000 Si no se recibe la propiedad *method* dentro de __$arguments__.
     * @throws CustomException 1x001 Si el método solicitado no existe dentro de la clase.
     * @return array Arreglo que contiene la respuesta obtenida desde el método ejecutado dentro de la clase.
     */

    protected function executeQuery(array $arguments = Array()){
        $this->validateArguments($arguments, array(
            'method' => 'required|alpha',
        ));

        if (!method_exists($this, $arguments['method'])) {
            throw new CustomException("Method '{$arguments['method']}' doesn't exist", '1x001');
        }

        try {
            if (isset($arguments['arguments']) && is_array($arguments['arguments'])) {
                $response = $this->{$arguments['method']}($arguments['arguments']);
            } else {
                $response = $this->{$arguments['method']}();
            }
            return array(
                'response' => $response,
            );
        } catch (Exception $e) {
            throw new CustomException($e->getMessage(), method_exists($e, 'getCustomCode') ? $e->getCustomCode() : $e->getCode());
        }
    }

    /**
     * Este método procesa todas las peticiones hechas.
     * Se encarga de consultar si existe el método dentro de la clase, y pasar los argumentos obtenidos desde la petición a dicho método, de existir.
     *
     * @throws CustomException 2x000, si no se recibe un arreglo como propiedad *queries* de __$arguments__.
     * @throws CustomException 2x001, si la propiedad *queries* dentro de __$arguments__ no tiene longitud.
     * @throws CustomException 2x003, si no existe la propiedad *method* dentro de uno de los queries.
     * @api
     * @param array $arguments Un arreglo que puede contener uno o varios queries.
     */

    public function processRequest(array $arguments = Array()){
        if (isset($arguments['queries'])) {
            if (!is_array($arguments['queries'])) {
                throw new CustomException("Wrong data type for property 'queries'", "2x000");
            }
            if (!count($arguments['queries'])) {
                throw new CustomException("No queries were received", "2x001");
            }
            $response = array(
                'response' => array()
            );

            foreach ($arguments['queries'] as $k => $v) {
                if (!is_array($v)) {
                    throw new CustomException("Only arrays are allowed in property 'queries'", "2x002");
                }
                if (!isset($v['method'])) {
                    throw new CustomException("Missing property 'method' in query in position {$k}", "2x003");
                }

                try{
                    $response['response'][$k] = $this->executeQuery($v);
                }
                catch(CustomException $e){
                    throw new CustomException("Error while executing query {$k}. {$e->getMessage()}", $e->getCustomCode());
                }
            }

            return $response;
        } else {
            return $this->executeQuery($arguments);
        }
    }
}
