<?php

trait Media{

    /**
     * Este método se encarga de obtener los IDs de recursos multimediales del OVA
     * @param array $arguments  Un arreglo asociativo que debe contener la propiedad *unit*.
     *
     * @api
     *
     * @throws CustomException __11x000__, si ocurre un error en la obtención de información
     * @throws CustomException __11x001__, si no se encuentran recursos asociados
     * @throws CustomException __11x002__, si no se encuentra el curso.
     * 
     * @return array
     */

    protected function getLOMedia(array $arguments = array()){
        $this->validateArguments($arguments, array(
            'unit' => 'required|numeric'
        ));

        $courseId = $this->getCourseIdByShortName($arguments);

        if (is_string($courseId)) {
            $sql = "SELECT mediatype, mediaid
                      FROM mdl_media
                     WHERE courseId = ?
                       AND unit     = ?";

            try{
                $media = $this->DB->get_records_sql($sql, array(
                    $courseId,
                    $arguments["unit"],
                ));
            }
            catch(Exception $e){
                throw new CustomException("There was an error while fetching media sources.", "11x000");
            }

            if (is_array($media)) {
                return $media;
            } 

            throw new CustomException("No media sources were found for course {$arguments['shortName']} with ID {$courseId} and unit {$arguments['unit']}", "11x001");
        } 

        throw new CustomException("Course wasn't found", "11x002");
    }

    /**
     * Este método se encarga de adicionar un recurso multimedial en el OVA
     * @param array $arguments
     *
     * @api
     * @throws CustomException __12x000__, si los recursos no están en un array
     * @throws CustomException __12x001__, si no se recibe la propiedad *mediaid* dentro de uno de los recursos multimediales
     * @throws CustomException __12x002__, si no se recibe la propiedad *mediatype* dentro de uno de los recursos multimediales
     * @throws CustomException __12x003__, si ocurre un error en la inserción de datos.
     *
     * @return bool
     */

    protected function addLOMedia(array $arguments = array()){
        $courseId = $this->getCourseIdByShortName($arguments);

        $this->deleteLOMedia($arguments);

        $sql = "INSERT INTO mdl_media
                           (mediaid, mediatype, courseid, unit)
                   VALUES __VALUES__";

        $values = array();
        $sqlArguments = array();
        foreach ($arguments['media'] as $key => $value) {
            if (!is_array($value)) {
                throw new CustomException("An array with 'mediaid' and 'mediatype' fields was expected in item {$key}", "12x000");
            }
            if (!isset($value['mediaid']) || !is_string($value['mediaid'])) {
                throw new CustomException("A property 'mediaid' was expected in a string in item {$key}", "12x001");
            }

            if (!isset($value['mediatype']) || !is_string($value['mediatype'])) {
                throw new CustomException("A property 'mediatype' was expected in a string in item {$key}", "12x002");
            }

            array_push($sqlArguments, $value['mediaid'], $value['mediatype'], $courseId, $arguments['unit']);
            array_push($values, "(?, ?, ?, ?)");
        }

        $valuesSQL = join(', ', $values);
        $sql = str_replace('__VALUES__', $valuesSQL, $sql);

        var_export($sql);
        var_export($sqlArguments);
        try {
            $this->DB->Execute($sql, $sqlArguments);
        } catch (Exception $e) {
            throw new CustomException("There was an error while inserting media sources", "12x003");
        }

        return true;
    }

    /**
     * Este método se encarga de eliminar recursos multimediales de un OVA en base al identificador corto del mismo e.g. "AV1", "AN2"
     * @param array $arguments  Un arreglo asociativo que debe contener las propiedades *unit* y *media*.
     *
     * @api
     *
     * @throws CustomException __13x000__, si los recursos no están en una array
     * @throws CustomException __13x001__, si uno de los campos *mediatype* no es de tipo string
     * @throws CustomException __13x002__, si ocurre un error en la eliminación de información
     *
     * @return array
     */

    protected function deleteLOMedia(array $arguments = array()){
        $this->validateArguments($arguments, array(
            'media' => 'required|array',
            'unit' => 'required|numeric',
        ));

        $courseId = $this->getCourseIdByShortName($arguments);

        $sql = "DELETE
                  FROM mdl_media
                 WHERE __CONDITIONS__";

        $conditions = array();
        $sqlArguments = array();
        foreach ($arguments['media'] as $key => $value) {
            if (!is_array($value)) {
                throw new CustomException("An array with a 'mediatype' field was expected in item {$key}", "13x000");
            }

            if (!isset($value['mediatype']) || !is_string($value['mediatype'])) {
                throw new CustomException("A property 'mediatype' was expected in a string in item {$key}", "13x001");
            }

            array_push($sqlArguments, $value['mediatype'], $courseId, $arguments['unit']);
            array_push($conditions, "(mediatype   = ?
                                 AND courseid = ?
                                 AND unit     = ?)");
        }

        $conditionsSQL = join(' OR ', $conditions);
        $sql = str_replace('__CONDITIONS__', $conditionsSQL, $sql);

        try {
            $this->DB->Execute($sql, $sqlArguments);
        } catch (Exception $e) {
            throw new CustomException("There was an error while trying to delete media sources", "13x002");
        }

        return true;
    }
}
