<?php

trait Team{
    /**
     * Este método se encarga de obtener el equipo del OVA en base al ID del curso y la unidad
     *
     * @api
     *
     * @throws CustomException _0x000_, si ocurre un error en la consulta.
     * @throws CustomException _0x002_, si no se encuntra el curso.
     *
     * @return array
     */

    protected function getLOTeam(array $arguments = array()){
        $this->validateArguments($arguments, array(
            'unit' => 'required|numeric'
        ));

        $courseId = $this->getCourseIdByShortName($arguments);

        if (is_string($courseId)) {
            $sql = "SELECT team
                      FROM mdl_team
                     WHERE courseid = ?
                       AND unit     = ?";

            try{
                $team = $this->DB->get_record_sql($sql, array(
                    $courseId,
                    $arguments["unit"],
                ));
            }
            catch(Exception $e){
                throw new CustomException("There was an error while fetching team.", "0x000");
            }

            if (is_object($team)) {
                return $team->team;
            } 

            return "No team was found for course {$arguments['shortName']} with ID {$courseId} and unit {$arguments['unit']}";
        } 

        throw new CustomException("Course wasn't found", "0x001");
    }

    /**
     * Este método se encarga de ingresar el equipo del OVA
     * @param array $arguments  Un arreglo asociativo que debe contener las propiedades *courseid* , *unit* y *team*
     *
     * @api
     * @throws CustomException __1x000__, si ocurre un error en la inserción de datos.
     *
     * @return bool
     */

    protected function addLOTeam(array $arguments = array()){
        $this->validateArguments($arguments, array(
            'team' => 'required|valid_json_string'
        ));

        $courseId = $this->getCourseIdByShortName($arguments);

        $this->deleteLOTeam($arguments);

        $sql = "INSERT INTO mdl_team
                           (courseid, unit, team)
                   VALUES (?, ?, ?)";

        try {
            $this->DB->Execute($sql, array(
                $courseId,
                $arguments["unit"],
                trim(preg_replace('/\\\\/', '', preg_replace('/\s\s+/', ' ', $arguments['team'])))
            ));
        } catch (Exception $e) {
            throw new CustomException("There was an error while inserting team", "1x000");
        }

        return true;
    }

    /**
     * Este método se encarga de eliminar el equipo del OVA
     * @param array $arguments  Un arreglo asociativo que debe contener las propiedades *courseid* y *unit*.
     *
     * @api
     *
     * @throws CustomException __2x000__, si ocurre un error en la eliminación
     *
     * @return array
     */

    protected function deleteLOTeam(array $arguments = array()){
        $this->validateArguments($arguments, array(
            'unit' => 'required|numeric',
        ));

        $courseId = $this->getCourseIdByShortName($arguments);

        $sql = "DELETE
                  FROM mdl_team
                 WHERE courseid = ?
                   AND unit     = ?";

        try {
            $this->DB->Execute($sql, Array(
                $courseId,
                $arguments['unit']
            ));
        } catch (Exception $e) {
            throw new CustomException("There was an error while trying to delete team", "2x000");
        }

        return true;
    }

}

?>