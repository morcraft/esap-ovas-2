<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ACREDITACIÓN PARA ENTIDADES PRESTADORAS DE SALUD</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        img {
            width: 100%;
        }
        @font-face {
            font-family: 'BebasNeue';
            src: url('../fonts/BebasNeue.otf');
        }
        @font-face {
            font-family: 'FFLight';
            src: url('../fonts/FF_DIN_Pro_Light.otf');
        }
        @font-face {
            font-family: 'FFBold';
            src: url('../fonts/FF_DIN_Pro_Bold.otf');
        }
        @font-face {
            font-family: 'FFBCondBold';
            src: url('../fonts/FF_DIN_Pro_Cond_Bold.otf');
        }
        @font-face {
            font-family: 'FFCondRegular';
            src: url('../fonts/FF_DIN_Pro_Cond_Regular.otf');
        }
        @font-face {
            font-family: 'FFCondLight';
            src: url('../fonts/FF_DIN_Pro_Cond_Light.otf');
        }
        @font-face {
            font-family: 'FFLight';
            src: url('../fonts/FF_DIN_Pro_Light.otf');
        }
        @font-face {
            font-family: 'FFThin';
            src: url('../fonts/FF_DIN_Pro_Thin.otf');
        }
        @font-face {
            font-family: 'Wingding';
            src: url('../fonts/wingding.ttf');
        }
        @font-face {
            font-family: 'century';
            src: url('../fonts/CenturyGothic.ttf');
        }
        @font-face {
            font-family: 'gothiddd';
            src: url('../fonts/GOTHIC.ttf');

        }
        @font-face {
            font-family: 'myriadRegular';
            src: url('../fonts/MyriadPro-Regular.otf');
        }

        html {

        }

        body {
            font-size: 12px;
            font-family: 'gothiddd';
            padding-top:30px;
        }

        h2 {
            font-family: 'gothiddd';
            font-size: 15px;
            font-weight: 100;
            text-align: center;
            color: #033449;
        }

        p {
            font-size: 20px;
            color:#033449;
            font-family: 'gothiddd';
        }

        @page {
            margin: 100px 25px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 70px;
            background-color: #1D324F;
            padding: 10px;
            border-radius: 20px;
            margin-bottom: 20px;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            font-size: 12px;
            font-family: 'FFDINProCond-Bold';
            text-align: center;
            background-color: #1D324F;
            color: #FFFFFF;
            padding: 10px;
            border-radius: 10px;
        }

        header h1 {
            font-family: 'gothiddd';
            font-size: 20px;
            font-weight: 100;
            margin: 0;
            padding: 0;
            text-align: left;
            color: #FFF;
            padding-left: 80px;

        }

        header h2 {
            font-family: 'gothiddd';
            font-size: 15px;
            font-weight: 100;
            margin: 0;
            padding: 0;
            text-align: left;
            color: #FFFFFF;
            padding-left: 80px;
        }

        #esap-logo {
            position: absolute;
            width: 60px;
        }

        .question-text {
            font-family: 'FFDINPro-Light';
            color: #033449;
            font-size: 16px;
            margin-top: 15px;
        }

        .date {
            font-family: 'FFDINPro-Light';
            color: #90133C;
            text-align: right;
            font-size: 8px;
            border-bottom: solid #90133C 1px dashed;
            margin-bottom: 10px;
        }

        .date p {
            font-size: 12px;
        }

        .date b {
            font-family: 'FFDINPro-Bold';
            font-weight: inherit;
        }

        .quiz-title {
            font-family: 'FFDINPro-Light';
            margin: 0 18px 0 10px;
            padding: 0;
            font-size: 24px;
        }

        .contRtaUno {
            width: 100%;
            height: auto;
        }

        .contRtaUno h2 {
            padding: 0%;
            margin: 0%;
            font-size: 2em;
        }
        .contRtaUno p {
            padding: 0%;
            margin: 0%;
            font-size: 2em;
            text-align: center;
        }
        .contRtaUno p span{
            font-family: 'FFDINPro-Bold';
        }

        .answer-text{
            font-family:'FFDINPro-Light';
            font-size:16px;
            margin-left:16px;
            background-color:#D3D2D6;
            padding:5px;
        }
    </style>

    <body>
        <header>
            <img src="../img/esap_logo.png" id="esap-logo">
            <h1>ACREDITACIÓN PARA ENTIDADES PRESTADORAS DE SALUD</h1>
            <h2>ASPECTOS GENERALES DEL PROCESO DE ACREDITACIÓN</h2>
        </header>
        <hr>

        <div class="date">
            <p>Hora de generación: <b>{$date}</b></p>
        </div>

        <h2 class="quiz-title">Actividad de aprendizaje 2</h2>
        
        {foreach from=$options item=$item key=$key}
            <div class="question">
                <p class="question-text">{$key+1}. {$item}</p>
                {foreach from=$pdfArgs item=object key=k}
                        {if $k == $key}
                            <p class="answer-text">{$object['answer']}</p>
                        {/if}                     
                {/foreach}
            </div>  
        {/foreach}

        <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>
        
    </body>
</html>
