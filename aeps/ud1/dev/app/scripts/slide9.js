const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');

module.exports = {
    currentProps: {
        answers: []
    },

    addEventListeners: function(){

    	var self = this;

        $('.slide[data-slide="9"]  textarea').on("keyup", function () {
            $this = $(this);
            $respuesta = $this.data("respuesta");

            module.exports.currentProps.answers[$respuesta-1] = $this.val();
        });

        $("[data-slide='9'] .pdf").on("click", function () {
            require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
                component: 'activities',
                method: 'addLOActivities',
                showFeedback: true,
                arguments: {
                    shortName: require('courseData.json').shortName,
                    unit: require('courseData.json').unit,
                    activityId: 'actividad segunda',
                    answers: JSON.stringify({
                        respuestas: module.exports.currentProps.answers
                    })
                }
            })
            .then(function(response){
                console.log('response', response);
            });
            
            var pdfArgs = {};
            for (var i = 0; i < module.exports.currentProps.answers.length; i++) {
                pdfArgs[i] = {
                  answer: module.exports.currentProps.answers[i]
                };
            }

            console.log('pdfArgs', pdfArgs);

            var pdfArgs = $.param({
              pdfArgs: pdfArgs
            });
            console.log(pdfArgs);
            $("[data-slide='9'] .pdfAct2").attr("href", "dynamicPdf/pdfActividad2.php?"+pdfArgs);
        });

        $("[data-slide='9'] .foro").on("click", function () {
            $("[data-slide='9'] .enlace").attr("href", _.get(self, 'courseActivities.assign.1.href'));
        	$("[data-slide='9'] .button-continuar").removeClass("escondido");
        });

        $("[data-slide='9'] .button-continuar").on("click", function () {
        	presentation.switchToSlide({
        	  slide: $('.slide[data-slide="1"]')
        	});
        	setTimeout(function () {
        		self.ovaProgress.updateFinishedTopics({
        		    topics: {
        		        'actividad_2': true
        		    },
        		    action: 'add',
        		    courseShortName: courseData.shortName,
        		    courseUnit: courseData.unit
        		});
        	}, 1000);
        	setTimeout(function () {
        		$('.slide[data-slide="1"] .actividad[data-actividad="2"]').addClass("visto");
        	}, 2000);
        });
	}
}