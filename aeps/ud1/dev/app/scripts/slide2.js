const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');

module.exports = {

    onEnter: function(){
        swal({
            target: stageResize.currentProps.$stage.get(0),
            allowOutsideClick: false,
            allowEscapeKey: false,
            customClass: 'ova-themed',
            showCloseButton: true,
            showConfirmButton: false,
            title: 'Actividad de aprendizaje 1.  ¿Qué conozco acerca del Sistema Obligatorio de Garantía de la Calidad de la Salud? Lea atentamente todas las instrucciones y siga los pasos descritos a continuación.',
        });
    },

    addEventListeners: function(){

        var self = this;

        $("[data-slide='2'] .opcion").on("click", function () {
            if (!$(this).hasClass("disabled")) {
                $this = $(this);
                $this.addClass("visto");
                
                if ($("[data-slide='2'] .visto").length == $("[data-slide='2'] .opcion").length - 1) {
                    $("[data-slide='2'] .guardar").removeClass("disabled");
                }

                if ($(this).hasClass("guardar")) {
                    $("[data-slide='2'] .enlace").attr("href", _.get(self, 'courseActivities.assign.0.href'));
                    continuar();
                }
            }
        });

        function continuar() {
            setTimeout(function () {
                presentation.switchToSlide({
                  slide: $('.slide[data-slide="1"]')
                });
            }, 1500);

            setTimeout(function () {
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'actividad_inicial': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                });
            }, 2500);
            setTimeout(function () {
                $("[data-slide='1'] .learning-experience").removeClass("disabled");
                $("[data-slide='1'] .actividad").removeClass("disabled");
                $("[data-slide='1'] .recurso").removeClass("disabled");
            }, 3500);
            setTimeout(function () {
                $("[data-slide='1'] .actividad").removeClass("clicked");
                $("[data-slide='1'] .recurso").removeClass("clicked");
                $('.slide[data-slide="3"]').data("presentation").enabled = true;
            }, 4500);
            setTimeout(function () {
                $("[data-slide='1'] .actividad[data-actividad='1']").addClass("disabled clicked");
                $('.slide[data-slide="2"]').data("presentation").enabled = false;
            }, 5500);
        };
    }
}