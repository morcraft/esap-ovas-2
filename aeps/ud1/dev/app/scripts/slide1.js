const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

    addEventListeners: function(){

        var self = this;

        console.log(self);

        $("#presentation").on("slide", function (e, i) {
            console.log("onSlide");
            $('.navigation-buttons .home').addClass("oculto");
            $('.navigation-buttons .next').addClass("oculto");
            $('.navigation-buttons .prev').addClass("oculto");

            var slideId = presentation.currentProps.$slide.attr("data-slide");
            console.log(slideId);
            if (slideId != 1 && slideId != 3) {
                setTimeout(function () {
                    $('.navigation-buttons .home').removeClass("oculto");
                    //$('.navigation-buttons .home').addClass("volver");
                }, 1000);
            }

            if (slideId == 8) {
                $('.navigation-buttons .next').removeClass("oculto");
                setTimeout(function () {
                    $('.navigation-buttons .next').addClass("highlight");
                }, 1000);
            }

            if (slideId == 9) {
                $('.navigation-buttons .prev').removeClass("oculto");
            }
        });

        $("[data-slide='1'] .recurso[data-recurso='5']").on("click", function () {
            if (!$(this).hasClass("disabled")) {
                swal({
                    target: stageResize.currentProps.$stage.get(0),
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    customClass: 'ova-themed video',
                    showCloseButton: true,
                    showConfirmButton: false,
                    html: require("templates/slide1Video.hbs")(self.media),
                }).then(function (t) {
                    var targetConcepts = _.pick(concepts, [
                        "Estándar"
                    ]);
                    
                    self.ovaConcepts.showNewConceptsModal({
                        title: "¿Qué se puede entender por estandar en los proceso de acreditación de empresas prestadoras de salud?",
                        subtitle: "Busque en internet alguna noticia que se sirva como ejemplo para contextualizar y dar respuesta a este interrogante. Guíe su búsqueda usando la definición de 'estándar' que se encuentra en su cofre conceptos.",
                        concepts: targetConcepts
                    })
                    .then(function (t) {
                        presentation.switchToSlide({
                          slide: $('.slide[data-slide="7"]')
                        });
                        setTimeout(function () {
                            self.ovaConcepts.updateConcepts({
                                concepts: targetConcepts,
                                action: "insert",
                                courseShortName: courseData.shortName,
                                courseUnit: courseData.unit
                            });
                        }, 1000);
                    })
                });
            }
        });

        $('.navigation-buttons .next').addClass("oculto");
        $('.navigation-buttons .prev').addClass("oculto");
        $('.navigation-buttons .home').addClass("oculto");

        if (_.isObject(this.finishedTopics['actividad_inicial'])) {
            $("[data-slide='1'] .learning-experience").removeClass("disabled");
            $("[data-slide='1'] .actividad").removeClass("disabled");
            $("[data-slide='1'] .recurso").removeClass("disabled");
            $("[data-slide='1'] .actividad").removeClass("clicked");
            $("[data-slide='1'] .recurso").removeClass("clicked");
            $('.slide[data-slide="3"]').data("presentation").enabled = true;
            $("[data-slide='1'] .actividad[data-actividad='1']").addClass("disabled clicked");
            $('.slide[data-slide="2"]').data("presentation").enabled = false;
        }
        if (_.isObject(this.finishedTopics['actividad_final'])) {
            $("[data-slide='1'] .actividad[data-actividad='1']").addClass("disabled clicked");
            $('.slide[data-slide="2"]').data("presentation").enabled = false;
            $("[data-slide='1'] .actividad[data-actividad='3']").addClass("disabled clicked");
            $('.slide[data-slide="12"]').data("presentation").enabled = false;
        }
    }
}