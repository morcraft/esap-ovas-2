const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

    addEventListeners: function(){

    	var self = this;

        $("[data-slide='10'] .bandera").on("click", function () {
        	$this = $(this);
        	$this.addClass("visto");
        	$bandera = $this.data("bandera");

        	$("[data-slide='10'] .info").addClass("oculto");

        	$("[data-slide='10'] #band" + $bandera).removeClass("oculto");

        	if ($("[data-slide='10'] .visto").length == $("[data-slide='10'] .info").length) {
        		setTimeout(function () {
        			$("[data-slide='10'] .button-continuar").removeClass("escondido");
        		}, 1500);
        	}
        });

        $("[data-slide='10'] .button-continuar").on("click", function () {
            presentation.switchToSlide({
              slide: $('.slide[data-slide="1"]')
            });
        	setTimeout(function () {
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'recurso_6': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                });
            }, 1500);
            setTimeout(function () {
                $('.slide[data-slide="1"] .recurso[data-recurso="6"]').addClass("visto");
            }, 3000);
        });
	}
}