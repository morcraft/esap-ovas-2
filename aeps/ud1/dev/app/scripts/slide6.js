const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {
	currentProps: {
	    pagina: 5,
	    totalPaginas: 9
	},

    addEventListeners: function(){

    	var self = this;

        $("[data-slide='6'] .cafe").on("click", function () {
        	$this = $(this);
        	$boton = $this.data("opcion");
        	entrar = false;
        	if ($boton == "arriba" && module.exports.currentProps.pagina > 1){ 
        		module.exports.currentProps.pagina--;
        		entrar = true;
        	}
        	if ($boton == "abajo" && module.exports.currentProps.pagina != module.exports.currentProps.totalPaginas) {
        		module.exports.currentProps.pagina++;
        		entrar = true;
        	}

        	if (entrar) {
        		$("[data-slide='6'] .fechita").removeClass("activo");
        		$("[data-slide='6'] .pagina").addClass("oculto");
        		$("[data-slide='6'] .fechita[data-fecha='" + module.exports.currentProps.pagina + "']").addClass("visto activo");
        		$("[data-slide='6'] .pagina[data-pagina='" + module.exports.currentProps.pagina + "']").removeClass("oculto");

        		if ($("[data-slide='6'] .visto").length == $("[data-slide='6'] .fechita").length) {
        		    $("[data-slide='6'] .button-continuar").removeClass("escondido");
        		}
        	}
        });

        $("[data-slide='6'] .button-continuar").on("click", function () {
        	var targetConcepts = _.pick(concepts, [
        	    "Calidad"
        	]);
        	
        	self.ovaConcepts.showNewConceptsModal({
        	    title: "¿En qué año cree usted que se realizó el mayor avance en materia de calidad de la Salud?",
        	    subtitle: "Revise en internet la normatividad y guarde en sus archivos personales los documentos encontrados. Realice la lectura de los mismos y ubique la fecha donde se presentó el avance más significativo en materia de calidad en salud.",
        	    concepts: targetConcepts
        	})
        	.then(function (t) {
        	    presentation.switchToSlide({
        	      slide: $('.slide[data-slide="1"]')
        	    });
        	    setTimeout(function () {
        	        self.ovaConcepts.updateConcepts({
        	            concepts: targetConcepts,
        	            action: "insert",
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 1000);
        	    setTimeout(function () {
        	        self.ovaProgress.updateFinishedTopics({
        	            topics: {
        	                'recurso_4': true
        	            },
        	            action: 'add',
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 2000);
        	    setTimeout(function () {
        	        $('.slide[data-slide="1"] .recurso[data-recurso="4"]').addClass("visto");
        	    }, 3000);
        	})
        });
	}
}