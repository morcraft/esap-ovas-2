<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ACREDITACIÓN PARA ENTIDADES PRESTADORAS DE SALUD</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        img{
            width:100%;
        }

        @font-face {
            font-family: 'BebasNeue';
            src: url('assets/fonts/BebasNeue.ttf');
        }

        @font-face {
            font-family: 'FFDINPro-Bold';
            src: url('assets/fonts/FFDINPro-Bold.ttf');
        }

        @font-face {
            font-family: 'FFDINPro-Light';
            src: url('assets/fonts/FFDINPro-Light.ttf');
        }

        @font-face {
            font-family: 'FFDINPro-Thin';
            src: url('assets/fonts/FFDINPro-Thin.ttf');
        }

        @font-face {
            font-family: 'FFDINProCond-Bold';
            src: url('assets/fonts/FFDINProCond-Bold.ttf');
        }

        @font-face {
            font-family: 'FFDINProCond-Light';
            src: url('assets/fonts/FFDINProCond-Light.ttf');
        }

        @font-face {
            font-family: 'FFDINProCond-Regular';
            src: url('assets/fonts/FFDINProCond-Regular.ttf');
        }

        @font-face {
            font-family: 'MyriadPro-Regular';
            src: url('assets/fonts/MyriadPro-Regular.ttf');
        }

        @font-face {
            font-family: 'wingding';
            src: url('assets/fonts/wingding.ttf');
        }

        @font-face {
            font-family: 'Wingdings-Regular';
            src: url('assets/fonts/Wingdings-Regular.ttf');
        }

        html{
        }
        
        body{
            font-size:12px;
            font-family:'MyriadPro-Regular';
        }
        h2{
            font-family:'FFDINProCond-Bold';
            font-size:20px;
            font-weight:100;
            text-align:center;
            color:#2C043F;
        }
        p{
            font-size:20px;
            color:#2C043F;
            font-family:'MyriadPro-Regular';
        }

        
        @page {
            margin: 100px 25px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 50px;
            background-color:#512A67;
            padding:10px;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            font-size:12px;
            font-family:'FFDINProCond-Bold';
            text-align: center;
            background-color:#512A67;
            color:#FFFFFF;
            padding:10px;
        } 

        header h1{
            font-family:'FFDINProCond-Bold';
            font-size:20px;
            font-weight:100;
            margin:0;
            padding:0;
            text-align: left;
            color:#2C043F;
            padding-left:80px;
        }

        header h2{
            font-family:'FFDINProCond-Bold';
            font-size:16px;
            font-weight:100;
            margin:0;
            padding:0;
            text-align: left;
            color:#FFFFFF;
            padding-left:80px;

        }

        #esap-logo{
            position:absolute;
            width:60px;
        }

        .question-text{
            font-family:'FFDINPro-Light';
            color:#2C043F;
            font-size:16px;
        }

        .answer-text{
            font-family:'FFDINPro-Light';
            color:#C45500; 
            font-size:16px;
            margin-left:16px;
            background-color:#9A95AD;
            padding:5px;
        }

        .date{
            font-family:'FFDINPro-Light';
            color:#C45500;
            text-align:right;
            font-size:8px;
        }

        .date p{
            font-size:12px;
        }

        .date b{
            font-family:'FFDINPro-Bold';
            font-weight:inherit;
        }

        .quiz-title{
            font-family:'FFDINPro-Light';
            margin:0 18px 0 10px;
            padding:0;
            font-size:24px;
        }

        .caja{
            margin-top: 5%;
            background-color: #9A95AD;
        }

        .texto{
            display: inline;
            color: white;
        }

        .respuesta{
            display: inline;
            /*margin: 0.5% 1%;*/
            color: #2C043F;
        }

    </style>
    <body>
        <header>
            <img src="../img/esap_logo.png" id="esap-logo">
            <h1>ACREDITACIÓN PARA ENTIDADES PRESTADORAS DE SALUD</h1>
            <h2>ESTÁNDARES DEL PROCESO DE GERENCIA DEL TALENTO HUMANO Y DE AMBIENTE FÍSICO</h2>
        </header>
        <hr>

        <div class="date">
            <p>Hora de generación: <b>{$date}</b></p>
        </div>

        <h2 class="quiz-title">Actividad de autoevaluación inicial</h2>
        <br>
        <p>¿Qué conozco sobre el estándar de gerencia del talento humano y del ambiente físico?</p>
        <div class="caja">
            <p class="texto">
                {foreach from=$options item=$item key=$key}
                    {$item}
                     <span class="respuesta">
                        <!-- {$answers[$pdfArgs[$key].answer]} -->
                        {if $answers[$pdfArgs[$key].answer] ne null}
                            {$answers[$pdfArgs[$key].answer]}
                        {else}
                            ___________________
                        {/if}
                    </span>
                 
                {/foreach}
            </p>
        </div> 

        <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>
        
    </body>
</html>
