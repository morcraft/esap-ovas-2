<?php
error_reporting(E_ALL);
require 'vendor/autoload.php';
require 'preguntasActividad.php';
require 'respuestasActividad.php';

use Dompdf\Dompdf;
// echo '<pre>';
// var_export($_GET);
// echo '</pre>';
// exit();
$smarty = new Smarty();
$smarty->error_reporting = 1;
$smarty->caching = 0;
$smarty->assign($_GET);
$smarty->assign(array(
    'date' => date("Y-m-d h:i:sa"),
    'options' =>$options,
    'answers' =>$answers
));
$view = $smarty->fetch('viewFinal.tpl');

$dompdf = new Dompdf();
$dompdf->loadHtml($view);
$dompdf->setPaper('A4', 'portrait');
$dompdf->render();
$dompdf->stream("autoevaluacion_final.pdf", array("Attachment" => false));
//$dompdf->stream("justificacion_actividad_MASC.pdf", array("Attachment" => true));
exit(0);
?>
