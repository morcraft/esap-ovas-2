const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

    addEventListeners: function(){

    	var self = this;

        var bpx = 2;
        var bpy = 65;

        audio1 = document.getElementById('audio1');
        audio1.onended = function(){
            $(this).addClass("visto");
            validaAudios();
        };

        audio2 = document.getElementById('audio2');
        audio2.onended = function(){
            $(this).addClass("visto");
            validaAudios();
        };

        audio3 = document.getElementById('audio3');
        audio3.onended = function(){
            $(this).addClass("visto");
            validaAudios();
        };

        audio4 = document.getElementById('audio4');
        audio4.onended = function(){
            $(this).addClass("visto");
            validaAudios();
        };

        function validaAudios(){
            if ($("[data-slide='4'] .visto").length == $("[data-slide='4'] .audios").length) {
                $("[data-slide='4'] .button-continuar").removeClass("escondido");
            }
        }

        $("[data-slide='4'] .repro").on("click", function () {
            $this = $(this);
            $this.addClass("visto");
        });

        $("[data-slide='4'] .direccion").on("click", function () {
            $this = $(this);
            $dir = $this.data("direccion");
            $pos = $this.data("posicion");

            if ($pos == "x") {
                if ($dir == "suma"){
                    if (bpx < 152) {
                        bpx += 10;
                    }
                }
                else{
                    if (bpx > - 58) {
                        bpx -= 10;
                    }
                }
                $("[data-slide='4'] .mapa").css('background-position-x', bpx + "%");
            }else{
                if ($dir == "suma"){
                    if (bpy < 105) {
                        bpy += 10;
                    }
                }
                else{
                    if (bpy > -5) {
                        bpy -= 10;
                    }
                }
                $("[data-slide='4'] .mapa").css('background-position-y', bpy + "%");
            }
        });

        $("[data-slide='4'] .button-continuar").on("click", function () {
        	var targetConcepts = _.pick(concepts, [
        	    "Mitigación del riesgo",
        	    "Proceso",
        	    "Registro"
        	]);
        	
        	self.ovaConcepts.showNewConceptsModal({
        	    title: "¿Qué tienen que ver los conceptos de mitigación de riesgo, proceso y registro con los elementos del espacio físico de su IPS?",
        	    subtitle: "¿Recuerda la última vez que visito su IPS? intente elaborar mentalmente un corto relato que incorpore los términos mitigación de riesgo, proceso y registro para contarle a un amigo o familiar cómo se percibe este aspecto en su IPS. Después revise estos conceptos en el cofre y contraste su descripción  mental con estas definiciones.",
        	    concepts: targetConcepts
        	})
        	.then(function (t) {
        	    presentation.switchToSlide({
        	      slide: $('.slide[data-slide="1"]')
        	    });
        	    setTimeout(function () {
        	        self.ovaConcepts.updateConcepts({
        	            concepts: targetConcepts,
        	            action: "insert",
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 1000);
        	    setTimeout(function () {
        	        self.ovaProgress.updateFinishedTopics({
        	            topics: {
        	                'recurso_2': true
        	            },
        	            action: 'add',
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 2000);
        	})
        });
	}
}