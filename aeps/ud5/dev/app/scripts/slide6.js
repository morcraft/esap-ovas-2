const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const slide3 = require("slide3.js");
const draggables = require("draggables.json");

module.exports = {

    addEventListeners: function(){

        var self = this;

        $("[data-slide='6'] .descarga").on("click", function () {
            console.log("oldAnswers" + slide3.currentProps.oldAnswers);

            if (_.isObject(self.activities['Autoevaluacion primera'])) {
                respuestasIniciales = self.activities['Autoevaluacion primera'].respuestas;
                console.log(respuestasIniciales);
                cargarRespuestas(respuestasIniciales);
            }else{
                cargarRespuestas(slide3.currentProps.oldAnswers);
            }
        });

        function cargarRespuestas(respuestasIniciales){
            var pdfArgs = {};
            for (var i = 0; i < respuestasIniciales.length; i++) {
                pdfArgs[i] = {
                  answer: respuestasIniciales[i]
                };
            }

            console.log('pdfArgs', pdfArgs);

            var pdfArgs = $.param({
              pdfArgs: pdfArgs
            });

            $("[data-slide='6'] .generarPdf").attr("href", "dynamicPdf/pdfInicial.php?" + pdfArgs);

            $("[data-slide='6'] .button-continuar").removeClass("escondido");
        }

        $("[data-slide='6'] .button-continuar").on("click", function () {
        	presentation.switchToSlide({
        	  slide: $('.slide[data-slide="7"]')
        	});
        });
	}
}