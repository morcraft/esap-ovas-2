'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
module.exports = {
    currentProps: {
        finished: false
    },
    showModal: function(args){
        swal({
                target: stageResize.currentProps.$stage.get(0),
                customClass: 'ova-themed biggest marcoAvatar',
                background: "transparent",
                showCloseButton: false,
                showConfirmButton: true,
                confirmButtonText: "Continuar",
                confirmButtonClass: "continuar click-me",
                allowOutsideClick: false,
                allowEscapeKey: false,
                html: require('templates/slide1Avatar.hbs')()
            });

            var avatares = {
                $index : 1,
                $tam: 3
            }

        $('html').attr("data-selected-character", 1);

        $('.marcoAvatar .boton').on("click", function() {
            console.log("click");
            if ($(this).hasClass("next")) {
                avatares.$index++;
                if(avatares.$index > avatares.$tam)
                    avatares.$index = 1;
            }else{
                avatares.$index--;
                if(avatares.$index < 1)
                    avatares.$index = avatares.$tam;
            }
            $('.avatar').css("background-image", 'url("img/1/avatar' + avatares.$index + '.png")');
            $('.hexagono').addClass("color" + avatares.$index);
            $('.marcoAvatar .marco').css("background-image", 'url("img/1/marco' + avatares.$index + '.png")');
            $('html').attr("data-selected-character", avatares.$index);
        });
    },
    addEventListeners: function () {
        var self = this;
        console.log(this)
        module.exports.showModal();

        $("#presentation").on("slide", function (e, i) {
            console.log("onSlide");
            $('.navigation-buttons .home').addClass("oculto");
            $('.navigation-buttons .home').removeClass("volver");
            $('.navigation-buttons .next').addClass("oculto");
            $('.navigation-buttons .prev').addClass("oculto");
            $('.navigation-buttons .next').removeClass("siguiente");
            var slideId = presentation.currentProps.$slide.attr("data-slide");
            console.log(slideId);
            if (slideId != 1) {
                setTimeout(function () {
                    $('.navigation-buttons .home').removeClass("oculto");
                    //$('.navigation-buttons .home').addClass("volver");
                }, 1000);

                // if (slideId == 4 || slideId == 8 || slideId == 10 || slideId == 11 || slideId == 14 || slideId == 15 || slideId == 17 || slideId == 19 || slideId == 24 || slideId == 26 || slideId == 28 || slideId == 29 || slideId == 31 || slideId == 34) {
                //     $('.navigation-buttons .next').removeClass("oculto");
                //     setTimeout(function () {
                //         $('.navigation-buttons .next').addClass("siguiente");
                //     }, 1500);
                // }

                if (slideId == 3) {
                    $('.navigation-buttons .prev').removeClass("oculto");
                }
            }
        });

        $(".pdf").on("click", function () {
            var $pdf = $(this);
            var $a = $pdf.find("a"); 
            $pdf.find("i").removeClass("animated flip infinite")

            $("[data-slide='1'] .actividad[data-actividad='2']").removeClass("clicked");            
        });

        $("[data-slide='1'] .learning-experience[data-experience='1']").on("click", function () {
            if(!$(this).hasClass("disabled")){
                swal({
                    target: stageResize.currentProps.$stage.get(0),
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCloseButton: true,
                    showConfirmButton: false,
                    customClass: "ova-themed biggest video",
                    html: require("templates/slide1Video.hbs")(self.media)
                }).then(function (t) {
                    var targetConcepts = _.pick(concepts, [
                        "Desastre",
                        "Inventario"
                    ]);
                    
                    self.ovaConcepts.showNewConceptsModal({
                        title: "¿Cómo articular estos dos conceptos?",
                        subtitle: "Ubique mentalmente dos situaciones de su vida que le recuerden estas dos palabras. Intente generar una sola imagen mental coherente donde puedan estar las dos, después revise las definiciones de estos conceptos en el cofre y corrobore hasta dónde su imagen se ajusta a las definiciones.",
                        concepts: targetConcepts
                    })
                    .then(function (t) {
                        self.ovaConcepts.updateConcepts({
                            concepts: targetConcepts,
                            action: "insert",
                            courseShortName: courseData.shortName,
                            courseUnit: courseData.unit
                        });
                        setTimeout(function () {
                            self.ovaProgress.updateFinishedTopics({
                                topics: {
                                    'recurso_1': true
                                },
                                action: 'add',
                                courseShortName: courseData.shortName,
                                courseUnit: courseData.unit
                            });
                        }, 1000);
                    })
                });
            }
        });

        $("[data-slide='1'] .learning-experience[data-experience='3']").on("click", function () {
            if(!$(this).hasClass("disabled")){
                swal({
                    target: stageResize.currentProps.$stage.get(0),
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCloseButton: true,
                    showConfirmButton: false,
                    customClass: "ova-themed biggest video",
                    html: require("templates/slide1Video2.hbs")(self.media)
                }).then(function (t) {
                    var targetConcepts = _.pick(concepts, [
                        "Profesional de salud",
                        "Relación docencia-servicio",
                        "Seguridad industrial"
                    ]);
                    
                    self.ovaConcepts.showNewConceptsModal({
                        title: "¿Qué puede significar cada uno de estos conceptos?",
                        subtitle: "Vamos a jugar 'diccionario mental'. Lea con atención cada uno de estos términos e invente un significado para cada uno. Si quiere puede escribir estos significados en su libreta de apuntes. Después explore el cofre de conceptos y contraste los significados con los que usted pensó ¿se acerco al significado real?",
                        concepts: targetConcepts
                    })
                    .then(function (t) {
                        self.ovaConcepts.updateConcepts({
                            concepts: targetConcepts,
                            action: "insert",
                            courseShortName: courseData.shortName,
                            courseUnit: courseData.unit
                        });
                        setTimeout(function () {
                            self.ovaProgress.updateFinishedTopics({
                                topics: {
                                    'recurso_3': true
                                },
                                action: 'add',
                                courseShortName: courseData.shortName,
                                courseUnit: courseData.unit
                            });
                        }, 1000);
                    })
                });
            }
        });

        $('.navigation-buttons .next').addClass("oculto");
        $('.navigation-buttons .prev').addClass("oculto");
        $('.navigation-buttons .home').addClass("oculto");

        if (_.isObject(this.finishedTopics['evaluacion_inicial'])) {
            $("[data-slide='1'] .actividad[data-actividad='1']").addClass("disabled");
            $('.slide[data-slide="2"]').data("presentation").enabled = false;
            $("[data-slide='1'] .learning-experience").removeClass("disabled");
            $("[data-slide='1'] .actividad[data-actividad='2']").removeClass("disabled");
            $("[data-slide='1'] .actividad[data-actividad='3']").removeClass("disabled");
            $("[data-slide='1'] .learning-experience").removeClass("clicked");
            $("[data-slide='1'] .actividad[data-actividad='2']").removeClass("clicked");
            $("[data-slide='1'] .actividad[data-actividad='3']").removeClass("clicked");
            $('.slide[data-slide="4"]').data("presentation").enabled = true;
        }
        if (_.isObject(this.finishedTopics['evaluacion_final'])) {
            $("[data-slide='1'] .actividad[data-actividad='3']").addClass("disabled");
            $('.slide[data-slide="6"]').data("presentation").enabled = false;
        }
    }
}
