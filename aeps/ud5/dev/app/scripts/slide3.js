'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
const draggables = require("draggables.json");
const preciseDraggable = require('precise-draggable')
const slidesManager = require('slidesManager.js')

module.exports = {
    currentProps: {
        oldAnswers: []
    },
    addEventListeners: function () {
        var self = this;

        var initialAnswers = [];

        _.forEach(draggables.droppableZones, function (v, k) {
            $('<div class="droppable drop" data-ani-class="wobble" data-posicion="' + k +'"/>')
            .appendTo($('.slide[data-slide="3"] .caja'))
            .css({
                'top': v.y + '%',
                'left': v.x + '%'
            }).on({
                'draggableOver': function () {
                    if (preciseDraggable.currentProps.dragging) {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('over');
                        }
                    }
                },
                'draggableLeave': function () {
                    if (!$(this).data('dropped')) {
                        $(this).removeClass('over');
                    }
                },
                'draggableDrop': function () {
                    preciseDraggable.currentProps.lastDraggable.$element.removeClass('is-dragging');
                    if (!$(this).data('dropped')) {
                        var element = preciseDraggable.currentProps.lastDraggable;
                        var draggableKey = element.data.key;
                        var droppableKey = $(this).attr('data-key')
                        $(this).removeClass('over');
                        $(this).addClass('dropeado');
                        if(draggableKey !== droppableKey){
                            return true;
                        }
                        $(this).data('dropped', true);
                        var $el = preciseDraggable.currentProps.lastDraggable.$element;
                        $(this).append($el.html());
                        $el.remove();

                        console.log($el.data("palabra"));
                        initialAnswers[k] = $el.data("palabra");
                        // if(self.$slide.find('.draggable-element').length === 0){
                            
                        // }
                        $("[data-slide='3'] .reiniciar").removeClass("oculto");
                    }
                    
                }
            });
        });

        var $droppableElements = self.$slide.find('.drop');

        cargarDrags();

        function cargarDrags(){

            var $draggableItems = $('.slide[data-slide="3"] .draggable-items'); //toma div 
            $draggableItems.html("");

            _.forEach(draggables.items, function (v, k) {
                var $el = $('<div class="palabra draggable-element" data-palabra="' + k +'" data-ani-class="wobble" data-ani-delay="700"><span>' + v.label + '</span></div>')
                    .appendTo($('.slide[data-slide="3"] .draggable-items'));

                $el.on('mouseenter', function(){
                    $(this).addClass('animations-disabled');
                });

                preciseDraggable.currentProps.$stage = $('#stage');

                var $draggable = $el;
                var draggie = preciseDraggable.setDraggable({
                    $target: $draggable,
                    data: {
                        key: $draggable.attr('data-key')
                    }
                });

                draggie.on('dragStart', function(){
                    $draggable.addClass('animations-disabled');
                    preciseDraggable.currentProps.dragging = true;
                    var $this = draggie.$element;
                    preciseDraggable.currentProps.lastDraggable = {
                        data: {
                            key: $this.attr('data-key')
                        },
                        $element: $this
                    };
                    self.$slide.find('.drop').each(function () {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('highlight');
                        }
                    })
                });

                draggie.on('dragMove', function(event){
                    draggie.overlap = preciseDraggable.getOverlap({
                        originalGroup: $draggable,
                        matchingGroup: $droppableElements
                    });

                    $droppableElements.trigger('draggableLeave');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableOver');
                    }
                })

                draggie.on('dragEnd', function(){
                    $droppableElements.removeClass('highlight over');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableDrop');
                    }
                })
            });
        }

        $("[data-slide='3'] .reiniciar").on("click", function () {
            $("[data-slide='3'] .drop").html("");
            $("[data-slide='3'] .drop").removeClass("dropeado");
            $("[data-slide='3'] .drop").data('dropped', false);

            console.log(initialAnswers);
             initialAnswers = [];
             console.log(initialAnswers);

             cargarDrags();

             $(this).addClass("oculto");
        });

        $("[data-slide='3'] .guardar").on("click", function () {
            module.exports.currentProps.oldAnswers = initialAnswers;
            console.log(module.exports.currentProps.oldAnswers);

            require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
                component: 'activities',
                method: 'addLOActivities',
                showFeedback: true,
                arguments: {
                    shortName: require('courseData.json').shortName,
                    unit: require('courseData.json').unit,
                    activityId: 'Autoevaluacion primera',
                    answers: JSON.stringify({
                        respuestas: module.exports.currentProps.oldAnswers
                    })
                }
            })
            .then(function(response){
                console.log('response', response);
            });

            var pdfArgs = {};
            _.forEach(draggables.droppableZones, function(v, k){
                pdfArgs[k] = {
                  answer: module.exports.currentProps.oldAnswers[k]
                };
            });

            console.log('pdfArgs', pdfArgs);

            var pdfArgs = $.param({
              pdfArgs: pdfArgs
            });

            $("[data-slide='3'] .generarPdf").attr("href", "dynamicPdf/pdfInicial.php?" + pdfArgs);

            $("[data-slide='3'] .enviar").removeClass("oculto");
        });

        $("[data-slide='3'] .enviar").on("click", function () {
            swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/slide3Enviar.hbs")
                (
                    {
                        assignLink: _.get(self, 'courseActivities.assign.0.href')
                    }
                ),
            }).then(function (t) {
                var targetConcepts = _.pick(concepts, [
                    "Insumo",
                    "Estándar",
                    "Peligro"
                ]);
                
                self.ovaConcepts.showNewConceptsModal({
                    title: "¿Qué condiciones de la ambiente físico y talento humano considera más trascendentales?",
                    subtitle: "Busque en internet imágenes que puedan responder a esta pregunta. Luego explore las definiciones de los conceptos obtenidos en el cofre y verifique que las imágenes corresponden con la definición de cada concepto. Guarde las imágenes más apropiadas para posteriores actividades.",
                    concepts: targetConcepts
                }).then(function (t) {
                    presentation.switchToSlide({
                      slide: $('.slide[data-slide="1"]')
                    });
                    setTimeout(function () {
                        self.ovaConcepts.updateConcepts({
                            concepts: targetConcepts,
                            action: "insert",
                            courseShortName: courseData.shortName,
                            courseUnit: courseData.unit
                        });
                    }, 1000);
                    setTimeout(function () {
                        self.ovaProgress.updateFinishedTopics({
                            topics: {
                                'evaluacion_inicial': true
                            },
                            action: 'add',
                            courseShortName: courseData.shortName,
                            courseUnit: courseData.unit
                        });
                    }, 2000);
                    setTimeout(function () {
                        $("[data-slide='1'] .actividad[data-actividad='1']").addClass("disabled");
                        $('.slide[data-slide="2"]').data("presentation").enabled = false;
                    }, 3000);
                    setTimeout(function () {
                        $("[data-slide='1'] .learning-experience").removeClass("disabled");
                        $("[data-slide='1'] .actividad[data-actividad='2']").removeClass("disabled");
                        $("[data-slide='1'] .actividad[data-actividad='3']").removeClass("disabled");
                    }, 4000);
                    setTimeout(function () {
                        $("[data-slide='1'] .learning-experience").removeClass("clicked");
                        $("[data-slide='1'] .actividad[data-actividad='2']").removeClass("clicked");
                        $("[data-slide='1'] .actividad[data-actividad='3']").removeClass("clicked");
                        $('.slide[data-slide="4"]').data("presentation").enabled = true;
                    }, 4500);
                });
            });
        });
    }
}


