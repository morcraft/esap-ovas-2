<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Acreditación para Entidades Prestadoras de Salud</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<style>
    img {
        width: 100%;
    }
    @font-face {
        font-family: 'BebasNeue';
        src: url('../fonts/BebasNeue.otf');
    }
    @font-face {
        font-family: 'FFLight';
        src: url('../fonts/FF_DIN_Pro_Light.otf');
    }
    @font-face {
        font-family: 'FFBold';
        src: url('../fonts/FF_DIN_Pro_Bold.otf');
    }
    @font-face {
        font-family: 'FFBCondBold';
        src: url('../fonts/FF_DIN_Pro_Cond_Bold.otf');
    }
    @font-face {
        font-family: 'FFCondRegular';
        src: url('../fonts/FF_DIN_Pro_Cond_Regular.otf');
    }
    @font-face {
        font-family: 'FFCondLight';
        src: url('../fonts/FF_DIN_Pro_Cond_Light.otf');
    }
    @font-face {
        font-family: 'FFLight';
        src: url('../fonts/FF_DIN_Pro_Light.otf');
    }
    @font-face {
        font-family: 'FFThin';
        src: url('../fonts/FF_DIN_Pro_Thin.otf');
    }
    @font-face {
        font-family: 'Wingding';
        src: url('../fonts/wingding.ttf');
    }
    @font-face {
        font-family: 'century';
        src: url('../fonts/CenturyGothic.ttf');
    }
    @font-face {
        font-family: 'gothiddd';
        src: url('../fonts/GOTHIC.otf');

    }
    @font-face {
        font-family: 'myriadRegular';
        src: url('../fonts/MyriadPro-Regular.otf');
    }

    html {

    }

    body {
        font-size: 12px;
        font-family: 'gothiddd';
    }

    h2 {
        font-family: 'FFDINProCond-Bold';
        font-size: 20px;
        font-weight: 100;
        text-align: center;
        color: #90133C;
    }

    p {
        font-size: 20px;
        color:#90133C;
        font-family: 'gothiddd';
    }

    @page {
        margin: 100px 25px;
    }

    header {
        position: fixed;
        top: -60px;
        left: 0px;
        right: 0px;
        height: 50px;
        background-color: #960056;
        padding: 10px;
        border-radius: 20px;
    }

    footer {
        position: fixed;
        bottom: -60px;
        left: 0px;
        right: 0px;
        font-size: 12px;
        font-family: 'FFDINProCond-Bold';
        text-align: center;
        background-color: #960056;
        color: #FFFFFF;
        padding: 10px;
        border-radius: 10px;
    }

    header h1 {
        font-family: 'FFDINProCond-Bold';
        font-size: 20px;
        font-weight: 100;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #FFF;
        padding-left: 80px;

    }

    header h2 {
        font-family:'FFDINProCond-Bold';
        font-size: 16px;
        font-weight: 100;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #FFFFFF;
        padding-left: 80px;
    }
    #esap-logo {
        position: absolute;
        width: 60px;
    }
    .question-text {
        font-family: 'FFDINPro-Light';
        color: #960056;
        font-size: 16px;
    }
    .date {
        font-family: 'FFDINPro-Light';
        color: #960056;
        text-align: right;
        font-size: 8px;
        border-bottom: solid #960056 1px dashed;
        margin-bottom: 10px;
    }
    .date p {
        font-size: 12px;
    }

    .date b {
        font-family: 'FFDINPro-Bold';
        font-weight: inherit;
    }
    .quiz-title {
        font-family: 'FFDINPro-Light';
        margin: 0 18px 0 10px;
        padding: 0;
        font-size: 24px;
    }
    .pregunta-title{
      width: 100%;
      height: auto;
      padding: 5px;
    }
    .pregunta-title h2{
      font-family: 'FFDINPro-Bold';
      font-size: 1.5em;
      margin: 0px;
      padding: 0px;
    }
    .pregunta-title p{
      font-family: 'FFDINPro-Light';
      font-size:2em;
      margin: 0px;
      padding: 10px;
      border: solid 2px #960056;
      text-align: center;
    }
</style>

<body>
    <header>
        <img src="assets/img/esap_logo.png" id="esap-logo">
        <h1>Estándar de direccionamiento y gerencia</h1>
        <h2>Acreditación para Entidades Prestadoras de Salud</h2>
    </header>
    <div class="date">
        <p>Hora de generación:
            <b>{$date}</b>
        </p>
    </div>

    <h2 class="quiz-title">{$a1}</h2>
    <br>
    <div class="pregunta-title">
        <h2>1. La importancia de los procesos de direccionamiento y gerencia dentro de una institución prestadora de servicios de salud.</h2>
        <p>{$r1[0]}</p>
        </div>
        <br>
          <div class="pregunta-title">
        <h2>2. Explique cuáles cree que son las diferencias entre estos dos tipos de procesos: direccionamiento y gerencia.</h2>
        <p>{$r1[1]}</p>
        <br>
          </div>
          <div class="pregunta-title">
        <h2>3. Describa cuáles cree que son esos procesos de direccionamiento y gerencia con los que debe contar una institución prestadora de servicios de salud.</h2>
        <p>{$r1[2]}</p>

    </div>
    <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>

</body>

</html>
