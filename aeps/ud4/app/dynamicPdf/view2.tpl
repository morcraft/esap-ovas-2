<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Acreditación para Entidades Prestadoras de Salud</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<style>
    img {
        width: 100%;
    }
    @font-face {
        font-family: 'BebasNeue';
        src: url('../fonts/BebasNeue.otf');
    }
    @font-face {
        font-family: 'FFLight';
        src: url('../fonts/FF_DIN_Pro_Light.otf');
    }
    @font-face {
        font-family: 'FFBold';
        src: url('../fonts/FF_DIN_Pro_Bold.otf');
    }
    @font-face {
        font-family: 'FFBCondBold';
        src: url('../fonts/FF_DIN_Pro_Cond_Bold.otf');
    }
    @font-face {
        font-family: 'FFCondRegular';
        src: url('../fonts/FF_DIN_Pro_Cond_Regular.otf');
    }
    @font-face {
        font-family: 'FFCondLight';
        src: url('../fonts/FF_DIN_Pro_Cond_Light.otf');
    }
    @font-face {
        font-family: 'FFLight';
        src: url('../fonts/FF_DIN_Pro_Light.otf');
    }
    @font-face {
        font-family: 'FFThin';
        src: url('../fonts/FF_DIN_Pro_Thin.otf');
    }
    @font-face {
        font-family: 'Wingding';
        src: url('../fonts/wingding.ttf');
    }
    @font-face {
        font-family: 'century';
        src: url('../fonts/CenturyGothic.ttf');
    }
    @font-face {
        font-family: 'gothiddd';
        src: url('../fonts/GOTHIC.ttf');

    }
    @font-face {
        font-family: 'myriadRegular';
        src: url('../fonts/MyriadPro-Regular.otf');
    }

    html {

    }

    body {
        font-size: 12px;
        font-family: 'FFBCondBold';
    }

    h2 {
        font-family: 'FFBCondBold';
        font-size: 20px;
        font-weight: 100;
        text-align: center;
        color: #90133C;
    }

    p {
        font-size: 20px;
        color:#90133C;
        font-family: 'FFBCondBold';

    }

    @page {
        margin: 100px 25px;
    }

    header {
        position: fixed;
        top: -60px;
        left: 0px;
        right: 0px;
        height: 50px;
        background-color: #960056;
        padding: 10px;
        border-radius: 20px;
    }

    footer {
        position: fixed;
        bottom: -60px;
        left: 0px;
        right: 0px;
        font-size: 12px;
        font-family: 'FFBCondBold';
        text-align: center;
        background-color: #960056;
        color: #FFFFFF;
        padding: 10px;
        border-radius: 10px;
    }

    header h1 {
        font-family:'FFBCondBold';
        font-size: 20px;
        font-weight: 100;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #FFF;
        padding-left: 80px;

    }

    header h2 {
        font-family: 'FFBCondBold';
        font-size: 16px;
        font-weight: 100;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #FFFFFF;
        padding-left: 80px;
    }

    #esap-logo {
        position: absolute;
        width: 60px;
    }

    .question-text {
        font-family: 'FFDINPro-Light';
        color: #04371c;
        font-size: 16px;
    }

    .date {
        font-family: 'FFDINPro-Light';
        color: #90133C;
        text-align: right;
        font-size: 8px;
        border-bottom: solid #960056 1px dashed;
        margin-bottom: 10px;
    }

    .date p {
        font-size: 12px;
    }

    .date b {
        font-family: 'FFDINPro-Bold';
        font-weight: inherit;
    }

    .quiz-title {
        font-family: 'FFDINPro-Light';
        margin: 0 18px 0 10px;
        padding: 0;
        font-size: 24px;
    }

    .contRtaUno {
        width: 100%;
        height: auto;
    }

    .contRtaUno h2 {
        padding: 0%;
        margin: 0%;
        font-size: 2em;
        font-family: 'FFDINPro-Bold';
    }
    .contRtaUno p {
        padding: 0%;
        margin: 0%;
        font-size: 2em;
        text-align: center;
    }
    .contRtaUno p span{
        font-family: 'FFDINPro-Bold';
    }

    .tabla{
      width: 100%;
      height: auto;
      text-align: center;
    }
    .tabla .fila{
      width: 100%;
      height: auto;
      border: solid #960056 2px;
      margin-top: 5px;
      padding: 5px;
    }

    .celda{
      width: 33%;
      height: auto;
      min-height: 100%;
      display: inline-block;
      vertical-align: middle;
      font-family: 'FFDINPro-Light';
      padding: 5px;
    }
    .celda-center{
      width: 13%!important;
      border-left: solid #960056 2px;
      border-right: solid #960056 2px;
    }
    .tabla .filatitulo{
      width: 100%;
      height: auto;
      margin-top: 5px;
      padding: 5px;
    }

    .celda-titulo{
      width: 33%;
      height: auto;
      min-height: 100%;
      display: inline-block;
      vertical-align: middle;
      font-family: 'FFDINPro-Bold';
      padding: 5px;
    }
    .celda-titulo-center{
      width: 13%!important;
      border-left: solid #960056 2px;
      border-right: solid #960056 2px;
    }

    .cont-rta-tres{
      width: 100%;
      padding:10px;
      font-family: 'FFDINPro-Light';
      font-size: 1.5em;
      margin: 0 auto;
      border: solid #960056;
    }

</style>

<body>
    <header>
        <img src="assets/img/esap_logo.png" id="esap-logo">
        <h1>Estándar de direccionamiento y gerencia</h1>
        <h2>Acreditación para Entidades Prestadoras de Salud</h2>
    </header>
    <div class="date">
        <p>Hora de generación:
            <b>{$date}</b>
        </p>
    </div>

    <h2 class="quiz-title">Respuestas Actividad 2</h2>
    <br>
    <div class="contRtaUno">
        <div class="tabla">

          <div class="filatitulo">
            <div class="celda-titulo">ASPECTO</div>
            <div class="celda-titulo celda-titulo-center">D/G</div>
            <div class="celda-titulo celda-titulo-center">C/I</div>
            <div class="celda-titulo ">ARGUMENTO</div>
          </div>

          <div class="fila">
            <div class="celda">Lectura del entorno</div>
            <div class="celda celda-center">{$r1[0]}</div>
            <div class="celda celda-center">{$r2[0]}</div>
            <div class="celda">{$r3[0]}</div>
          </div>

          <div class="fila">
            <div class="celda">Construcción de un plan de direccionamientoestratégico.</div>
            <div class="celda celda-center">{$r1[1]}</div>
            <div class="celda celda-center">{$r2[1]}</div>
            <div class="celda">{$r3[1]}</div>
          </div>

          <div class="fila">
            <div class="celda">Asignación de recursos físicos, financieros y de talento humano para las labores de mejoramiento.</div>
            <div class="celda celda-center">{$r1[2]}</div>
            <div class="celda celda-center">{$r2[2]}</div>
            <div class="celda">{$r3[2]}</div>
          </div>

          <div class="fila">
            <div class="celda">Definición y monitorización de metas y objetivos por unidad funcional, alineados con las metas y los objetivos institucionales.</div>
            <div class="celda celda-center">{$r1[3]}</div>
            <div class="celda celda-center">{$r2[3]}</div>
            <div class="celda">{$r3[3]}</div>
          </div>

          <div class="fila">
            <div class="celda">Sustentación de la gestión del personal ante la junta.</div>
            <div class="celda celda-center">{$r1[4]}</div>
            <div class="celda celda-center">{$r2[4]}</div>
            <div class="celda">{$r3[4]}</div>
          </div>

          <div class="fila">
            <div class="celda">Evaluación integral de la gestión en salud</div>
            <div class="celda celda-center">{$r1[5]}</div>
            <div class="celda celda-center">{$r2[5]}</div>
            <div class="celda">{$r3[5]}</div>
          </div>



    <div style="page-break-after: always;"></div>

    <br><br>

          <div class="filatitulo">
            <div class="celda-titulo">ASPECTO</div>
            <div class="celda-titulo celda-titulo-center">D/G</div>
            <div class="celda-titulo celda-titulo-center">C/I</div>
            <div class="celda-titulo ">ARGUMENTO</div>
          </div>

          <div class="fila">
            <div class="celda">Formulación y revisión periódica del direccionamiento estratégico.</div>
            <div class="celda celda-center">{$r1[6]}</div>
            <div class="celda celda-center">{$r2[6]}</div>
            <div class="celda">{$r3[6]}</div>
          </div>

          <div class="fila">
            <div class="celda">Comunicación, difusión y orientación del personal.</div>
            <div class="celda celda-center">{$r1[7]}</div>
            <div class="celda celda-center">{$r2[7]}</div>
            <div class="celda">{$r3[7]}</div>
          </div>

          <div class="fila">
            <div class="celda">Identificación y cumplimiento de los requisitos de entrada al proceso de acreditación.</div>
            <div class="celda celda-center">{$r1[8]}</div>
            <div class="celda celda-center">{$r2[8]}</div>
            <div class="celda">{$r3[8]}</div>
          </div>

          <div class="fila">
            <div class="celda">Identificación de los clientes internos y externos de los procesos y de sus necesidades.</div>
            <div class="celda celda-center">{$r1[9]}</div>
            <div class="celda celda-center">{$r2[9]}</div>
            <div class="celda">{$r3[9]}</div>
          </div>

          <div class="fila">
            <div class="celda">Seguimiento y evaluación del direccionamiento estratégico y del plan estratégico.</div>
            <div class="celda celda-center">{$r1[10]}</div>
            <div class="celda celda-center">{$r2[10]}</div>
            <div class="celda">{$r3[10]}</div>
          </div>

    </div>

    <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>

</body>

</html>
