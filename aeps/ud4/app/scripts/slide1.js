'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
module.exports = {
    currentProps: {
        finished: false
    },
    addEventListeners: function () {
        var self = this;
        // console.log(this)
        // if(_.size(self.finishedTopics) == 5){
        //     swal.close();
        // }
        if (_.isObject(self.finishedTopics['Actividad 1'])) {
            // CHANGE HOME 1
            $(".button-exp").removeClass("disabled");
            $(".initial-message").css("display", "none");
            $('.button-act[data-button-act="1"]').removeClass("animate-active").addClass("disabled");
        }

        if (_.isObject(self.finishedTopics['Actividad 3'])) {
            // CHANGE HOME 3
            $('.button-act[data-button-act="3"]').removeClass("animate-active").addClass("disabled");
        }

    }
}
