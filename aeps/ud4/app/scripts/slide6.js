'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const preciseDraggable = require("precise-draggable");
const courseData = require('courseData.json')
var currentProps = {
  droppedElements: 0
}
var $araytablauno= new Array(15)
var evaluador = 0;
var textorespuestauno;
var textorespuestados;
module.exports = {
  currentProps: {
      $araytablauno:{}
  },
  addEventListeners: function () {
    var self = this;
    var $draggableItems = $('.item-drag');
    preciseDraggable.currentProps.$stage = $('#stage');
    $draggableItems.each(function () {
      var draggie = preciseDraggable.setDraggable({
        $target: $(this),
        data: 1
      });
    })
    $(".item-drop").on({
      'mouseover': function () {
        if (preciseDraggable.currentProps.dragging) {
          if (!$(this).data('dropped')) {
            $(this).addClass('highlight');
          }
        }
      },
      'mouseup': function () {
        if (preciseDraggable.currentProps.dragging) {
          $("#stage").removeClass('grabbing');
          preciseDraggable.currentProps.dragging = false;
          preciseDraggable.currentProps.lastDraggable.$element.removeClass('is-dragging');

          if (!$(this).data('dropped')) {
            var target = preciseDraggable.currentProps.lastDraggable.data.target;
            var $el = preciseDraggable.currentProps.lastDraggable.$element;
            var $eldata = $el.data("drag");
            var $droptype = $(this).data("drop");
            console.log($eldata , $droptype);
            if( $eldata == $droptype){
              var posicion = $(this).data("posicion");
              var $eltablauno = $el.data("dragitem");
              $araytablauno[posicion] = $eltablauno;
              console.log("valor del data" , $eltablauno);
              console.log("array final" , $araytablauno);

              $(this).html($el.html());
              $(this).data('dropped', true);
              currentProps.droppedElements++;
              $el.remove();
            }else {
            }
              if (currentProps.droppedElements == 16) {
                $(".cont-textarea").removeClass("cont-textarea-inactive");
              }
          }
        }
      }
    });
  }
}
