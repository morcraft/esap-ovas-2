'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
module.exports = {
    currentProps: {
        seenSlides: {},
        finished: false
    },
    addEventListeners: function () {
        var self = this;
        $('.item_hoverable').on("click", function(){
            var dataitemhoverable = $(this).data("itemtres");
            module.exports.currentProps.seenSlides[dataitemhoverable] = true;
            module.exports.checkModals();
        })
    },
    checkModals: function () {
              var self = this;
        if (module.exports.currentProps.finished == true) {
            return true;
        }
        if (_.size(module.exports.currentProps.seenSlides) == 2) {
            $(".btn_cont_S3").css("display" , "block");
            $(".btn_cont_S3").on("click" , function(){
                var targetConcepts = _.pick(concepts, [
                "Confidencialidad",
                "Consentimiento informado"
                ]);
                self.ovaConcepts.showNewConceptsModal({
                    title: '¿Alguna vez ha estado involucrado en escenarios laborales donde estos términos hagan parte de las condiciones de su contrato?',
                    subtitle: 'Piense en qué tipo de situaciones, labores o responsabilidades, requieren tenerlos presentes. Revise su cofre de conceptos y ajuste su idea inicial al significado, pensando si las situaciones que supuso son las adecuadas.',
                    concepts: targetConcepts
                }).then(function (value) {
                    presentation.switchToSlide({
                        slide: $('.slide[data-slide="1"]')
                    });
                    self.ovaConcepts.updateConcepts({
                        concepts: targetConcepts,
                        action: "insert",
                        courseShortName: courseData.shortName,
                        courseUnit: courseData.unit
                    });
                    self.ovaProgress.updateFinishedTopics({
                        topics: {
                            'Recursos 1': true
                        },
                        action: 'add',
                        courseShortName: courseData.shortName,
                        courseUnit: courseData.unit
                    })
                })
            })

        }
    }
}
