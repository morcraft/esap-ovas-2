'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
  module.exports = {
    addEventListeners: function () {
        var self = this;
        // $('.btn_cont_S4').on("click", function(){
        //   var targetConcepts = _.pick(concepts, [
        //     "Equipo biomédico"
        //   ]);
        //   ovaConcepts.showNewConceptsModal({
        //       concepts: targetConcepts,
        //       title: ' Recurra a su memoria visual: ¿Con cuántos de ellos está familiarizado?',
        //       subtitle: '¿Como inciden estos elementos en los procesos de acreditación?'
        //
        //   }).then(function (value) {
        //       presentation.switchToSlide({
        //           slide: $('.slide[data-slide="1"]')
        //       });
        //       ovaConcepts.updateConcepts({
        //           concepts: targetConcepts,
        //           action: "insert",
        //           courseShortName: courseData.shortName,
        //           courseUnit: courseData.unit
        //       });
        //       self.ovaProgress.updateFinishedTopics({
        //           topics: {
        //               'Recursos 2': true
        //           },
        //           action: 'add',
        //           courseShortName: courseData.shortName,
        //           courseUnit: courseData.unit
        //       })
        //   })
        // })

        $('.item_grafica_btn').on('click', function(){
          var $valueitem = $(this).data('itemgrafica');
          swal({
              target: stageResize.currentProps.$stage.get(0),
              customClass: 'ova-themed modal_s4',
              showCloseButton: true,
              showConfirmButton: false,
              html: require("templates/modal_s4_" + $valueitem + ".hbs")()
            })
        })


    }
}
