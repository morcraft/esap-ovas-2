'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
module.exports = {
    addEventListeners: function () {
        var self = this;
        $(".btn_cont_S6").on("click" , function(){
            var targetConcepts = _.pick(concepts, [
              "Farmacovigilancia",
              "SOGCS (Sistema Obligatorio de Garantía de la Calidad en salud)"
            ]);
            self.ovaConcepts.showNewConceptsModal({
                title: 'Responda mentalmente la siguiente pregunta:',
                subtitle: '¿En qué aspectos de gestión de información cree que puede mejorar su IPS?',
                concepts: targetConcepts
            }).then(function (value) {
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });
                self.ovaConcepts.updateConcepts({
                    concepts: targetConcepts,
                    action: "insert",
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                });
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Actividad 2': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
            })
        })
    }
}
