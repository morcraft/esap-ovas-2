'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const preciseDraggable = require("precise-draggable");
const courseData = require('courseData.json')
module.exports = {
  addEventListeners: function () {
    var self = this;
    $('.icon-btn-s7').on('click' , function(){
      var dataicon = $(this).data("icons7");
      $('.info-item[data-infoitem="' + dataicon +'"]').css('opacity', '1').addClass('animated bounceInRight');
      if(dataicon == 2){
        $('.btn_cont_S7').css('display' , 'block');
      }
    })

    $('.btn_cont_S7').on('click' , function(){
      var targetConcepts = _.pick(concepts, [
        "Medicamento",
        "Tecnovigilancia"
      ]);
      self.ovaConcepts.showNewConceptsModal({
          title: 'Responda mentalmente la siguiente pregunta:',
          subtitle: '¿Qué asociación conceptual puede hacer con los términos medicamento y tecnovigilancia?. Revise su banco de conceptos para construir mentalmente esta asociación.',
          concepts: targetConcepts
      }).then(function (value) {
          presentation.switchToSlide({
              slide: $('.slide[data-slide="1"]')
          });
          self.ovaConcepts.updateConcepts({
              concepts: targetConcepts,
              action: "insert",
              courseShortName: courseData.shortName,
              courseUnit: courseData.unit
          });
          self.ovaProgress.updateFinishedTopics({
              topics: {
                  'Recursos 4': true
              },
              action: 'add',
              courseShortName: courseData.shortName,
              courseUnit: courseData.unit
          })
      })

    })

  }
}
