'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
const slide2 = require('slide2.js');

module.exports = {
  onEnter: function(args){
      var self = this;
      console.log(self.activities)
      if (_.isObject(self.activities.AutoevaluacionPrimeraUd6.textos.r2)) {
        var a=-1;
        var rtauno = self.activities.AutoevaluacionPrimeraUd6.textos.r2;
        console.log(rtauno);
        $('.word-cloud-dos li').each(function(){
          a++
          $(this).css('font-size', rtauno[a])
        })
      }else{
        var actpartuno  = slide2.currentProps.$nubeunosend
        console.log("sdfasdfasd", actpartuno);
        var a=-1;
        $('.word-cloud-dos li').each(function(){
          a++
          $(this).css('font-size', actpartuno[a])
        })
      }
    swal({
        target: stageResize.currentProps.$stage.get(0),
        customClass: 'ova-themed biggest modal_ini_act_1',
        showCloseButton: true,
        showConfirmButton: false,
        html: require("templates/modal_ini_act_3.hbs")()
      })

  },

    addEventListeners: function () {
      var self = this;

        $('.word-cloud-dos li').on('click' , function() {
          var $sizeinidos= parseInt($(this).css('font-size')) + 5;
          console.log("inivalue", $sizeinidos);
          $(this).css('font-size', $sizeinidos);
        })
        $('.info-modal').on('click' , function(){
          swal({
              target: stageResize.currentProps.$stage.get(0),
              customClass: 'ova-themed biggest modal_ini_act_1',
              showCloseButton: true,
              showConfirmButton: false,
              html: require("templates/modal_ini_act_3.hbs")()
            })
        })

        $('.button-finish-s9').on("click", function(){
          var $nomact = "Respuesta Actividad 3"
          var $nubeuno= []
          $('.word-cloud-dos li').each(function(){
            var $valuefont = $(this).css('font-size');
            var $sylefinal = $valuefont;
            $nubeuno.push($sylefinal);
            console.log($nubeuno);
          });
            swal({
                target: stageResize.currentProps.$stage.get(0),
                customClass: 'ova-send-activity',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/modal_act_1.hbs")({
                  parametros: $.param({
                    r1: $nubeuno,
                    r2: $nomact
                  }),
                  assignLink: _.get(self, 'courseActivities.assign.1.href')
                })
              })
              .then(function (value) {
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Actividad 3': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
                $('.icon-actividad[data-actividad="3"]').removeClass("animate-active").addClass("disabled");
            $('.icon-actividad[data-actividad="2"]').addClass("animated pulse");
            })
        })

    }
}
