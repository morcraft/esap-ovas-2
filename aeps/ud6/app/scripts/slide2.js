'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
var $nubeuno= new Array()
module.exports = {
  currentProps: {
      $nubeunosend:0
  },
  onEnter: function(){
    swal({
        target: stageResize.currentProps.$stage.get(0),
        customClass: 'ova-themed biggest modal_ini_act_1',
        showCloseButton: true,
        showConfirmButton: false,
        html: require("templates/modal_ini_act_1.hbs")()
      })
  },
    addEventListeners: function () {
        var self = this;
        $('.word-cloud li').on('click' , function() {
          var $sizeini= parseInt($(this).css('font-size')) + 5;
          console.log("inivalue", $sizeini);
          $(this).css('font-size', $sizeini);
        })
        $('.info-modal').on('click' , function(){
          swal({
              target: stageResize.currentProps.$stage.get(0),
              customClass: 'ova-themed biggest modal_ini_act_1',
              showCloseButton: true,
              showConfirmButton: false,
              html: require("templates/modal_ini_act_1.hbs")()
            })
        })

        $('.button-finish-s2').on("click", function(){
          var $nomact = "Respuesta Actividad 1"
          // var $nubeuno =[]
          $('.word-cloud li').each(function(){
            var $valuefont = $(this).css('font-size');
            // var $sylefinal = 'style="font-size:'+ $valuefont +'"';
            var $sylefinal = $valuefont;
            $nubeuno.push($sylefinal);
            console.log($nubeuno);
            module.exports.currentProps.$nubeunosend = $nubeuno;
          });
            swal({
                target: stageResize.currentProps.$stage.get(0),
                customClass: 'ova-send-activity',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/modal_act_1.hbs")({
                  parametros: $.param({
                    r1: $nubeuno,
                    r2: $nomact
                  }),
                  assignLink: _.get(self, 'courseActivities.assign.0.href')
                })
              })
              .then(function (value) {
                var rtacorta =$nubeuno[0,1]
                console.log(rtacorta);
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Actividad 1': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
                $(".cont-exp").removeClass("disabled");
                $(".cont-exp .cont-star img").removeClass("disabled");
                $('.icon-actividad[data-actividad="1"]').removeClass("animate-active").addClass("disabled");


                require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
                  component: 'activities',
                  method: 'addLOActivities',
                  showFeedback: true,
                  arguments: {
                      shortName: require('courseData.json').shortName,
                      unit: require('courseData.json').unit,
                      activityId: 'AutoevaluacionPrimeraUd6',
                      answers: JSON.stringify({
                        textos:{
                            r2:$nubeuno
                          }
                      })
                  }
              })
              .then(function(response){
                  console.log('response', response)
              })

            })
        })

    }
}
