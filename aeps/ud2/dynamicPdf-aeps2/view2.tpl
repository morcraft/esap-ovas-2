<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PLANEACIÓN ESTRATÉGICA</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<style>
    img {
        width: 100%;
    }
    @font-face {
        font-family: 'BebasNeue';
        src: url('../fonts/BebasNeue.otf');
    }
    @font-face {
        font-family: 'FFLight';
        src: url('../fonts/FF_DIN_Pro_Light.otf');
    }
    @font-face {
        font-family: 'FFBold';
        src: url('../fonts/FF_DIN_Pro_Bold.otf');
    }
    @font-face {
        font-family: 'FFBCondBold';
        src: url('../fonts/FF_DIN_Pro_Cond_Bold.otf');
    }
    @font-face {
        font-family: 'FFCondRegular';
        src: url('../fonts/FF_DIN_Pro_Cond_Regular.otf');
    }
    @font-face {
        font-family: 'FFCondLight';
        src: url('../fonts/FF_DIN_Pro_Cond_Light.otf');
    }
    @font-face {
        font-family: 'FFLight';
        src: url('../fonts/FF_DIN_Pro_Light.otf');
    }
    @font-face {
        font-family: 'FFThin';
        src: url('../fonts/FF_DIN_Pro_Thin.otf');
    }
    @font-face {
        font-family: 'Wingding';
        src: url('../fonts/wingding.ttf');
    }
    @font-face {
        font-family: 'century';
        src: url('../fonts/CenturyGothic.ttf');
    }
    @font-face {
        font-family: 'gothiddd';
        src: url('../fonts/GOTHIC.ttf');

    }
    @font-face {
        font-family: 'myriadRegular';
        src: url('../fonts/MyriadPro-Regular.otf');
    }

    html {

    }

    body {
        font-size: 12px;
        font-family: 'gothiddd';
    }

    h2 {
        font-family: 'gothiddd';
        font-size: 20px;
        font-weight: 100;
        text-align: center;
        color: #90133C;
    }

    p {
        font-size: 20px;
        color:#90133C;
        font-family: 'gothiddd';
    }

    @page {
        margin: 100px 25px;
    }

    header {
        position: fixed;
        top: -60px;
        left: 0px;
        right: 0px;
        height: 50px;
        background-color: #90133C;
        padding: 10px;
        border-radius: 20px;
    }

    footer {
        position: fixed;
        bottom: -60px;
        left: 0px;
        right: 0px;
        font-size: 12px;
        font-family: 'FFDINProCond-Bold';
        text-align: center;
        background-color: #90133C;
        color: #FFFFFF;
        padding: 10px;
        border-radius: 10px;
    }

    header h1 {
        font-family: 'gothiddd';
        font-size: 20px;
        font-weight: 100;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #FFF;
        padding-left: 80px;

    }

    header h2 {
        font-family: 'gothiddd';
        font-size: 16px;
        font-weight: 100;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #FFFFFF;
        padding-left: 80px;
    }

    #esap-logo {
        position: absolute;
        width: 60px;
    }

    .question-text {
        font-family: 'FFDINPro-Light';
        color: #04371c;
        font-size: 16px;
    }

    .date {
        font-family: 'FFDINPro-Light';
        color: #90133C;
        text-align: right;
        font-size: 8px;
        border-bottom: solid #90133C 1px dashed;
        margin-bottom: 10px;
    }

    .date p {
        font-size: 12px;
    }

    .date b {
        font-family: 'FFDINPro-Bold';
        font-weight: inherit;
    }

    .quiz-title {
        font-family: 'FFDINPro-Light';
        margin: 0 18px 0 10px;
        padding: 0;
        font-size: 24px;
    }

    .contRtaUno {
        width: 100%;
        height: auto;
    }

    .contRtaUno h2 {
        padding: 0%;
        margin: 0%;
        font-size: 2em;
        font-family: 'FFDINPro-Bold';
    }
    .contRtaUno p {
        padding: 0%;
        margin: 0%;
        font-size: 2em;
        text-align: center;
    }
    .contRtaUno p span{
        font-family: 'FFDINPro-Bold';
    }

    .tabla{
      width: 100%;
      height: auto;
      text-align: center;
    }
    .tabla .fila{
      width: 100%;
      height: auto;
      border: solid #90133C 2px;
      margin-top: 5px;
      padding: 5px;
    }

    .celda{
      width: 31%;
      height: auto;
      min-height: 100%;
      display: inline-block;
      vertical-align: middle;
      font-family: 'FFDINPro-Light';
      padding: 5px;
    }
    .celda-center{
      border-left: solid #90133C 2px;
      border-right: solid #90133C 2px;
    }
    .tabla .filatitulo{
      width: 100%;
      height: auto;
      margin-top: 5px;
      padding: 5px;
    }

    .celda-titulo{
      width: 31%;
      height: auto;
      min-height: 100%;
      display: inline-block;
      vertical-align: middle;
      font-family: 'FFDINPro-Bold';
      padding: 5px;
    }
    .celda-titulo-center{
      border-left: solid #90133C 2px;
      border-right: solid #90133C 2px;
    }

    .cont-rta-tres{
      width: 100%;
      padding:10px;
      font-family: 'FFDINPro-Light';
      font-size: 1.5em;
      margin: 0 auto;
      border: solid #90133C;
    }

</style>

<body>
    <header>
        <img src="assets/img/esap_logo.png" id="esap-logo">
        <h1>Estándares de Acreditación</h1>
        <h2>Acreditación para Entidades Prestadras de Salud</h2>
    </header>
    <div class="date">
        <p>Hora de generación:
            <b>{$date}</b>
        </p>
    </div>

    <h2 class="quiz-title">Respuestas Actividad 2</h2>
    <br>
    <div class="contRtaUno">
        <h2>Cuadro Comparativo</h2>
        <div class="tabla">

          <div class="filatitulo">
            <div class="celda-titulo">PROCESO DE VERIFICACIÓN DEL CUMPLIMIENTO DE LOS ESTÁNDARES DE HABILITACIÓ</div>
            <div class="celda-titulo celda-titulo-center">ESTÁNDARES DE ACREDITACIÓN</div>
            <div class="celda-titulo ">ELEMENTOS EN COMÚN</div>
          </div>

          <div class="fila">
            <div class="celda">{$columna1[$r2[0]]}</div>
            <div class="celda celda-center">{$columna1[$r2[8]]}</div>
            <div class="celda">{$r1[0]}</div>
          </div>

          <div class="fila">
            <div class="celda">{$columna1[$r2[1]]}</div>
            <div class="celda celda-center">{$columna1[$r2[9]]}</div>
            <div class="celda">{$r1[1]}</div>
          </div>

          <div class="fila">
            <div class="celda">{$columna1[$r2[2]]}</div>
            <div class="celda celda-center">{$columna1[$r2[10]]}</div>
            <div class="celda">{$r1[2]}</div>
          </div>

          <div class="fila">
            <div class="celda">{$columna1[$r2[3]]}</div>
            <div class="celda celda-center">{$columna1[$r2[11]]}</div>
            <div class="celda">{$r1[3]}</div>
          </div>

          <div class="fila">
            <div class="celda">{$columna1[$r2[4]]}</div>
            <div class="celda celda-center">{$columna1[$r2[12]]}</div>
            <div class="celda">{$r1[4]}</div>
          </div>

          <div class="fila">
            <div class="celda">{$columna1[$r2[5]]}</div>
            <div class="celda celda-center">{$columna1[$r2[13]]}</div>
            <div class="celda">{$r1[5]}</div>
          </div>

          <div class="fila">
            <div class="celda">{$columna1[$r2[6]]}</div>
            <div class="celda celda-center">{$columna1[$r2[14]]}</div>
            <div class="celda">{$r1[6]}</div>
          </div>

          <div class="fila">
            <div class="celda">{$columna1[$r2[7]]}</div>
            <div class="celda celda-center">{$columna1[$r2[15]]}</div>
            <div class="celda">{$r1[7]}</div>
          </div>
    </div>
    <div style="page-break-after: always;"></div>

    <br>
    <br>
    <h2>Equipo de Trabajo</h2>
    <div class="">
      <img src="assets/img/personajes.png" alt="">
    </div>
    <div class="cont-rta-tres">{$r3}</div>


    <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>

</body>

</html>
