const _ = require("lodash");
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const courseData = require('courseData.json');
var texto_actuno;
var textodos_actuno;
var textareasend;
var textinputsend;
module.exports = {
  currentProps: {
      textareasend:0,
      textinputsend:0
  },
    addEventListeners: function () {
        var self = this;
        $('input').click(function() {
          $('input').prop('checked' , false);
          $(this).prop('checked' , true);
        })
        $('.button-finish').on("click", function(){
          $('.second-message').css('display' , 'block');
          $('.home-exp[data-exp="1"]').addClass("animated pulse");
          $(".button-exp").removeClass("disabled");
          $(".initial-message").css("display", "none");
          $('.button-act[data-button-act="1"]').removeClass("animate-active").addClass("disabled");

          $('input:checked').each(function(){
            texto_actuno = $(this).val()
            module.exports.currentProps.textinputsend=texto_actuno;
            console.log('valor' , texto_actuno);
          })
          textodos_actuno = $('.cont-text textarea').val();
          module.exports.currentProps.textareasend=textodos_actuno;
          console.log('texto', textodos_actuno);

            swal({
                target: stageResize.currentProps.$stage.get(0),
                customClass: 'ova-send-activity',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/modal_act_1.hbs")({
                  parametros: $.param({
                    r1: texto_actuno,
                    r2: textodos_actuno
                  }),
                  assignLink: _.get(self, 'courseActivities.assign.0.href')
                })
              })
              .then(function (value) {
                console.log(self);
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Actividad 1': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });

                require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
                  component: 'activities',
                  method: 'addLOActivities',
                  showFeedback: true,
                  arguments: {
                      shortName: require('courseData.json').shortName,
                      unit: require('courseData.json').unit,
                      activityId: 'AutoevaluacionPrimera',
                      answers: JSON.stringify({
                        textos:{
                          f1:textodos_actuno,
                          f2:texto_actuno
                          }
                      })
                  }
              })
              .then(function(response){
                  console.log('response', response)
              })

            })
        })

    }
}
