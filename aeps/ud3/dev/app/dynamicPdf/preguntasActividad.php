<?php
    $options = Array(
        "Derechos de los pacientes",
        "Seguridad del paciente",
        "Acceso",
        "Registro e ingreso",
        "Evaluación de necesidades",
        "Planeación de la atención",
        "Ejecución del tratamiento",
        "Evaluación de la atención",
        "Salida y seguimiento",
        "Referencia y contrareferencia",
        "Sedes integradas en red",
        "Estándar de mejoramiento"
    )
?>