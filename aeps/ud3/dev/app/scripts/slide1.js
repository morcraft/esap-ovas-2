const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

    addEventListeners: function(){

        var self = this;

        console.log("this", self);

        $("#presentation").on("slide", function (e, i) {
            console.log("onSlide");
            $('.navigation-buttons .home').addClass("oculto");
            $('.navigation-buttons .next').addClass("oculto");
            $('.navigation-buttons .prev').addClass("oculto");

            var slideId = presentation.currentProps.$slide.attr("data-slide");
            console.log(slideId);
            if (slideId != 1) {
                setTimeout(function () {
                    $('.navigation-buttons .home').removeClass("oculto");
                    //$('.navigation-buttons .home').addClass("volver");
                }, 1000);
            }

            if (slideId == 2 || slideId == 4 || slideId == 7 || slideId == 10) {
                $('.navigation-buttons .next').removeClass("oculto");
                setTimeout(function () {
                    $('.navigation-buttons .next').addClass("highlight");
                }, 1000);
            }

            if (slideId == 3 || slideId == 5 || slideId == 8 || slideId == 11) { 
                $('.navigation-buttons .prev').removeClass("oculto");
            }
        });

        $(".pdf").on("click", function () {
            $pdf = $(this);
            $a = $pdf.find("a");
            $pdf.find("i").removeClass("animated flip infinite")

            $("[data-slide='1'] .actividad[data-actividad='2']").removeClass("clicked");            
        });

        $("[data-slide='1'] .actividad[data-actividad='1']").on("click", function () {
            swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed',
                showCloseButton: true,
                showConfirmButton: false,
                title: 'En su opinión ¿podría usted mencionar y explicar los procesos relacionados con la atención asistencial? Lo invitamos hacer la siguiente actividad para observar su conocimiento al respecto.',
            }).then(function (t) {
                presentation.switchToSlide({
                  slide: $('.slide[data-slide="2"]')
                });
            });
        });

        $("[data-slide='1'] .estrella[data-estrella='4']").on("click", function () {
            if(!$(this).hasClass("disabled")){
                swal({
                    target: stageResize.currentProps.$stage.get(0),
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    customClass: 'ova-themed video',
                    showCloseButton: true,
                    showConfirmButton: false,
                    html: require("templates/slide1Video.hbs")(self.media),
                }).then(function (t) {
                    var targetConcepts = _.pick(concepts, [
                        "Consentimiento informado"
                    ]);
                    
                    self.ovaConcepts.showNewConceptsModal({
                        title: "Recuerde la última vez que le han realizado a usted o a un familiar una cirugía.",
                        subtitle: "¿Recuerda si le pidieron firmar un consentimiento informado? Intente recordar el contenido de ese documento y revise el cofre de conceptos para constatar que el documento firmado haya cumplido con este concepto.",
                        concepts: targetConcepts
                    })
                    .then(function (t) {
                        setTimeout(function () {
                            self.ovaConcepts.updateConcepts({
                                concepts: targetConcepts,
                                action: "insert",
                                courseShortName: courseData.shortName,
                                courseUnit: courseData.unit
                            });
                        }, 500);
                        setTimeout(function () {
                            self.ovaProgress.updateFinishedTopics({
                                topics: {
                                    'recurso_3': true
                                },
                                action: 'add',
                                courseShortName: courseData.shortName,
                                courseUnit: courseData.unit
                            });
                        }, 1500);
                        setTimeout(function () {
                            $('.slide[data-slide="1"] .estrella[data-estrella="4"]').addClass("dorada"); 
                        }, 2500);
                    })
                });
            }
        });

        $("[data-slide='1'] .estrella[data-estrella='7']").on("click", function () {
            if(!$(this).hasClass("disabled")){
                swal({
                    target: stageResize.currentProps.$stage.get(0),
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    customClass: 'ova-themed video',
                    showCloseButton: true,
                    showConfirmButton: false,
                    html: require("templates/slide1Video1.hbs")(self.media),
                }).then(function (t) {
                    self.ovaProgress.updateFinishedTopics({
                        topics: {
                            'recurso_5': true
                        },
                        action: 'add',
                        courseShortName: courseData.shortName,
                        courseUnit: courseData.unit
                    });
                    setTimeout(function () {
                        $('.slide[data-slide="1"] .estrella[data-estrella="7"]').addClass("dorada"); 
                    }, 1000);
                    setTimeout(function () {
                        $('.slide[data-slide="1"] .estrella[data-estrella="8"]').removeClass("clicked"); 
                    }, 2000);
                });
            }
        });


        $('.navigation-buttons .next').addClass("oculto");
        $('.navigation-buttons .prev').addClass("oculto");
        $('.navigation-buttons .home').addClass("oculto");

        if (_.isObject(this.finishedTopics['evaluacion_inicial'])) {
            $('.slide[data-slide="3"]').data("presentation").enabled = false;
            $('.slide[data-slide="1"] .actividad[data-actividad="1"]').addClass("disabled");
            $('.slide[data-slide="1"] .learning-experience').removeClass("disabled");
            $('.slide[data-slide="1"] .estrella').removeClass("clicked");
            $('.slide[data-slide="4"]').data("presentation").enabled = true;  
        }

        if (_.isObject(this.finishedTopics['evaluacion_final'])) {
            $('.slide[data-slide="1"] .estrella[data-estrella="8"]').addClass("dorada");
            $('.slide[data-slide="11"]').data("presentation").enabled = false;
            $('.slide[data-slide="1"] .actividad[data-actividad="3"]').addClass("disabled");
        }
    }
}