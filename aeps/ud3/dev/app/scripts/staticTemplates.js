/*
    ¡No modificar!
    Script para renderizar los slides ubicados en la carpeta partials.
*/

const _ = require('lodash')
module.exports = {
    getSlidesCollection: function(args){
        if(!_.isObject(args)){
            throw('Some arguments are expected.')
        }

        var collection = []
        if(_.isArray(args.namesCollection)){
            collection = args.namesCollection
        }
        else{
            if(typeof args.number === 'undefined'){
                throw("A number of slides was expected.")
                //Maybe not all slides need to be rendered.
            }
            if(isNaN(args.startingFrom)){
                args.startingFrom = 1
            }

            _.times(args.number, function(k){
                collection.push(k + args.startingFrom)
            })
        }

        return collection
    },
    renderPartials: function(args){
        return $("[data-partial]").each(function () {
            const partial = $(this).attr("data-partial"),
                template = require("partials/" + partial + ".hbs"),
                content = $(template(args))
            $(this).replaceWith(content)
        })
    },
    renderSlides: function(args){
        if(typeof args.$target === 'undefined' || !args.target instanceof jQuery || !args.$target.length){
            throw("Invalid $target.")
        }

        if(!_.isObject(args.slidesArguments)){
            args.slidesArguments = {}
        }

        var collection = this.getSlidesCollection(args)

        _.forEach(collection, function(v, k){
            const slideFrameTemplate = require('partials/slideFrame.hbs')
            const slideContentTemplate = require('partials/slide' + v + '.hbs')
            const slidesArguments = _.merge({}, args.slidesArguments, {
                id: v
            })

            const slideContent = slideContentTemplate(slidesArguments)
            $(slideFrameTemplate({
                id: v,
                content: slideContent
            })).appendTo(args.$target)
        })
    },
    addEventListeners: function(args){
        if(!_.isObject(args)){
            throw new Error('Some arguments were expected.')
        }
        if(!_.isObject(args.slidesConfig)){
            throw new Error('A configuration of slides was expected.')
        }

        var slidesConfig = args.slidesConfig
        if(typeof slidesConfig.$target === 'undefined' || 
            !slidesConfig.target instanceof jQuery || 
            !slidesConfig.$target.length){
            throw new Error("Invalid $target.")
        }

        var collection = this.getSlidesCollection(args.slidesConfig)

        _.forEach(collection, function(v, k){
            var mod = undefined
            try {
                mod = require("slide" + v + ".js")
            } catch (error) {
                //Not all slides have a script
                //throw('Looks like slide' + v + '.js doesn\'t exist. Can\'t require it.')
            }

            if(_.isObject(mod)){
                
                if(typeof mod.addEventListeners !== 'function'){
                    console.warn('Module slide' + v + '.js\' addEventListeners property isn\'t a function.')
                    return true
                }
                if(typeof mod.$slide !== 'undefined'){
                    throw new Error('Module slide' + v + '.js seems to have a $slide property already.')
                }
                mod.$slide = $('.slide[data-slide="' + v + '"]')
                mod.slideId = v
                mod.APIInstance = args.APIInstance
                mod.ovaConcepts = args.ovaConcepts
                mod.ovaProgress = args.ovaProgress
                _.forEach(args.initResponse, function(v, k){
                    mod[k] = v.response
                })
                mod.addEventListeners()
            }
            else{
                //console.warn('Module slide' + v + '.js was expected to be an object. Found ' + typeof mod + ' instead.')
            }
        })
    } 
}
