const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

    currentProps: {
        pagina: 1,
        totalPaginas: 3,
        respuestas: [3, 7, 11]
    },

    addEventListeners: function(){

        var self = this;

        $("[data-slide='5'] .escena").on("click", function () {
            $this = $(this);
            $this.addClass("visto");
            $escena = $this.data("escena");
            $pagina = $("[data-slide='5'] .pagina[data-pagina='" + module.exports.currentProps.pagina + "']");

            console.log(module.exports.currentProps.pagina);
            setTimeout(function () {
                $this.next().removeClass("oculto");
            }, 300);

            setTimeout(function () {
                if ($pagina.find(".visto").length == $pagina.find(".escena").length) {
                    $("[data-slide='5'] .cajaRespuestas").removeClass("oculto");
                }
            }, 1000);
        });

        $("[data-slide='5'] .respuesta").on("click", function () {
            $this = $(this);
            $respuesta = $this.data("respuesta");
            $pagina = $("[data-slide='5'] .pagina[data-pagina='" + module.exports.currentProps.pagina + "']");

            if ($respuesta == module.exports.currentProps.pagina) {
                $this.addClass("correcta");
                audio = document.getElementById('correcto');
                audio.src = "img/5/correcto.wav";
                audio.load();
                audio.play();

                if($("[data-slide='5'] .correcta").length == module.exports.currentProps.respuestas[module.exports.currentProps.pagina-1]){
                    if(module.exports.currentProps.pagina == module.exports.currentProps.totalPaginas){
                        $("[data-slide='5'] .button-continuar").removeClass("escondido");
                    }else{
                        setTimeout(function () {
                            $pagina.addClass("oculto");
                            $pagina.next().removeClass("oculto");
                            $("[data-slide='5'] .cajaRespuestas").addClass("oculto");
                            module.exports.currentProps.pagina++;
                        }, 1000);
                    }
                }
            }
            else{
                audio = document.getElementById('incorrecto');
                audio.src = "img/5/incorrecto.wav";
                audio.load();
                audio.play();
            }
        });

        $("[data-slide='5'] .button-continuar").on("click", function () {
        	var targetConcepts = _.pick(concepts, [
        	    "Proceso",
        	    "Ciclo de atención del usuario"
        	]);
        	
        	self.ovaConcepts.showNewConceptsModal({
        	    title: "Conteste mentalmente las siguientes preguntas",
        	    subtitle: "¿Alguna vez usted o alguien cercano ha vivido una situación como las relatadas? ¿Qué cambios propondría en si usted tuviera que afrontarlas?",
        	    concepts: targetConcepts
        	})
        	.then(function (t) {
        	    presentation.switchToSlide({
        	      slide: $('.slide[data-slide="1"]')
        	    });
        	    setTimeout(function () {
        	        self.ovaConcepts.updateConcepts({
        	            concepts: targetConcepts,
        	            action: "insert",
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 1000);
        	    setTimeout(function () {
        	        self.ovaProgress.updateFinishedTopics({
        	            topics: {
        	                'recurso_1': true
        	            },
        	            action: 'add',
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 2000);
        	    setTimeout(function () {
        	        $("[data-slide='1'] .estrella[data-estrella='2']").addClass("dorada");
        	    }, 3000);
        	})
        });
	}
}