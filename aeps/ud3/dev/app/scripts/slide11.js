const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");
const slide3 = require("slide3.js");

module.exports = {

    currentProps: {
        newAnswers: []
    },

    addEventListeners: function(){

        var self = this;

        var finalAnswers = [];

        cargarRespuestasIniciales();

        function cargarRespuestasIniciales(){
            if (_.isObject(self.activities['Autoevaluacion primera'])) {
                respuestasIniciales = self.activities['Autoevaluacion primera'].respuestas;
                console.log(respuestasIniciales);
                for (var i = 0; i < respuestasIniciales.length; i++) {
                    $('.slide[data-slide="11"]  textarea[data-respuesta="' + (i+1) + '"]').text(respuestasIniciales[i]);
                }
                
            }
        }

        $('.slide[data-slide="11"]  textarea').on("keyup", function () {
            $this = $(this);
            $this.addClass("visto");
            $respuesta = $this.data("respuesta");

            finalAnswers[$respuesta-1] = $this.val();

            if ($("[data-slide='11'] .visto").length == $("[data-slide='11'] textarea").length) {
                $("[data-slide='11'] .button-continuar").removeClass("escondido");
            }
        });

        $('.slide[data-slide="11"] .button-continuar').on("click", function () {

            console.log(finalAnswers);

            module.exports.currentProps.newAnswers = finalAnswers;
            console.log(slide3.currentProps.oldAnswers);
            console.log(module.exports.currentProps.newAnswers);

            require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
                component: 'activities',
                method: 'addLOActivities',
                showFeedback: true,
                arguments: {
                    shortName: require('courseData.json').shortName,
                    unit: require('courseData.json').unit,
                    activityId: 'Autoevaluacion final',
                    answers: JSON.stringify({
                        respuestas: module.exports.currentProps.newAnswers
                    })
                }
            })
            .then(function(response){
                console.log('response', response);
            });

            var pdfArgs = {};
            // for (var i = 0; i < module.exports.currentProps.newAnswers.length; i++) {
            //     pdfArgs[i] = {
            //       initialAnswers: slide3.currentProps.oldAnswers[i],
            //       finalAnswers: module.exports.currentProps.newAnswers[i]
            //     };
            // }

            for (var i = 0; i < module.exports.currentProps.newAnswers.length; i++) {
                pdfArgs[i] = {
                  answer: module.exports.currentProps.newAnswers[i]
                };
            }

            console.log('pdfArgs', pdfArgs);

            var pdfArgs = $.param({
              pdfArgs: pdfArgs
            });

            swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed modalPdf',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/slide11Pdf.hbs")(
                        {
                            downloadArguments: "?" + pdfArgs,
                            assignLink: _.get(self, 'courseActivities.assign.2.href')
                        }
                    ),
            }).then(function (t) {
                presentation.switchToSlide({
                  slide: $('.slide[data-slide="1"]')
                });
                setTimeout(function () {
                    self.ovaProgress.updateFinishedTopics({
                        topics: {
                            'evaluacion_final': true
                        },
                        action: 'add',
                        courseShortName: courseData.shortName,
                        courseUnit: courseData.unit
                    });
                }, 1500);
                setTimeout(function () {
                    $('.slide[data-slide="1"] .estrella[data-estrella="8"]').addClass("dorada");
                }, 2500);
                setTimeout(function () {
                    $('.slide[data-slide="11"]').data("presentation").enabled = false;
                    $('.slide[data-slide="1"] .actividad[data-actividad="3"]').addClass("disabled");
                }, 3500);
            });
        });
    }
}