const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {
	slideSteps: 3,
    translationSpeed: 500,
    currentProps: {},
    addNavigationEvents: function(){    
        var self = this;
        self.$slide.attr('data-step', 3)
        var $buttons = self.$slide.find('.buttons > *').on('click', function(){
            var target = $(this).attr('data-switch-to');
            var state = self.currentProps;
            var currentStep = state.currentStep;
            if(isNaN(currentStep)){
                currentStep = state.currentStep = 3;
            }
            self.$slide.attr('data-step', currentStep);
            if(target === 'prev' && currentStep === 1 || target === 'next' && currentStep === self.slideSteps){
                return true;
            }

            currentStep = state.currentStep = target === 'next' ? currentStep + 1 : currentStep - 1;
            self.$slide
                .addClass('translating')
                .attr('data-step', currentStep);

            setTimeout(function(){
                self.$slide.removeClass('translating');
            }, self.translationSpeed || 1);
        })
    },

    addEventListeners: function(){

    	var self = this;
        
        self.addNavigationEvents();

        $("[data-slide='6'] .opcion").on("click", function () {
        	$this = $(this);
            $this.addClass("visto");
            $opcion = $this.data("opcion");

            setTimeout(function () {
                $this.removeClass("noColor");
            }, 200);
            setTimeout(function () {
                $("[data-slide='6'] .info[data-info='" + $opcion + "']").removeClass("oculto");
            }, 500);

            setTimeout(function () {
                if ($("[data-slide='6'] .visto").length == $("[data-slide='6'] .opcion").length) {
                    $("[data-slide='6'] .button-continuar").removeClass("escondido");
                }
            }, 1500);
        });

        $("[data-slide='6'] .button-continuar").on("click", function () {
            var targetConcepts = _.pick(concepts, [
                "Humanización del servicio", 
                "Satisfacción"
            ]);
            
            self.ovaConcepts.showNewConceptsModal({
                title: "Conteste mentalmente la siguiente pregunta",
                subtitle: "¿Cuál de los subgrupos relacionados observa usted necesita un reajuste en la IPS más cercana a usted?",
                concepts: targetConcepts
            })
            .then(function (t) {
                presentation.switchToSlide({
                  slide: $('.slide[data-slide="1"]')
                });
                setTimeout(function () {
                    self.ovaConcepts.updateConcepts({
                        concepts: targetConcepts,
                        action: "insert",
                        courseShortName: courseData.shortName,
                        courseUnit: courseData.unit
                    });
                }, 1000);
                setTimeout(function () {
                    self.ovaProgress.updateFinishedTopics({
                        topics: {
                            'recurso_2': true
                        },
                        action: 'add',
                        courseShortName: courseData.shortName,
                        courseUnit: courseData.unit
                    });
                }, 2000);
                setTimeout(function () {
                    $("[data-slide='1'] .estrella[data-estrella='3']").addClass("dorada"); 
                }, 3000);
            })
        });

	}
}