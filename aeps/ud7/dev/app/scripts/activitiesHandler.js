const courseData = require('courseData.json')
const _ = require('lodash')

module.exports = {
    add: function(args){
        if(!_.isObject(args))
            throw new Error('Invalid arguments')

        if(!_.isObject(args.APIInstance))
            throw new Error('Invalid APIInstance')

        if(!_.isString(args.activityId))
            throw new Error('Invalid activityId')
        
        if(!_.isObject(args.answers))
            throw new Error('Invalid answers')

        return args.APIInstance.makeRequest({
            component: 'activities',
            method: 'addLOActivities',
            showFeedback: true,
            arguments: {
                shortName: courseData.shortName,
                unit: courseData.unit,
                activityId: args.activityId,
                answers: JSON.stringify(args.answers)
            }
        })
    }
}