const _ = require('lodash')
const presentation = require('presentation')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')

module.exports = {
    props: {
        textInput: {}
    },
    finish: function(){
        var self = module.exports
        self.finished = true
        swal(_.merge(swalOptions, {
            html: require('templates/slide7Modal.hbs')({
                pdfArgs: $.param({
                    a: self.props.textInput
                }),
                forumLink: _.get(self, 'courseActivities.forum.0.href')
            }),
            customClass: 'ova-themed bigger slide7Modal ova-send-activity'
        }))
        .then(function(){
            presentation.switchToSlide({
                slide: $('.slide[data-slide="1"]')
            })

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Actividad 2': true
                },
                action: 'add'
            })
        })
    },
    addEventListeners: function(){
        var self = this
        self.$slide.find('.icon').on('click', function(){
            if($(this).hasClass('disabled')) return true
            $(this).siblings('.inner-content').removeClass('disabled')
            var $next = $(this).parent().next()
            $next.find('.icon').removeClass('disabled').addClass('click-me')

        })
        self.$slide.find('.finish').on('click', self.finish)
        self.$slide.find('textarea').on('keyup paste', function(){
            var val = $(this).val()
            self.props.textInput = val
            self.$slide.find('.button.finish').removeClass('disabled')
        })

    }
}