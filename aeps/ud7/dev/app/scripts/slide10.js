const _ = require('lodash')
const words = require('./slide3Words.js')
const wordContainer = require('templates/wordContainer.hbs')
const swalOptions = require('swalOptions.js')
const swal = require('sweetalert2')
const presentation = require('presentation')

module.exports = {
    props: {
        answers: {},
        activityId: 'Actividad 3',
    },
    onEnter: function(){
        return swal(_.merge(swalOptions, {
            html: require('templates/slide10Modal.hbs')(),
            customClass: 'ova-themed bigger slide10Modal'
        }))
    },
    showWordsMenu: function(args){
        if(!_.isObject(args))
            throw new Error('Invalid arguments')

        if(!_.isNumber(args.index))
            throw new Error('Invalid index')

        if(!(args.$element instanceof jQuery))
            throw new Error('Invalid $element')

        var self = this
        var index = args.index
        if(self.props.$wordsContainer instanceof jQuery){
            self.props.$wordsContainer.remove()
        }

        self.props.$container = $('<div class="words-container"/>')
        _.forEach(words, function(word, _index){
            var $word = $(wordContainer({
                word: word
            }))
            .on('click', function(){
                self.props.answers[index] = _index
                args.$element.html(words[_index])
                $(this).addClass('selected')
                    .siblings().removeClass('selected')

                swal.close()
            })
            .appendTo(self.props.$container)

            if(self.props.answers[index] === _index){
                $word.addClass('selected')
            }
        })

        swal(_.merge(swalOptions, {
            html: '<div class="words-modal-container></div>"',
            customClass: 'ova-themed biggest words-modal'
        }))

        self.props.$container.appendTo($('.swal2-modal'))
    },
    parseSelectors: function(){
        var self = this
        var whiteSpace = ''
        _.times(20, function(){
            whiteSpace+='&nbsp'
        })

        var $b = self.$slide.find('.content').children('b')
        
        $b.each(function(){
            $(this).html(whiteSpace)
        })

        $b.on('click', function(){
            self.showWordsMenu({
                $element: $(this),
                index: $b.index($(this))
            })
        })
    },
    finish: function(){
        var self = module.exports
        self.finished = true
        swal(_.merge(swalOptions, {
            html: require('templates/PDFModal.hbs')({
                pdfArgs: $.param({
                    a: self.props.answers
                }),
                assignLink: _.get(self, 'courseActivities.assign.1.href'),
                activityId: 3
            }),
            customClass: 'ova-themed bigger ova-send-activity PDFModal',
        }))
        .then(function(){
            presentation.switchToSlide({
                slide: $('.slide[data-slide="1"]')
            })

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Actividad 3': true
                },
                action: 'add'
            })
        })
    },
    addEventListeners: function(){
        var self = this
        self.parseSelectors()
        self.$slide.find('.button.finish').on('click', self.finish)
    }
}