const _ = require('lodash')
const words = require('./slide3Words.js')
const wordContainer = require('templates/wordContainer.hbs')
const swalOptions = require('swalOptions.js')
const swal = require('sweetalert2')
const presentation = require('presentation')

module.exports = {
    onEnter: function(){
        return swal(_.merge(swalOptions, {
            html: require('templates/slide9Modal.hbs')(),
            customClass: 'ova-themed bigger slide9Modal'
        }))
    },
    addEventListeners: function(){}
}