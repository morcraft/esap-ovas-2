const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const presentation = require('presentation')

module.exports = {
    props: {
        seenModals: {}
    },
    onEnter: function(){
        var self = this
        swal(_.merge(swalOptions, {
            html: require('templates/slide4Modal.hbs')(),
            customClass: 'ova-themed bigger slide4Modal'
        }))
    },
    finish: function(){
        var self = this
        self.finished = true

        presentation.switchToSlide({
            slide: $('.slide[data-slide="1"]')
        })

        return self.ovaProgress.updateFinishedTopics({
            topics: {
                'Ciclo de Deming o Ciclo PHVA de mejoramiento continuo': true
            },
            action: 'add'
        })
    },
    showModal: function(id){
        if(!_.isNumber(id) && !_.isString(id))
            throw new Error('Invalid id') 

        var self = this
        swal(_.merge(swalOptions, {
            html: require('templates/slide4Modal' + id + '.hbs')(),
            customClass: 'ova-themed bigger slide4Modal slide4Modal' + id
        }))
        .then(function(){
            self.props.seenModals[id] = true
            if(_.size(self.props.seenModals) === 4){
                self.finish()
            }
        })
    },
    addEventListeners: function(){
        var self = this
        self.$slide.find('.text').on('click', function(){
            var id = $(this).attr('data-id')
            $(this).addClass('clicked')
            self.showModal(id)
        })
    }
}