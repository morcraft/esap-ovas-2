<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ESTÁNDARES DE MEJORAMIENTO DE LA CALIDAD</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        {include file='base.css'}
        {include file='styles.css'}
    </style>
    <body>
        <header>
            <img src="assets/img/esap_logo.png" id="esap-logo">
            <h1>ACREDITACIÓN PARA ENTIDADES PRESTADORAS DE SALUD</h1>
            <h2>ESTÁNDARES DE MEJORAMIENTO DE LA CALIDAD</h2>
        </header>
        <hr>

        <div class="date">
            <p>Hora de generación: <b>{$date}</b></p>
        </div>

        <h2 class="quiz-title">Escriba la importancia que representa el mejoramiento continuo de una institución y mencione las consideraciones para un resultado positivo.</h2>
  
        <h2 class="content">{$a}</h2>

        <h2>Envíe este documento al aula por medio del botón "Enviar actividad" de la pestaña anterior que lo llevará al aula del curso. Luego de enviar sus respuestas cierre la pestaña del aula y siga explorando el recurso en la pestaña inicial.</h2>
        
        <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>
        
    </body>
</html>
