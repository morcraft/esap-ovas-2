<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require 'vendor/autoload.php';
require 'options.php';

use Dompdf\Dompdf;
// echo '<pre>';
// var_export($_GET);
// echo '</pre>';
// exit();
$smarty = new Smarty();
$smarty->error_reporting = 1;
$smarty->caching = 0;
$optionsLength = count($options);
if(isset($_GET['a'])){
    for($i = 0; $i < $optionsLength; $i++){
        if(!isset($_GET['a'][$i])){
            $_GET['a'][$i] = $optionsLength - 1;
        }
    }
}

$smarty->assign($_GET);
$smarty->assign(array(
    'date' => date("Y-m-d h:i:sa"),
    'o' => $options
));
$view = $smarty->fetch('view.tpl');


$dompdf = new Dompdf();
$dompdf->loadHtml($view);
$dompdf->setPaper('A4', 'portrait');
$dompdf->render();
$dompdf->stream("respuestas.pdf", array("Attachment" => false));
//$dompdf->stream("justificacion_actividad_MASC.pdf", array("Attachment" => true));
exit(0);
?>
