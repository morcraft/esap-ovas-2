'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const ovaProgress = require("ova-progress");
const ovaConcepts = require("ova-concepts");
const _ = require("lodash");
const preciseDraggable = require("precise-draggable");
const courseData = require('courseData.json')
const slide6 = require ("slide6.js");
var currentProps = {
  droppedElements: 0
}
var evaluador = 0;
var textorespuestauno;
var textorespuestados;
module.exports = {
  addEventListeners: function () {
    var self = this;
    var $draggableItems = $('.cara');
    preciseDraggable.currentProps.$stage = $('#stage');
    $draggableItems.each(function () {
      var draggie = preciseDraggable.setDraggable({
        $target: $(this),
        data: 1
      });
    })
    $(".cara-drop").on({
      'mouseover': function () {
        if (preciseDraggable.currentProps.dragging) {
          if (!$(this).data('dropped')) {
            $(this).addClass('highlight');
          }
        }
      },
      'mouseup': function () {
        if (preciseDraggable.currentProps.dragging) {
          $("#stage").removeClass('grabbing');
          preciseDraggable.currentProps.dragging = false;
          preciseDraggable.currentProps.lastDraggable.$element.removeClass('is-dragging');
          if (!$(this).data('dropped')) {
            var matches = false;
            var target = preciseDraggable.currentProps.lastDraggable.data.target;
            var $el = preciseDraggable.currentProps.lastDraggable.$element;
            var $eltype = $el.data("type")
            var $background = $el.css("background-image");
            if( $eltype == 1){
              $(this).css("background-image", "" + $background + "");

                // $(this).html($el.html());
              $(this).data('dropped', true);
              $el.remove();
              currentProps.droppedElements++;
            }else {
              $el.css("border", "solid red 5px");
              $el.css("opacity" , "0.5")
              $el.css("animation-iteration-count", "2");
              $el.addClass("animated shake");
            }
              if (currentProps.droppedElements == 4) {
                $(".cont-justificacion").css("display", "block");
              }
          }
        }

      }
    });
    $(".downpdf-act2").on("click", function () {
      var actpartuno = [];
      $(".item-input textarea").each(function() {
        var texto = $(this).val();
        actpartuno.push(texto);
        console.log("sisisi" , actpartuno);
      })
      var actpartdos  = slide6.currentProps.$araytablauno
      console.log("sdfasdfasd", actpartdos);

      var actparttres= $(".cont-justificacion textarea").val();
      console.log("partetres" , actparttres);
      // var textorespuestacuatro = $(".item_texto[data-texto='9'] , .item_texto[data-texto='10'] , .item_texto[data-texto='11']").text();
      swal({
          target: stageResize.currentProps.$stage.get(0),
          customClass: 'ova-send-activity',
          showCloseButton: true,
          showConfirmButton: false,
          html: require("templates/modal_act_2.hbs")({
            parametros: $.param({
              r1: actpartuno,
              r2: actpartdos,
              r3: actparttres
            })
          })
        })
    });
    $('.send-act-2').on("click" , function() {
      $('.button-finish-act2').css('display', 'block');
      console.log("asdfasdfasdf");
    })
    $('.button-finish-act2').on("click" , function() {
      presentation.switchToSlide({
        slide: $('.slide[data-slide="1"]')
      });
      self.ovaProgress.updateFinishedTopics({
        topics: {
            'Actividad 2': true
        },
        action: 'add',
        courseShortName: courseData.shortName,
        courseUnit: courseData.unit
    });
    })
  }
}
