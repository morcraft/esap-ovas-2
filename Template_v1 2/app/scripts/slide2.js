'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const ovaProgress = require("ova-progress");
const ovaConcepts = require("ova-concepts");
const _ = require("lodash");
const courseData = require('courseData.json');
module.exports = {
    addEventListeners: function () {
        var self = this;
        $('.button-finish').on("click", function(){
            swal({
                target: stageResize.currentProps.$stage.get(0),
                customClass: 'ova-send-activity',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/modal_act_1.hbs")({
                  parametros: "",
                  assignLink: _.get(self, 'courseActivities.assign.0.href')

                })
              })
              .then(function (value) {
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Actividad 1': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
                $('.home-exp[data-exp="1"]').addClass("animated pulse");
                $(".button-exp").removeClass("disabled");
                $(".initial-message").css("display", "none");
                $('.button-act[data-button-act="1"]').removeClass("animate-active").addClass("disabled");

            })
        })

    }
}
