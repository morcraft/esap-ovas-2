(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Símbolo2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FD8034").s().p("AC+LVQg9gQgrgtIgVAHQhgAfhZgvQhagvgehhQgVhDAShEQhJgYg3g3Qg4g3gXhKQgZhSAThUQAUhTA6g+QgcgmgNgrQgdhbAkhZQAkhZBTguIABgBQAJg+AogxQApgyA+gTQBXgcBQArQBQAqAbBXIFBP/QAbBXgqBRQgrBQhWAbQggAKghAAQgcAAgcgHgABvJrQAhApAzAQQAzAQAygQQBDgVAhg/QAhg/gVhDIlAwAQgVhDg/ghQg+ghhDAVQhEAVggA/QghA/AVBDQAOAsAhAfIghAkQgrgogSg4QgFgSgDgTQg5ApgWBDQgXBEAVBDQAPAuAgAjIAQASIgSARQg5A0gVBLQgUBLAXBKQAUA/AvAvQAwAvA/ATQAWgrAlggQAmghAwgPIAPAuQhNAYgmBIQgmBIAYBNQAZBNBHAmQBIAlBMgYQAQgFAPgIIASgJg");
	this.shape.setTransform(46.5,73.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FD8034").s().p("AgNBpQgtgXgPgxQgPgvAYgsQAXgtAvgPIAPAuQgcAJgOAbQgOAbAJAcQAJAdAaAOQAbAOAdgJIAOAuQgTAGgSAAQgdAAgagOg");
	this.shape_1.setTransform(57.9,104.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FD8034").s().p("AgUBJQAcgJAOgbQAOgbgJgcQgJgdgagOQgagOgdAJIgPgvQAwgPAsAYQAtAXAPAxQAPAvgYAtQgXAsgvAPg");
	this.shape_2.setTransform(51.1,55.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FD8034").s().p("AhogNQAXgtAwgPQAwgPAsAYQAtAXAPAvIguAPQgJgcgbgOQgbgOgcAJQgdAJgOAaQgOAaAJAdIgvAPQgPgwAYgsg");
	this.shape_3.setTransform(38.7,17.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FD8034").s().p("AhTArQgPgvAXgtQAYgsAwgPQAvgPAtAXIgXArQgagOgdAJQgcAJgOAbQgOAaAJAdQAJAdAaAOIgWArQgtgXgPgxg");
	this.shape_4.setTransform(30.6,75.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FD8034").s().p("AgTAqQg1gcgTg5IAvgPQAMAnAjATQAjARAngMIAPAvQgXAHgWAAQgiAAgggRg");
	this.shape_5.setTransform(40.9,125.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FD8034").s().p("AgxAeQAdgJAOgaQANgbgIgdIAtgOQAQAwgYArQgXAugwAPg");
	this.shape_6.setTransform(78.2,128.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FDB32B").s().p("ADDKjQgzgPghgqIgMgQIgSAKQgPAHgQAFQhMAZhIgmQhHglgZhOQgYhNAmhHQAmhIBNgYIgPgvQgwAPgmAhQglAggWAsQg/gTgwgvQgvgvgUg/QgXhKAUhMQAVhKA5g1IASgQIgQgSQgggjgPguQgVhEAXhDQAWhDA5gpQADATAFARQASA5ArAnIAhgkQghgegOgsQgVhEAhg+QAgg/BEgVQBDgVA+AhQA/AgAVBEIFAP/QAVBEghA/QghA+hDAVQgZAIgaAAQgZAAgZgIg");
	this.shape_7.setTransform(46.5,73.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Símbolo2, new cjs.Rectangle(0,0,93.1,146.5), null);


(lib.Símbolo1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#1EC296").s().p("AgkLCQhQgqgbhXIhQkAIgBAAIgriMIAAABIjFp0QgbhXAqhRQArhQBWgbQA+gUA/ASQA8ARArAvIABgBQBegJBQA1QBQA0AdBbQANAtgBAuQBTARBAA6QBAA6AaBSQAXBKgOBNQgOBLgvA+QA2AuAVBDQAeBggvBaQgvBZhgAfIgWAFQgKA+goAwQgqAwg7ATQgiAKghAAQgzAAgxgagAkxqjQhDAVghA/QghA/AVBDIFAQAQAVBDA/AhQA+AhBDgVQAygQAhgqQAhgqADg0IACgUIAUgDQATgDANgEQBOgYAlhIQAmhIgZhNQgYhNhHglQhIgmhNAYIgPguQAxgPAyAFQAwAFArAYQApg0ANhAQAMhCgUg/QgXhKg8gzQg8gyhNgLIgYgDIADgYQAGgwgOgtQgVhEg5gqQg5gqhFgCQAJASAGARQARA4gNA5IgvgKQAKgtgOgsQgVhDg/ghQgmgUgpAAQgZAAgaAIg");
	this.shape.setTransform(47.5,73.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#1EC296").s().p("AgUBJQAcgJAOgbQAOgbgJgcQgJgdgagOQgbgOgdAJIgOguQAwgPAsAXQAsAXAPAxQAPAvgXAtQgXAsgwAPg");
	this.shape_1.setTransform(54.1,102.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#1EC296").s().p("AgNBpQgtgXgPgxQgPgvAYgsQAXgtAvgPIAPAuQgcAJgOAbQgOAbAJAcQAJAdAaAOQAaAOAdgJIAPAuQgSAGgTAAQgcAAgbgOg");
	this.shape_2.setTransform(30.3,54.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#1EC296").s().p("AhogNQAXgtAxgPQAvgPAtAYQAsAXAPAvIguAPQgJgcgbgOQgbgOgcAJQgdAJgOAaQgOAbAJAdIgvAOQgPgwAYgsg");
	this.shape_3.setTransform(21.7,17.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#1EC296").s().p("AA6BDQAOgbgJgdQgJgcgbgOQgbgOgcAJQgdAJgOAbIgrgXQAXgtAxgPQAvgPAtAYQAsAXAPAwQAQAwgYAsg");
	this.shape_4.setTransform(57.6,60);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#1EC296").s().p("Ag6AtQAngMATgjQARgjgMgnIAvgPQASA6gcA1QgcA2g5ASg");
	this.shape_5.setTransform(79.4,108.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#1EC296").s().p("AgQAkQgsgYgPgvIAugOQAJAdAaAOQAbANAdgIIAPAtQgTAGgTAAQgcAAgbgOg");
	this.shape_6.setTransform(50.1,131.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#7CFED9").s().p("AgNKYQg/ghgVhEIlBv/QgVhEAhg+QAhg/BDgVQBEgVA/AhQA+AgAVBEQAOAsgKAsIAwALQANg5gSg5QgFgQgJgSQBFABA4AqQA6ArAVBEQAOAsgGAwIgDAYIAYAEQBNAKA8AzQA8AyAXBKQAUA/gNBCQgMBAgpA0QgsgXgwgFQgygGgwAPIAOAvQBNgYBIAlQBIAmAYBNQAYBNglBIQgmBHhNAYQgNAFgUACIgUADIgBAUQgEA1ggAqQghAqgyAPQgbAJgZAAQgoAAglgUg");
	this.shape_7.setTransform(47.6,73.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Símbolo1, new cjs.Rectangle(0,0,95.1,146.5), null);


(lib.Interpolación1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#771736").s().p("AgwAxQgVgUAAgdQAAgcAVgUQAUgVAcAAQAdAAAUAVQAVAUAAAcQAAAdgVAUQgUAVgdAAQgcAAgUgVgAgZgZQgLALAAAOQAAAQALAKQAKALAPAAQAPAAALgLQALgKAAgQQAAgOgLgLQgLgLgPAAQgPAAgKALg");
	this.shape.setTransform(-71.9,84);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FDB32B").s().p("AgUBLIAAg1Ig2AAIAAgqIA2AAIAAg2IApAAIAAA2IA2AAIAAAqIg2AAIAAA1g");
	this.shape_1.setTransform(102,14.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#79C8B0").s().p("AhNArIB5h4IAiAjIh5B4g");
	this.shape_2.setTransform(-90.5,-68);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#79C8B0").s().p("AgwgOIAigiIA/A+IgjAjg");
	this.shape_3.setTransform(-96.3,-65.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#771736").s().p("AA0QMQgFgFAAgGQgBgHAFgFQAFgFAGgBQBDgEBAgMQAHgBAFADQAGAEABAHQABAHgDAFQgEAGgHABQhGANhBAEIgBAAQgGAAgFgEgAhJQPQg+gEhHgOQgHgBgEgGQgDgFABgHQABgHAGgDQAGgEAGABQBBANBBAFQAHAAAFAFQAEAFAAAHQgBAHgEAEQgFAEgGAAgAE2PSQgCgHADgGQADgFAHgDQA+gVA8gdQAGgDAGADQAGACADAGQADAGgCAGQgCAHgGADQg/Aeg/AVIgFABQgMAAgEgLgAlSPaQhDgYg6gdQgGgDgCgHQgDgGADgGQADgGAHgCQAGgCAGADQA5AcBBAXQAGACAEAGQADAGgDAGQgEALgLAAgAIoNgQgDgFABgHQABgGAGgEQA4gmAxgqQAFgFAHABQAHAAAEAFQAEAGAAAGQgBAHgFAEQgwArg8AoQgEADgFAAQgIAAgGgIgAL1K0QgFgFAAgGQgBgHAFgFQArgxAmg3QAEgGAGgBQAHgBAFADQAGAEABAHQACAGgEAGQgoA6gsAyQgFAFgHAAQgGAAgFgEgAsRKsQgrgygng6QgEgGABgGQACgHAFgEQAGgDAGABQAHABAEAGQAjA0AtA1QAEAFAAAGQgBAHgFAFQgFAEgFAAQgHAAgGgGgAOPHZQgGgDgCgHQgDgGADgGQAfg/AUg7QADgHAGgDQAGgCAGACQAHACADAGQADAGgDAHQgVA+gfA/QgFAJgKAAQgEAAgDgBgAunHKQgdg8gXhCQgCgHADgGQADgGAGgCQAHgCAGADQAGADACAGQAWBCAcA4QADAGgCAHQgCAGgGADIgHACQgKAAgFgJgAv4DSQgFgEgBgGQgNhGgEhBQAAgHAEgFQAFgFAGAAQAHgBAFAFQAFAEABAHQADA9ANBGQACAGgEAGQgEAGgHABIgDAAQgFAAgFgDgAQAgxQgHAAgEgEQgFgEgBgHQgDg/gOhEQgBgHAEgFQAEgGAGgBQAHgBAGAEQAFADABAHQAOBEAEBDQAAAHgEAFQgFAFgGAAgAv/g2QgHAAgEgFQgFgFABgGIAAgDQAEhDAOhBQABgHAGgEQAFgDAHABQAHABADAGQAEAGgBAGQgNBBgFBAQAAAHgFAEQgFAFgGAAgAO8lCQgUg9geg9QgDgGACgGQACgHAGgDQAHgDAGACQAGACADAGQAfA/AVA/QADAHgDAGQgDAGgHACIgFABQgMAAgEgLgAvPk8QgGgCgDgGQgDgGACgHQAVg+AghAQADgGAGgCQAHgCAGADQAGADACAHQACAGgDAGQgeA8gVA+QgEALgLAAIgGgBgANIouQgmg5gqgwQgEgFAAgHQABgHAFgEQAFgFAGABQAHAAAFAFQAuA1AkA4QAEAFgBAHQgCAHgFADQgDADgGAAQgJAAgFgHgAKbr1Qgygsg3gkQgFgEgCgHQgBgGAEgGQAEgFAGgCQAHgBAGAEQA6AmAxAtQAFAEABAHQAAAHgEAFQgEAFgIAAQgHAAgEgEgAqtr5QgFgFABgHQAAgHAFgEQAxgrA7goQAGgDAHABQAHABADAGQAEAFgBAHQgCAHgFADQg4AmgxAqQgEAEgHAAQgIAAgEgFgAHAuIQg8gdg+gVQgGgCgDgHQgDgGACgGQADgHAFgDQAGgCAHACQA+AVBAAfQAGADACAHQACAGgDAGQgFAJgKAAQgDAAgEgCgAnRuRQgDgGACgGQACgHAGgDQA/gfA/gVQAHgCAGADQAGADACAHQACAGgDAGQgDAGgGACQg+AVg8AeQgDABgEAAQgLAAgEgJgADHvdQhCgMg+gFIgDAAQgHAAgEgGQgFgFABgHQABgGAFgFQAFgEAHAAIACABQBCAEBCANQAHACAEAFQADAGgBAHQgBAFgEAEQgFAEgGAAgAjPvhQgFgEgBgFQgBgHAEgGQADgFAHgCQBGgNBBgEQAHAAAFAEQAFAFAAAGQABAHgEAFQgFAFgHABQg9ADhGANIgDABQgGAAgEgEg");
	this.shape_4.setTransform(-5.5,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-109.5,-104,219.1,208);


// stage content:
(lib.brain = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// circ
	this.instance = new lib.Interpolación1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(125.5,123.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:360},71).wait(1));

	// Capa_3
	this.instance_1 = new lib.Símbolo1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(153,116.8,1,1,0,0,0,47.6,73.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({regX:47.5,regY:73.3,scaleX:1.01,scaleY:1.01,x:152.9,y:116.9},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:116.8},0).wait(1).to({scaleX:1.03,scaleY:1.03,y:116.9},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.05,scaleY:1.06},0).wait(1).to({scaleX:1.06,scaleY:1.06},0).wait(1).to({scaleX:1.07,scaleY:1.07,y:116.8},0).wait(1).to({scaleX:1.07,scaleY:1.08,y:116.9},0).wait(1).to({scaleX:1.08,scaleY:1.08},0).wait(1).to({scaleX:1.09,scaleY:1.09},0).wait(1).to({scaleX:1.09,scaleY:1.1},0).wait(1).to({scaleX:1.1,scaleY:1.1},0).wait(1).to({scaleX:1.11,scaleY:1.11},0).wait(1).to({scaleX:1.11,scaleY:1.12,y:116.8},0).wait(1).to({scaleX:1.12,scaleY:1.12,y:116.9},0).wait(1).to({scaleX:1.13,scaleY:1.13},0).wait(1).to({scaleX:1.13,scaleY:1.14},0).wait(1).to({scaleX:1.14,scaleY:1.14,y:116.8},0).wait(1).to({scaleX:1.14,scaleY:1.15,y:116.9},0).wait(1).to({scaleX:1.15,scaleY:1.15,y:116.8},0).wait(1).to({scaleX:1.15,scaleY:1.16},0).wait(1).to({scaleX:1.16,scaleY:1.16},0).wait(1).to({scaleX:1.15,scaleY:1.16,y:116.9},0).wait(1).to({scaleX:1.15,scaleY:1.15},0).wait(1).to({scaleX:1.14,scaleY:1.14,y:116.8},0).wait(1).to({scaleX:1.13,scaleY:1.14,y:116.9},0).wait(1).to({scaleX:1.13,scaleY:1.13,y:116.8},0).wait(1).to({scaleX:1.12,scaleY:1.13},0).wait(1).to({scaleX:1.12,scaleY:1.12,y:116.9},0).wait(1).to({scaleX:1.11,scaleY:1.11},0).wait(1).to({scaleX:1.11,scaleY:1.11},0).wait(1).to({scaleX:1.1,scaleY:1.1,y:116.8},0).wait(1).to({scaleX:1.09,scaleY:1.1},0).wait(1).to({scaleX:1.09,scaleY:1.09,y:116.9},0).wait(1).to({scaleX:1.08,scaleY:1.09,y:116.8},0).wait(1).to({scaleX:1.08,scaleY:1.08,y:116.9},0).wait(1).to({scaleX:1.07,scaleY:1.08},0).wait(1).to({scaleX:1.07,scaleY:1.07},0).wait(1).to({scaleX:1.07,scaleY:1.07,y:116.8},0).wait(1).to({scaleX:1.06,scaleY:1.06,y:116.9},0).wait(1).to({scaleX:1.06,scaleY:1.06},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.05,scaleY:1.05,y:116.8},0).wait(1).to({scaleX:1.04,scaleY:1.04,y:116.9},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.03,scaleY:1.04},0).wait(1).to({scaleX:1.03,scaleY:1.03,y:116.8},0).wait(1).to({scaleX:1.03,scaleY:1.03,y:116.9},0).wait(1).to({scaleX:1.03,scaleY:1.03,y:116.8},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:116.9},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:116.8},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:116.9},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:116.8},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:116.9},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1,y:116.8},0).wait(1).to({scaleX:1,scaleY:1,y:116.9},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(2).to({scaleX:1,scaleY:1,y:116.8},0).wait(2).to({y:116.9},0).wait(1));

	// Capa_4
	this.instance_2 = new lib.Símbolo2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(86.3,131.9,1.164,1.164,0,0,0,46.5,73);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({regY:73.2,scaleX:1.16,scaleY:1.16,y:132.1},0).wait(1).to({scaleX:1.15,scaleY:1.15},0).wait(1).to({scaleX:1.14,scaleY:1.14},0).wait(1).to({scaleX:1.13,scaleY:1.13},0).wait(1).to({scaleX:1.12,scaleY:1.12},0).wait(1).to({scaleX:1.12,scaleY:1.12},0).wait(1).to({scaleX:1.11,scaleY:1.11},0).wait(1).to({scaleX:1.1,scaleY:1.1},0).wait(1).to({scaleX:1.1,scaleY:1.1},0).wait(1).to({scaleX:1.09,scaleY:1.09},0).wait(1).to({scaleX:1.08,scaleY:1.08},0).wait(1).to({scaleX:1.07,scaleY:1.07},0).wait(1).to({scaleX:1.07,scaleY:1.07},0).wait(1).to({scaleX:1.06,scaleY:1.06},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.06,scaleY:1.06},0).wait(1).to({scaleX:1.06,scaleY:1.06},0).wait(1).to({scaleX:1.07,scaleY:1.07},0).wait(1).to({scaleX:1.07,scaleY:1.07},0).wait(1).to({scaleX:1.08,scaleY:1.08},0).wait(1).to({scaleX:1.08,scaleY:1.08},0).wait(1).to({scaleX:1.09,scaleY:1.09},0).wait(1).to({scaleX:1.09,scaleY:1.09},0).wait(1).to({scaleX:1.1,scaleY:1.1},0).wait(1).to({scaleX:1.1,scaleY:1.1},0).wait(1).to({scaleX:1.11,scaleY:1.11},0).wait(1).to({scaleX:1.11,scaleY:1.11},0).wait(1).to({scaleX:1.11,scaleY:1.11},0).wait(1).to({scaleX:1.12,scaleY:1.12},0).wait(1).to({scaleX:1.12,scaleY:1.12},0).wait(1).to({scaleX:1.13,scaleY:1.13},0).wait(1).to({scaleX:1.13,scaleY:1.13},0).wait(1).to({scaleX:1.13,scaleY:1.13},0).wait(1).to({scaleX:1.13,scaleY:1.13},0).wait(1).to({scaleX:1.14,scaleY:1.14},0).wait(1).to({scaleX:1.14,scaleY:1.14},0).wait(1).to({scaleX:1.14,scaleY:1.14},0).wait(1).to({scaleX:1.15,scaleY:1.15},0).wait(1).to({scaleX:1.15,scaleY:1.15},0).wait(1).to({scaleX:1.15,scaleY:1.15},0).wait(1).to({scaleX:1.15,scaleY:1.15},0).wait(1).to({scaleX:1.15,scaleY:1.15},0).wait(1).to({scaleX:1.16,scaleY:1.16},0).wait(1).to({scaleX:1.16,scaleY:1.16},0).wait(1).to({scaleX:1.16,scaleY:1.16},0).wait(1).to({scaleX:1.16,scaleY:1.16},0).wait(1).to({scaleX:1.16,scaleY:1.16},0).wait(1).to({scaleX:1.16,scaleY:1.16},0).wait(1).to({scaleX:1.16,scaleY:1.16},0).wait(1).to({scaleX:1.16,scaleY:1.16},0).wait(2).to({scaleX:1.16,scaleY:1.16},0).wait(3));

	// Capa_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#771736").s().p("AlrNdQinhHiCiBQiBiChHioQhJitAAi+QAAi9BJiuQBHinCBiCQCCiBCnhHQCuhJC9AAQC+AACtBJQCoBHCCCBQCBCCBHCnQBJCuAAC9QAAC+hJCtQhHCoiBCCQiCCBioBHQitBJi+AAQi9AAiuhJg");
	this.shape.setTransform(120.5,123.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(72));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(141,143,219.1,208);
// library properties:
lib.properties = {
	id: '491324DABDC64E69A3DF5C32672750EB',
	width: 250,
	height: 247,
	fps: 24,
	color: "#FFFFFF",
	opacity: 0.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['491324DABDC64E69A3DF5C32672750EB'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;