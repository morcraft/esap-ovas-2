<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ESTÁNDARES DE MEJORAMIENTO DE LA CALIDAD</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        {include file='base.css'}
        {include file='styles.css'}
    </style>
    <body>
        <header>
            <img src="assets/img/esap_logo.png" id="esap-logo">
            <h1>Políticas Públicas y Derechos Humanos</h1>
            <h2>Gestión Local y Práctica de Los Derechos Humanos</h2>
        </header>
        <hr>

        <div class="date">
            <p>Hora de generación: <b>{$date}</b></p>
        </div>

        <h2 class="quiz-title">Actividad 2</h2>

        <h2>Suponga que usted es habitante de la vereda El Hornito y todos los días debe enfrentarse al problema de agua contaminada, entonces identifique:</h2>

        <p class="point">1. ¿Cuál es el problema de carácter público y descríbalo en un párrafo?</p>
        <p class="answer">{$a[0][0]}</p>       

        <p class="point">2. ¿Qué derechos humanos son los que se encuentran afectados y descríbalo en un párrafo?</p>
        <p class="answer">{$a[0][1]}</p>   

        <p class="point">3. ¿Qué mecanismo(s) de incidencia política y protección sugeriría y descríbalos en dos párrafos?</p>
        <p class="answer">{$a[1][0]}</p>       

        <p class="point">4. ¿Qué mecanismo de protección sugeriría?</p>
        <p class="answer">{$a[1][1]}</p>

        <h2>Usando el caso anterior, construya paso a paso un plan de incidencia y protección de los derechos afectados. La extensión de este planteamiento debe ser máximo de una hoja.</h2>
        <p class="answer">{$a[2][0]}</p>

        <h2>Envíe este documento al aula por medio del botón "Enviar actividad" de la pestaña anterior que lo llevará al aula del curso. Luego de enviar sus respuestas cierre la pestaña del aula y siga explorando el recurso en la pestaña inicial.</h2>
        
        <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>
        
    </body>
</html>
