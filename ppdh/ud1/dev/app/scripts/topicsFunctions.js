const _ = require('lodash')
const slide1 = require('slide1.js')

module.exports = {
    'Actividad 1': function(){
        slide1.$slide.find('.pointer')
            .removeClass('disabled')

        slide1.$slide.find('.black-square')
            .removeClass('disabled')
            .eq(0).addClass('disabled')
    },
    'Actividad 3': function(){
        slide1.$slide.find('.black-square').last().addClass('disabled')
    }
}