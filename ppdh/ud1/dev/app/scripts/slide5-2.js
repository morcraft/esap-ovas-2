const _ = require('lodash')

module.exports = {
    props: {
        answers: {}
    },
    addEventListeners: function(){
        var self = this
        self.$slide.find('textarea').on('keyup paste', function(){
            var val = $(this).val()
            var id = $(this).attr('data-id')
            self.props.answers[id] = val
        })
    }
}