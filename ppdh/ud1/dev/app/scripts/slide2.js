const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const presentation = require('presentation')

module.exports = {
    props:{
        activityId: 'Actividad 1',
        answers: {},
    },
    onEnter: function(){
        var self = this
        if(self.viewed) return true
        self.viewed = true
        return swal(_.merge(swalOptions, {
            html: require('templates/slide2Modal.hbs')(),
            customClass: 'ova-themed bigger slide2Modal',
        }))
    },
    finish: function(){
        var self = module.exports
        if(self.finished) return true
        var slideId = self.slideId
        var s1 = require('slide' + slideId + '-1.js')
        var s2 = require('slide' + slideId + '-2.js')
        self.finished = true

        self.props.answers = {
            a: {
                '0': s1.props.dropped,
                '1': s2.props.texts,
            },
        }

        return swal(_.merge(swalOptions, {
            html: require('templates/PDFModal.hbs')({
                pdfArgs: $.param(self.props.answers),
                assignLink: _.get(self, 'courseActivities.assign.0.href'),
                activityId: 1,
            }),
            customClass: 'ova-themed bigger PDFModal ova-send-activity',
        }))
        .then(function(){
            presentation.switchToSlide({
                slide: $('.slide[data-slide="1"]'),
            })

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Actividad 1': true,
                },
                action: 'add',
            })
        })
    },
    addEventListeners: function(){
        var self = this
        self.$slide.find('.link[data-id="3"]').on('click', self.finish)
    }
}