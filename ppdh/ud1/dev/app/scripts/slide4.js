const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const presentation = require('presentation')

module.exports = {
    onEnter: function() {
        var self = this
        if(self.viewed) return true
        self.viewed = true
        return swal(_.merge(swalOptions, {
            html: require('templates/slide4InstructionsModal.hbs')(),
            customClass: 'ova-themed bigger slide4InstructionsModal'
        }))
    },
    showModal: function(id){
        if(!_.isNumber(id))
            throw new Error('Invalid id')

        var self = this
        
        swal(_.merge(swalOptions, {
            html: require('templates/slide4Modal' + id + '.hbs')(),
            customClass: 'ova-themed bigger slide4Modal slide4Modal' + id
        }))
        .then(function(){
            if(id < 6){
                var nextId = id + 1
                self.$slide.find('.audio-trigger[data-id="' + nextId + '"]').addClass('visible')
            }
            else{
                self.finished = true
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="3"]')
                })
            }
        })
    },
    addEventListeners: function(){
        var self = this
        self.$slide.find('.audio-trigger').on('click', function(){
            var id = $(this).attr('data-id')
            self.showModal(+id)
        })
    }
}