const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const presentation = require('presentation')

module.exports = {
    onEnter: function(){
        var self = this
        if(self.viewed) return true
        self.viewed = true
        var s = swal(_.merge(swalOptions, {
            html: require('templates/slide3Modal.hbs')(),
            customClass: 'ova-themed bigger slide3Modal'        
        }))

        $('.swal2-modal .button').on('click', swal.clickConfirm)
        return s
    },
    step1: function(){
        return swal(_.merge(swalOptions, {
            html: require('templates/slide3Step1Modal.hbs')(),
            customClass: 'ova-themed bigger slide3Step1Modal'
        }))
    },
    step5: function(){
        return swal(_.merge(swalOptions, {
            html: require('templates/slide3Step5Modal.hbs')(),
            customClass: 'ova-themed biggest slide3Step5Modal'
        }))
    },
    step6: function(){
        const slide5_1 = require('slide5-1.js')
        const slide5_1_2 = require('slide5-1-2.js')
        const slide5_2 = require('slide5-2.js')
        const self = this
        self.finished = true

        return swal(_.merge(swalOptions, {
            html: require('templates/PDFModal.hbs')({
                pdfArgs: $.param({
                    a: {
                        '0': slide5_1.props.answers,
                        '1': slide5_1_2.props.answers,
                        '2': slide5_2.props.answers,
                    }
                }),
                pdfId: 2,
                activityId: 2,
                assignLink: _.get(self, 'courseActivities.forum.0.href'),
            }),
            customClass: 'ova-themed PDFModal slide3Step6Modal ova-send-activity',
        }))
        .then(function(){
            presentation.switchToSlide({
                slide: $('.slide[data-slide="1"]'),
            })

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Actividad 2': true,
                },
                action: 'add',
            })
        })
    },
    addEventListeners: function(){
        var self = this
        self.$slide.find('.step').on('click', function(){
            var id = +$(this).attr('data-id')
            _.isFunction(self['step' + id]) && self['step' + id]()
            var nextId = id + 1
            self.$slide.find('.step[data-id="' + nextId + '"]').addClass('visible')
        })
    }
}