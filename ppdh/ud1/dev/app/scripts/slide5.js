const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const presentation = require('presentation')

module.exports = {
    onEnter: function(){
        var self = this
        if(self.viewed) return true
        self.viewed = true
        return swal(_.merge(swalOptions, {
            html: require('templates/slide5InstructionsModal.hbs')(),
            customClass: 'ova-themed bigger slide5InstructionsModal slide5Modal',
        }))
    },
    showModal: function(id){
        if(!_.isNumber(id))
            throw new Error('Invalid id')

        var self = this
        
        swal(_.merge(swalOptions, {
            html: require('templates/slide5Modal' + id + '.hbs')(),
            customClass: 'ova-themed biggest slide5Modal slide5Modal' + id
        }))
        .then(function(){
            if(id < 5){
                var nextId = id + 1
                self.$slide.find('.point[data-id="' + nextId + '"]').addClass('visible')
            }
            else{
                return self.finish()
            }
        })
    },
    finish: function(){
        var self = this

        self.finished = true
        
        presentation.switchToSlide({
            slide: $('.slide[data-slide="1"]'),
        })

        return self.ovaProgress.updateFinishedTopics({
            topics: {
                'Incidencia política y herramientas para la incidencia en derechos humanos': true,
            },
            action: 'add',
        })  
    },
    addEventListeners: function(){
        var self = this
        self.$slide.find('.point').on('click', function(){
            var id = $(this).attr('data-id')
            self.showModal(+id)
        })
    }
}