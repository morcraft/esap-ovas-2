<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Políticas públicas</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        {include file='base.css'}
        {include file='styles.css'}
    </style>
    <body>
        <header>
            <img src="assets/img/esap_logo.png" id="esap-logo">
            <h1>Políticas Públicas y Derechos Humanos</h1>
            <h2>Políticas públicas</h2>
        </header>
        <hr>

        <div class="date">
            <p>Hora de generación: <b>{$date}</b></p>
        </div>

        <h2 class="quiz-title">Actividad 2</h2>

        <h2>Identifique un problema que pueda ser considerado como un problema público en la ciudad o localidad donde vive. Siga los siguientes pasos:
            <br> 
            1. Realice el video. 
            <br>
            2. Suba un video del problema. 
            <br>
            3. Realice la descripción del mismo</h2>

        <h2 class="term"><b>Video relacionado:</b> {$a[0]}</h2>
        <h2 class="term"><b>Problema descrito:</b> {$a[1]}</h2>
        <h2 class="term"><b>Tres alternativas de solución:</b></h2>
        <h2>{$a[2]}</h2>
        <h2>{$a[3]}</h2>
        <h2>{$a[4]}</h2>
        <h2 class="term"><b>Alternativa de solución seleccionada:</b></h2>
        <h2>{$a[5]}</h2>

        <h2>Envíe este documento al aula por medio del botón "Enviar actividad" de la pestaña anterior que lo llevará al aula del curso. Luego de enviar sus respuestas cierre la pestaña del aula y siga explorando el recurso en la pestaña inicial.</h2>
        
        <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>
        
    </body>
</html>
