const ovaAnimateElements = require('ova-animate-elements')
const animatePerSlide = require('animate-per-slide')
const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const concepts = require('concepts.json')
const conceptsGroup1 = require('conceptsGroup1.json')
const conceptsGroup3 = require('conceptsGroup3.json')
const activitiesHandler = require('activitiesHandler.js')
const presentation = require('presentation')

module.exports = {
    props: {
        slidesWithActivities: [4],
        finishedSlides: {}
    },
    onEnter: function(){
        var self = this

        self.processActivities()
        var slide4 = require('slide4.js')
        if(slide4.finished && !self.slide4Finished){
            self.slide4Finished = true
            swal(_.merge(swalOptions, {
                html: require('templates/continueWithExperience.hbs')({
                    experienceId: 1
                }),
                customClass: 'ova-themed bigger continueWithExperience'
            }))
        }
    },
    processActivities: function(){
        var self = module.exports
        _.forEach(self.props.slidesWithActivities, function(v){
            var slide = require('slide' + v + '.js')
            if(!slide.finished || self.props.finishedSlides[v] === true){
                return true
            }

            self.props.finishedSlides[v] = true
            var activityId = slide.props.activityId
            activitiesHandler.add({
                activityId: activityId,
                answers: slide.props.answers,
                APIInstance: self.APIInstance
            })
            .then(function(response){
                console.log('response', response)
                if(response !== true){
                    return swal(_.merge(swalOptions, {
                        html: require('templates/activitySavingError.hbs')({
                            activityId: activityId
                        }),
                        customClass: 'ova-themed bigger activitySavingError visible-icons',
                        type: 'error'
                    }))
                }

                return self.renderActivityIcon({
                    activityId: activityId,
                    answers: slide.props.answers
                })
            })
        })
    },
    renderActivityIcon: function(args){
        if(!_.isObject(args))
            throw new Error('Invalid arguments')

        if(!_.isString(args.activityId) && !_.isNumber(args.activityId))
            throw new Error('Invalid activityId')

        if(!_.isObject(args.answers))
            throw new Error('Invalid answers')

        var self = this
        var $anchor = self.$slide.find('a[data-activity="' + args.activityId + '"]')
        if(!$anchor.length){
            console.info('No anchor was found for activity ' + args.activityId)
            return false
        }

        var _href = $anchor.attr('_href')
        var href = _href + '?' + $.param({a: args.answers})
        $anchor.attr('href', href).removeClass('disabled')
    },
    animateSlides: function(){
        var self = module.exports
        $('.slide').each(function(){
            ovaAnimateElements($(this).children())
        })

        animatePerSlide.setAnimationClasses({
            $slide: self.$slide,
            action: 'add'
        })
    },
    showInitialModal: function(){
        var self = this
        return swal(_.merge(swalOptions, {
            html: require('templates/slide1Modal.hbs')(),
            customClass: 'ova-themed bigger slide1Modal'
        }))
    },
    showVideoModal1: function(){
        var self = module.exports
        var targetConcepts = _.pick(concepts, [
            'Agenda pública',
            'Políticas públicas'
        ])

        swal(_.merge(swalOptions, {
            html: require('templates/videoModal.hbs')({
                videoLink: _.get(self, 'media.AN.mediaid')
            }),
            customClass: 'ova-themed bigger video'
        }))
        .then(function(){
            return self.ovaConcepts.showNewConceptsModal(_.merge({
                concepts: targetConcepts
            }, conceptsGroup1))
        })
        .then(function (t) {
            self.ovaConcepts.updateConcepts({
                concepts: targetConcepts,
                action: 'insert',
            })

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Tema 1: Definiciones': true
                },
                action: 'add'
            })
        })
    },
    showVideoModal2: function(){
        var self = module.exports
        var targetConcepts = _.pick(concepts, [
            'Evaluación y seguimiento de las políticas públicas'
        ])

        swal(_.merge(swalOptions, {
            html: require('templates/videoModal.hbs')({
                videoLink: _.get(self, 'media.AV.mediaid')
            }),
            customClass: 'ova-themed bigger video'
        }))
        .then(function(){
            return self.ovaConcepts.showNewConceptsModal(_.merge({
                concepts: targetConcepts
            }, conceptsGroup3))
        })
        .then(function (t) {
            self.ovaConcepts.updateConcepts({
                concepts: targetConcepts,
                action: 'insert',
            })

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Tema 3. modificación de políticas': true
                },
                action: 'add'
            })
        })
    },
    parseSavedActivities: function(){
        var self = this
        _.forEach(['Actividad 1', 'Actividad 3'], function(v){
            if(_.isObject(self.activities[v])){
                self.renderActivityIcon({
                    activityId: v,
                    answers: self.activities[v]
                })
            }
        })
    },
    showModal3: function(){
        var self = module.exports
        var topicsToCheck = [
            'Tema 1: Definiciones',
            'Tema 2: Ciclo de políticas públicas'
        ]
        var pendingTopics = self.ovaProgress.topics.pending()
        var showModal = false
        _.forEach(topicsToCheck, function(v, k){
            if(!_.isUndefined(pendingTopics[v])){
                showModal = true
                return true
            }
        })

        if(showModal){
            var s = swal(_.merge(swalOptions, {
                html: require('templates/slide1ContinueModal.hbs')(),
                customClass: 'ova-themed bigger slide1ContinueModal'
            }))
            $('.swal2-modal').find('.option').on('click', swal.close)
        }

        else{
            presentation.switchToSlide({
                slide: $('.slide[data-slide="13"]')
            })
        }
    },
    addEventListeners: function(){
        var self = this
        self.showInitialModal()
        self.animateSlides()
        self.$slide.find('.experience-menu[data-id="0"] .black-square[data-id="1"]').on('click', self.showVideoModal1)
        //self.$slide.find('.experience-menu[data-id="1"] .black-square[data-id="1"]').on('click', self.showVideoModal2)
        self.$slide.find('.experience-menu[data-id="1"] .black-square[data-id="1"]').on('click', self.showModal3)
        self.$slide.find('.experience-menu[data-id="2"] .black-square[data-id="0"]').on('click', self.showVideoModal2)
        self.parseSavedActivities()
    }
}