const _ = require('lodash')
const swalOptions = require('swalOptions.js')
const swal = require('sweetalert2')
const presentation = require('presentation')
const preciseDraggable = require('precise-draggable')

module.exports = {
    props: {
        answers: {},
        activityId: 'Actividad 1',
        dropped: {}
    },
    addEventListeners: function(){
        var self = this
        self.addDraggableEvents()
        self.addDroppableEvents()
        self.$slide.find('.restart').on('click', self.restart)
    },
    restart: function(){
        var self = module.exports
        var $draggableElements = self.$slide.find('.draggable-element')
        var $droppableElements = self.$slide.find('.droppable-element')
        $draggableElements.show()
        $droppableElements.each(function(){
            $(this).data('dropped', false).html('')
        })
        self.props.dropped = {}
    },
    addDraggableEvents: function(){
        var self = this
        var $draggableElements = self.$slide.find('.draggable-element')
        self.props.$droppable = self.$slide.find('.droppable-element')

        preciseDraggable.currentProps.$stage = $('#stage')

        $draggableElements.each(function () {
            var $draggable = $(this)
            var draggie = preciseDraggable.setDraggable({
                $target: $draggable,
                data: {
                    key: $draggable.index(),
                }
            })

            draggie.on('dragStart', function () {
                preciseDraggable.currentProps.dragging = true
                preciseDraggable.currentProps.lastDraggable = {
                    data: {
                        key: $draggable.index(),
                    },
                    $element: $draggable
                }
                self.props.$droppable.each(function () {
                    if (!$(this).data('dropped')) {
                        $(this).addClass('highlight')
                    }
                })
            })

            draggie.on('dragMove', function (event) {
                draggie.overlap = preciseDraggable.getOverlap({
                    originalGroup: $draggable,
                    matchingGroup: self.props.$droppable
                })

                self.props.$droppable.trigger('draggableLeave')
                if (_.isObject(draggie.overlap)) {
                    draggie.overlap.$element2.trigger('draggableOver')
                }
            })


            draggie.on('dragEnd', function () {
                self.props.$droppable.removeClass('highlight over')
                if (_.isObject(draggie.overlap)) {
                    draggie.overlap.$element2.trigger('draggableDrop')
                }
            })

        })
        
    },
    addDroppableEvents: function($element){
        var self = this
        var $droppableElements = self.$slide.find('.droppable-element')

        $droppableElements.on({
            'draggableOver': function () {
                if (preciseDraggable.currentProps.dragging) {
                    if (!$(this).data('dropped')) {
                        $(this).addClass('over')
                    }
                }
            },
            'draggableLeave': function () {
                if (!$(this).data('dropped')) {
                    $(this).removeClass('over')
                }
            },
            'draggableDrop': function () {
                $('#stage').removeClass('grabbing')
                var $droppable = $(this)
                preciseDraggable.currentProps.lastDraggable.$element.removeClass('is-dragging')
                if (!$droppable.data('dropped')) {
                    var element = preciseDraggable.currentProps.lastDraggable
                    var draggableKey = element.data.key
                    var droppableKey = $droppable.attr('data-id')

                    $droppable
                        .data('dropped', true)
                        .html(element.$element.hide().html())

                    self.props.dropped[droppableKey] = draggableKey
                    /*if(_.size(self.props.draggable) === self.props.$droppable.length){
                        self.finish()
                    }*/
                }

            }
        })
    },
}