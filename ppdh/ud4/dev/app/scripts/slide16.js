const _ = require('lodash')
const swalOptions = require('swalOptions.js')
const swal = require('sweetalert2')
const presentation = require('presentation')
const preciseDraggable = require('precise-draggable')

module.exports = {
    props: {
        texts: {},
        activityId: 'Actividad 3'
    },
    onEnter: function(){
        return swal(_.merge(swalOptions, {
            html: require('templates/slide4Modal.hbs')(),
            customClass: 'ova-themed biggest slide4Modal'
        }))
    },
    finish: function(){
        var self = module.exports
        if(self.finished) return true
        var previousSlideId = +self.slideId - 1
        var previousSlide = require('slide' + previousSlideId + '.js')
        self.finished = true

        self.props.answers = {
            a: {
                '0': previousSlide.props.dropped,
                '1': self.props.texts,
            },
        }

        return swal(_.merge(swalOptions, {
            html: require('templates/PDFModal.hbs')({
                pdfArgs: $.param(self.props.answers),
                assignLink: _.get(self, 'courseActivities.assign.1.href'),
                activityId: 3,
            }),
            customClass: 'ova-themed bigger PDFModal ova-send-activity',
        }))
        .then(function(){
            presentation.switchToSlide({
                slide: 'next',
            })

            $('.navigation-buttons .home').addClass('highlight')

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Actividad 3': true,
                },
                action: 'add',
            })
        })
    },
    addTextareaEvents: function(){
        var self = this
        self.$slide.find('textarea').on('keyup paste', function(){
            var id = $(this).attr('data-id')
            var val = $(this).val()
            self.props.texts[id] = val
        })
    },
    addEventListeners: function(){
        var self = this
        self.$slide.find('.finish').on('click', self.finish)
        self.addTextareaEvents()
    }
}