const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const presentation = require('presentation')
const concepts = require('concepts.json')

module.exports = {
    props: {
        answers: {},
        activityId: 'Actividad 3',
        texts: {}
    },
    finish: function(){
        var self = module.exports
        var slide10 = require('slide10.js')
        self.finished = true
        self.props.answers = {
            '0': {
                '0': slide10.props.answers.selectedCards,
                '1': slide10.props.answers.texts,
            },
            '1': self.props.texts,
        }

        var targetConcepts = _.pick(concepts, [
            'Vulnerabilidad acentuada'
        ])


        swal(_.merge(swalOptions, {
            html: require('templates/PDFModal.hbs')({
                pdfArgs: $.param({
                    a: self.props.answers,
                }),
                pdfId: 6,
                assignLink: _.get(self, 'courseActivities.assign.1.href'),
            }),
            customClass: 'ova-themed bigger ova-send-activity PDFModal',
        }))
        .then(function(){
            self.ovaConcepts.updateConcepts({
                concepts: targetConcepts,
                action: 'insert',
            })
        
            presentation.switchToSlide({
                slide: $('.slide[data-slide="1"]')
            })

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Actividad 3': true,
                },
                action: 'add',
            })
        })
    },
    addTextareaListeners: function(){
        var self = this
        self.$slide.find('textarea').on('keyup paste', function(){
            var val = $(this).val()
            var id = $(this).attr('data-id')
            self.props.texts[id] = val
        })
    },
    addEventListeners: function(){
        var self = this
        self.$slide.find('.button.finish').on('click', self.finish)
        self.addTextareaListeners()
    }
}