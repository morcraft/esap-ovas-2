const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const presentation = require('presentation')

module.exports = {
    props: {
        texts: {}
    },
    finish: function(){
        var self = module.exports
        
        swal(_.merge(swalOptions, {
            html: require('templates/PDFModal.hbs')({
                pdfArgs: $.param({
                    a: self.props.texts,
                }),
                pdfId: 4,
                assignLink: _.get(self, 'courseActivities.forum.0.href'),
            }),
            customClass: 'ova-themed bigger ova-send-activity PDFModal',
        }))
        .then(function(){
            presentation.switchToSlide({
                slide: $('.slide[data-slide="1"]')
            })

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Actividad 2': true,
                },
                action: 'add',
            })
        })
    },
    addTextareaListeners: function(){
        var self = this
        self.$slide.find('textarea').on('keyup paste', function(){
            var val = $(this).val()
            var id = $(this).attr('data-id')
            self.props.texts[id] = val
        })
    },
    addEventListeners: function(){
        var self = this
        self.$slide.find('.button.finish').on('click', self.finish)
        self.addTextareaListeners()
    }
}