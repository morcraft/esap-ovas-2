<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Personas y grupos vulnerables de especial protección</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        {include file='base.css'}
        {include file='styles.css'}
    </style>
    <body>
        <header>
            <img src="assets/img/esap_logo.png" id="esap-logo">
            <h1>Políticas Públicas y Derechos Humanos</h1>
            <h2>Personas y grupos vulnerables de especial protección</h2>
        </header>
        <hr>

        <div class="date">
            <p>Hora de generación: <b>{$date}</b></p>
        </div>

        <h2 class="quiz-title">Autoevaluación</h2>

        <h2>Cartas escogidas</h2>

        <div class="selected-cards">

            {foreach from=$a[0][0] key=$k item=$v}
                <div class="card" style="background-image:url(assets/img/cards/{$k}.png)"></div>
                <span>{$cards[$k]}</span>
                <h2 class="response"><b>Justificacion:</b> {$a[0][1][$v@iteration-1]}</h2>
            {/foreach}
        </div>

        <h2>¿De qué manera ha cambiado su percepción de la población vulnerable después de realizar esta unidad?</h2>
        <h2 class="answer">{$a[1][0]}</h2>

        <h2>Envíe este documento al aula por medio del botón "Enviar actividad" de la pestaña anterior que lo llevará al aula del curso. Luego de enviar sus respuestas cierre la pestaña del aula y siga explorando el recurso en la pestaña inicial.</h2>
        
        <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>
        
    </body>
</html>
