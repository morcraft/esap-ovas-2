<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Personas y grupos vulnerables de especial protección</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        {include file='base.css'}
        {include file='styles.css'}
    </style>
    <body>
        <header>
            <img src="assets/img/esap_logo.png" id="esap-logo">
            <h1>Políticas Públicas y Derechos Humanos</h1>
            <h2>Personas y grupos vulnerables de especial protección</h2>
        </header>
        <hr>

        <div class="date">
            <p>Hora de generación: <b>{$date}</b></p>
        </div>

        <h2 class="quiz-title">¿Cree usted que estos grupos tienen estos derechos garantizados?¿Qué estrategias deben implementarse?</h2>

        <h2>{$a[0]}</h2>
        
        <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>
        
    </body>
</html>
