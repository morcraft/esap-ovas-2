<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require 'vendor/autoload.php';
require 'cards.php';

use Dompdf\Dompdf;
// echo '<pre>';
// var_export($_GET);
// echo '</pre>';
// exit();
$smarty = new Smarty();
$smarty->error_reporting = 1;
$smarty->caching = 0;

$smarty->assign($_GET);
$smarty->assign(array(
    'date' => date("Y-m-d h:i:sa"),
    'cards' => $cards,
));

$view = $smarty->fetch('view.tpl');

//echo $view;
//exit();

$dompdf = new Dompdf();
$dompdf->loadHtml($view);
$dompdf->setPaper('A4', 'portrait');
$dompdf->render();
$dompdf->stream("respuestas.pdf", array("Attachment" => false));
//$dompdf->stream("justificacion_actividad_MASC.pdf", array("Attachment" => true));
exit(0);
?>
