<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Mecanismos Nacionales de protección de derechos humanos</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        {include file='base.css'}
        {include file='styles.css'}
    </style>
    <body>
        <header>
            <img src="assets/img/esap_logo.png" id="esap-logo">
            <h1>Políticas Públicas y Derechos Humanos</h1>
            <h2>Mecanismos Nacionales de protección de derechos humanos</h2>
        </header>
        <hr>

        <div class="date">
            <p>Hora de generación: <b>{$date}</b></p>
        </div>

        <h2 class="quiz-title">Autoevaluación</h2>

        <h2>Identifique su conocimiento sobre los mecanismos de protección de Derechos Humanos</h2>
        <h2>Responda las pregunta de acuerdo a sus conocimientos. Seleccione el nivel en que considera están sus conocimientos del tema.</h2>

        <h2 class="question">¿DEFINO LOS DISTINTOS MECANISMOS DE PROTECCIÓN DE DERECHOS?</h2>
        <h2 class="answer">{$range[$a[0]]}</h2>

        <h2 class="question">¿DIFERENCIO EL MARCO LEGAL DE CADA UNO DE LOS MECANISMOS DE PROTECCION DE DERECHOS HUMANOS?</h2>
        <h2 class="answer">{$range[$a[1]]}</h2>

        <hr>
        <h2 class="note">Envíe este documento al aula por medio del botón "Enviar actividad" de la pestaña anterior que lo llevará al aula del curso. Luego de enviar sus respuestas cierre la pestaña del aula y siga explorando el recurso en la pestaña inicial.</h2>
        
        <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>
        
    </body>
</html>
