const _ = require('lodash')
const swalOptions = require('swalOptions.js')
const swal = require('sweetalert2')
const presentation = require('presentation')

module.exports = {
    props: {
        selectedLevel: null,
    },
    finish: function(){
        var self = module.exports
        self.finished = true
        var slide8 = require('slide8.js')
        var slide9 = require('slide9.js')
        var slide10 = require('slide10.js')
        var slide11 = require('slide11.js')
        var slide12 = require('slide12.js')

        swal(_.merge(swalOptions, {
            html: require('templates/PDFModal.hbs')({
                pdfArgs: $.param({
                    a: {
                        '0': slide8.props.texts,
                        '1': slide9.props.texts,
                        '2': slide10.props.texts,
                        '3': slide11.props.texts,
                        '4': slide12.props.texts,
                    }
                }),
                assignLink: _.get(self, 'courseActivities.forum.0.href'),
                activityId: 2,
                pdfId: 2
            }),
            customClass: 'ova-themed bigger ova-send-activity PDFModal'
        }))
        .then(function(){
            presentation.switchToSlide({
                slide: $('.slide[data-slide="1"]')
            })

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Actividad 2': true
                },
                action: 'add'
            })
        })
    },
    addEventListeners: function(){
        var self = this
        self.$slide.find('.button.finish').on('click', self.finish)
    }
}