const _ = require('lodash')
const words = require('./slide3Words.js')
const swalOptions = require('swalOptions.js')
const swal = require('sweetalert2')
const presentation = require('presentation')

module.exports = {
    props: {
        selectedLevel: null,
    },
    fillLiquid: function(rangeId){
        if(!_.isNumber(rangeId))
            throw new Error('Invalid rangeId')
        
        var self = this
        var $rangeSelector = self.$slide.find('.range-selector')
        var $liquid = $rangeSelector.find('.liquid')

        var liquidDimensions = {
            height: 125 - ((rangeId + 1) * 100 / 4)
        }

        var liquidStyle = {
            height: liquidDimensions.height + '%'
        }

        var topStyle = {
            bottom: (liquidDimensions.height - 3) + '%',
            height: '40px'
        }

        $liquid.find('.fill').css(liquidStyle)
        $liquid.find('.top').css(topStyle)
    },
    addRangeEvents: function(){
        var self = this
        self.$slide.find('.range').on('mouseenter', function(){
            var id = $(this).index()
            self.fillLiquid(id)
        })
        .on('click', function(){
            var id = $(this).index()
            self.props.selectedLevel = id
            var $liquid = $(this).parent().siblings('.liquid')
            $liquid.addClass('selected')
        })

        self.$slide.find('.range-selector').on('mouseleave', function(){
            if(!_.isNumber(self.props.selectedLevel)){
                $(this).find('.fill').css({
                    height: 0
                })

                $(this).find('.top').css({
                    bottom: 0,
                    height: 0
                })
            }
            else{
                self.fillLiquid(self.props.selectedLevel)
            }
        })
    },
    addEventListeners: function(){
        var self = this
        self.addRangeEvents()
        self.$slide.find('.button.finish').on('click', self.finish)
    }
}