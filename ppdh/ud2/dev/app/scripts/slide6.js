const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const concepts = require('concepts.json')
const conceptsGroup2 = require('conceptsGroup2.json')
const presentation = require('presentation')

module.exports = {
    onSlide: function(){
        var self = this
        self.finished = true
        var targetConcepts = _.pick(concepts, [
            'Derecho de petición',
        ])

        self.ovaConcepts.showNewConceptsModal(_.merge({
            concepts: targetConcepts
        }, conceptsGroup2))
        .then(function (t) {
            return swal(_.merge(swalOptions, {
                html: require('templates/keepGoingModal.hbs')(),
                customClass: 'ova-themed bigger slide1Modal keepGoingModal'
            }))
        })
        .then(function(){
            self.ovaConcepts.updateConcepts({
                concepts: targetConcepts,
                action: 'insert',
            })
    
            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Derecho de petición - Acciones populares y de grupo': true
                },
                action: 'add'
            })
        })

        return true
    },
    addEventListeners: function(){}
}