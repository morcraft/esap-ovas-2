const _ = require('lodash')
const swalOptions = require('swalOptions.js')
const swal = require('sweetalert2')
const presentation = require('presentation')

module.exports = {
    props: {
        selectedLevel: null,
        activityId: 'Actividad 1'
    },
    finish: function(){
        var self = module.exports
        self.finished = true
        var previousSlideId = self.slideId - 1
        var previousSlide = require('slide' + previousSlideId + '.js')
        self.props.answers = {
            '0': previousSlide.props.selectedLevel,
            '1': self.props.selectedLevel
        }
        swal(_.merge(swalOptions, {
            html: require('templates/PDFModal.hbs')({
                pdfArgs: $.param({
                    a: self.props.answers
                }),
                assignLink: _.get(self, 'courseActivities.assign.0.href'),
                activityId: 1
            }),
            customClass: 'ova-themed bigger ova-send-activity PDFModal'
        }))
        .then(function(){
            presentation.switchToSlide({
                slide: 'next'
            })

            $('.navigation-buttons .home').addClass('highlight')

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Actividad 1': true
                },
                action: 'add'
            })
        })
    },
    fillLiquid: function(rangeId){
        if(!_.isNumber(rangeId))
            throw new Error('Invalid rangeId')
        
        var self = this
        var $rangeSelector = self.$slide.find('.range-selector')
        var $liquid = $rangeSelector.find('.liquid')

        var liquidDimensions = {
            height: 125 - ((rangeId + 1) * 100 / 4)
        }

        var liquidStyle = {
            height: liquidDimensions.height + '%'
        }

        var topStyle = {
            bottom: (liquidDimensions.height - 3) + '%',
            height: '40px'
        }

        $liquid.find('.fill').css(liquidStyle)
        $liquid.find('.top').css(topStyle)
    },
    addRangeEvents: function(){
        var self = this
        self.$slide.find('.range').on('mouseenter', function(){
            var id = $(this).index()
            self.fillLiquid(id)
        })
        .on('click', function(){
            var id = $(this).index()
            self.props.selectedLevel = id
            var $liquid = $(this).parent().siblings('.liquid')
            $liquid.addClass('selected')
        })

        self.$slide.find('.range-selector').on('mouseleave', function(){
            if(!_.isNumber(self.props.selectedLevel)){
                $(this).find('.fill').css({
                    height: 0
                })

                $(this).find('.top').css({
                    bottom: 0,
                    height: 0
                })
            }
            else{
                self.fillLiquid(self.props.selectedLevel)
            }
        })
    },
    addEventListeners: function(){
        var self = this
        self.addRangeEvents()
        self.$slide.find('.button.finish').on('click', self.finish)
    }
}